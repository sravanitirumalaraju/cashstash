//
//  GalleryIncomingCollectionViewCell.swift
//  CashStash
//
//  Created by apple on 3/21/19.
//  Copyright © 2019 havells. All rights reserved.
//

import UIKit

class GalleryIncomingCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var galleryIncomingImgVw: UIImageView!
    @IBOutlet weak var userProfileImg: UIImageView!
    @IBOutlet weak var dateAndNameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
