//
//  GalleryOutgoingCollectionViewCell.swift
//  CashStash
//
//  Created by apple on 3/21/19.
//  Copyright © 2019 havells. All rights reserved.
//

import UIKit

class GalleryOutgoingCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var galleryOutgoingImgVw: UIImageView!
    @IBOutlet weak var dateAndTimeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
