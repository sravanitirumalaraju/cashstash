//
//  DeleteMsgCollectionViewCell.swift
//  CashStash
//
//  Created by apple on 4/30/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit

class DeleteMsgCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var deleteLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        deleteLbl.layer.cornerRadius = 10
        deleteLbl.clipsToBounds = true
        // Initialization code
    }

}
