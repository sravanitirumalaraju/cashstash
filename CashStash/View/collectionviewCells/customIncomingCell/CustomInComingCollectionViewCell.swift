//
//  CustomInComingCollectionViewCell.swift
//  CashStash
//
//  Created by apple on 4/26/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit

class CustomInComingCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var incomingMessageBubble: UIImageView!
    @IBOutlet weak var moneyLbl: UILabel!
    @IBOutlet weak var requestLbl: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var userImageVw: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
