//
//  CustomOutgoingCollectionViewCell.swift
//  CashStash
//
//  Created by apple on 4/26/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit
import JSQMessagesViewController

class CustomOutgoingCollectionViewCell:  UICollectionViewCell{

    @IBOutlet weak var messageHeight: NSLayoutConstraint!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var messageBubble: UIImageView!
    @IBOutlet weak var moneyLbl: UILabel!
    @IBOutlet weak var requestLbl: UILabel!
    @IBOutlet weak var userImageVw: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.contentView.translatesAutoresizingMaskIntoConstraints = false
//        let cellHeight = 120
//        messageHeight.constant = CGFloat(cellHeight)
    }

}
