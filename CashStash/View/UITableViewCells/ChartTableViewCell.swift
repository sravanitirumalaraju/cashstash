//
//  ChartTableViewCell.swift
//  CashStash
//
//  Created by apple on 3/27/19.
//  Copyright © 2019 havells. All rights reserved.
//

import UIKit

class ChartTableViewCell: UITableViewCell {

    @IBOutlet weak var userImageVw: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var contributeprogress: UIProgressView!
    @IBOutlet weak var contributeStatusLbl: UILabel!
    @IBOutlet weak var withDrawProgress: UIProgressView!
    @IBOutlet weak var withDrawStatusLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contributeprogress.layer.cornerRadius = 5
        contributeprogress.clipsToBounds = true
        
        withDrawProgress.layer.cornerRadius = 5
        withDrawProgress.clipsToBounds = true
        
        userImageVw.layer.cornerRadius = userImageVw.frame.size.height / 2
        userImageVw.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
