//
//  transactionTableViewCell.swift
//  CashStash
//
//  Created by apple on 28/03/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit

class transactionTableViewCell: UITableViewCell {

    @IBOutlet var fromDataLbl: UILabel!
    @IBOutlet var receivedAmountLbl: UILabel!
    @IBOutlet var dateLbl: UILabel!
    @IBOutlet var toDataLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
