//
//  ReasonsTableViewCell.swift
//  CashStash
//
//  Created by apple on 5/7/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit

class ReasonsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var reasonLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
