//
//  FavouriteTableViewCell.swift
//  CashStash
//
//  Created by apple on 01/02/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit

class FavouriteTableViewCell: UITableViewCell {

    @IBOutlet var contactNameLbl: UILabel!
    @IBOutlet var phoneNumberLbl: UILabel!
    @IBOutlet var profilePictureImg: UIImageView!
    @IBOutlet var payBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        profilePictureImg.layer.cornerRadius = profilePictureImg.frame.size.height/2
        profilePictureImg.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
