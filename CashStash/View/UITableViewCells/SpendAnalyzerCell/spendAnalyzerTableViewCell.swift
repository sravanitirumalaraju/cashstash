//
//  spendAnalyzerTableViewCell.swift
//  CashStash
//
//  Created by apple on 5/4/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit

class spendAnalyzerTableViewCell: UITableViewCell {

    @IBOutlet weak var nospendingsLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var transacLabel: UILabel!
    @IBOutlet weak var analyzeNameLbl: UILabel!
    @IBOutlet weak var colorLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        colorLbl.layer.cornerRadius = 10
        colorLbl.clipsToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
