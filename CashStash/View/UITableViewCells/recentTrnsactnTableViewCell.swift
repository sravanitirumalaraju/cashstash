//
//  recentTrnsactnTableViewCell.swift
//  CashStash
//
//  Created by apple on 05/02/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit

class recentTrnsactnTableViewCell: UITableViewCell {

    @IBOutlet var memberView: UIView!
    @IBOutlet var userNameLbl: UILabel!
    @IBOutlet var userStatus: UIButton!
    @IBOutlet var userAmountLbl: UILabel!
    @IBOutlet var spendAmountLbl: UILabel!
    @IBOutlet weak var withDrawBtn: UIButton!
    
    @IBOutlet weak var withDrawSwitch: UISwitch!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
