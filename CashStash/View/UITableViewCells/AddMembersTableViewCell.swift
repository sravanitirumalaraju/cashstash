//
//  AddMembersTableViewCell.swift
//  CashStash
//
//  Created by apple on 09/03/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit

class AddMembersTableViewCell: UITableViewCell {

    @IBOutlet var profileImageVw: UIImageView!
    @IBOutlet var requestBtn: UIButton!
    @IBOutlet var contactNoLbl: UILabel!
    @IBOutlet var contactNameLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        profileImageVw.layer.cornerRadius = profileImageVw.frame.size.height/2
        profileImageVw.clipsToBounds = true
        
        requestBtn.layer.cornerRadius = requestBtn.frame.size.height/2
        requestBtn.clipsToBounds = true
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
