//
//  AppDelegate.swift
//  CashStash
//
//  Created by apple on 30/01/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications
import CoreData
import FirebaseInstanceID
import FirebaseMessaging
import FirebaseAuth


@UIApplicationMain
class AppDelegate: UIResponder,UIApplicationDelegate,CLLocationManagerDelegate,UNUserNotificationCenterDelegate, MessagingDelegate {

    var window: UIWindow?
    var navigationController = UINavigationController()
    let gcmMessageIDKey = "gcm.message_id"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
//        Thread.sleep(forTimeInterval: 2)
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = UIColor(red: 5.0/255.0, green: 78.0/255.0, blue: 106.0/255.0, alpha: 1.0)
        FirebaseApp.configure()
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        moveToHome()
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification), name: Notification.Name.InstanceIDTokenRefresh, object: nil)
        storingContactToCoredata()
        return true
    }
    func storingContactToCoredata(){
        Contact.DeleteAllData()
        Contact.getContacts(completion: {(result) in
            print(result)
        })
    }
    @objc func tokenRefreshNotification(notification: NSNotification) {
        if let refreshedToken = Messaging.messaging().fcmToken {
            print("refresh token: \(refreshedToken)")
            if UserDefaults.standard.object(forKey: "FCM_Token") == nil{
                SharedClass.sharedInstance.saveToUserDefaultsWithString(string: refreshedToken, key: "FCM_Token")
            }else{
                let fcmSavedToken = UserDefaults.standard.value(forKey: "FCM_Token") as! String
                if fcmSavedToken == Messaging.messaging().fcmToken{
                    print(fcmSavedToken)
                }else{
                    SharedClass.sharedInstance.saveToUserDefaultsWithString(string: refreshedToken, key: "FCM_Token")
                }
            }
        }
    }
    func moveToHome(){
        if let retrievedToken: String = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "token"){
            if retrievedToken != "" {
                print("Retrieved password is: \(retrievedToken)")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                self.navigationController.pushViewController(initialViewController, animated: true)
                self.navigationController.isNavigationBarHidden = true
                self.window?.rootViewController = navigationController
            }
        }else{
            print("do nothing")
        }
    }

    // The callback to handle data message received via FCM for devices running iOS 10 or above.
    func application(received remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Pass device token to auth
        Messaging.messaging().apnsToken = deviceToken
//        Auth.auth().setAPNSToken(deviceToken, type: AuthAPNSTokenType.prod)
        Auth.auth().setAPNSToken(deviceToken, type: AuthAPNSTokenType.sandbox)
    }

    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Swift.Void){
        let userInfo = notification.request.content.userInfo
        // Print message ID.
        print("Message ID: \(userInfo["gcm.message_id"]!)")
        // Print full message.
        print("%@", userInfo)
        let navigationController = window?.rootViewController as? UINavigationController
        if let activeController = navigationController!.visibleViewController{
            if activeController.isKind(of: RecentChatViewController.self){
                print("I have found my controller!")
            }else{
                completionHandler([.alert, .badge, .sound])
            }
        }
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        print(userInfo)
        if let title = userInfo["notificationTitle"] as? String {
            print(title)
            if userInfo["notificationTitle"] as! String == "Chat-Notification"{
                if let _ = userInfo["userFromId"] as? String{
                    let navigationController = window?.rootViewController as? UINavigationController
                    if let activeController = navigationController!.visibleViewController{
                        if activeController.isKind(of: RecentChatViewController.self){
                            print("I have found my controller!")
                        }else{
                            var PhoneNumber = String()
                            Contact.fetchingCoreData(completion: { (contactResults) in
                                if contactResults.count != 0{
                                    Contact.getRegisterUsers(completion: {(RegistersDict) in
                                        if let _ = RegistersDict["message"] as? String{
                                        }else{
                                            for result in contactResults {
                                                PhoneNumber = (result.value(forKey: "mobileNumber")!) as! String
                                                let contactName = (result.value(forKey: "contactName")!)
                                                for (_,value) in RegistersDict.enumerated(){
                                                    let contactDict = value.value as! NSMutableDictionary
                                                    if (value.key as! String).contains(userInfo["userFromId"] as! String){
                                                        let numberDict = contactDict.value(forKey: "registers") as! NSMutableDictionary
                                                        if (PhoneNumber).contains(numberDict["Contact"] as! String){
                                                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                                            let destVc = storyboard.instantiateViewController(withIdentifier: "RecentChatViewController") as! RecentChatViewController
                                                            destVc.toId = (userInfo["userFromId"] as! String)
                                                            destVc.senderDisplayName = contactName as? String
                                                            navigationController?.pushViewController(destVc, animated: true)
                                                            break
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    })
                                }
                            })
                        }
                    }
                }
            }else if userInfo["notificationTitle"] as! String == "Shared wallet Chat"{
                let navigationController = window?.rootViewController as? UINavigationController
                if let activeController = navigationController!.visibleViewController{
                    if activeController.isKind(of: GroupChatViewController.self){
                        print("I have found my controller!")
                    }else{
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let destVc = storyboard.instantiateViewController(withIdentifier: "GroupChatViewController") as! GroupChatViewController
                        destVc.GroupNameString = (userInfo["walletName"] as! String)
                        destVc.GroupId = (userInfo["sharedWalletId"] as! String)
                        destVc.GroupImageURL = (userInfo["walletImage"] as! String)
                        navigationController?.pushViewController(destVc, animated: true)
                    }
                }
            }else if userInfo["notificationTitle"] as! String == "Shared wallet Request"{
                let navigationController = window?.rootViewController as? UINavigationController
                if let activeController = navigationController!.visibleViewController{
                    if activeController.isKind(of: ShareWalletViewController.self){
                        print("I have found my controller!")
                    }else{
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let destVc = storyboard.instantiateViewController(withIdentifier: "ShareWalletViewController") as! ShareWalletViewController
                        navigationController?.pushViewController(destVc, animated: true)
                    }
                }
            }else if userInfo["notificationTitle"] as! String == "Shared wallet Delete" || userInfo["notificationTitle"] as! String == "KYC-Verification" || userInfo["notificationTitle"] as! String == "Withdrawn Cancel"{
                let navigationController = window?.rootViewController as? UINavigationController
                if let activeController = navigationController!.visibleViewController{
                    if activeController.isKind(of: NotificationViewController.self){
                        print("I have found my controller!")
                    }else{
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let destVc = storyboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
                        navigationController?.pushViewController(destVc, animated: true)
                    }
                }
            }
            else{
                if Auth.auth().canHandleNotification(userInfo) {
                    print(userInfo)
                    completionHandler(UIBackgroundFetchResult.noData)
                    return
                }
            }
        }else{
            if Auth.auth().canHandleNotification(userInfo) {
                print(userInfo)
                completionHandler(UIBackgroundFetchResult.noData)
                return
            }
        }
       
    }
    // For iOS 9+
    func application(_ application: UIApplication, open url: URL,options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        if Auth.auth().canHandle(url) {
            return true
        }
        return true
        // URL not auth related, developer should handle it.
    }
    func application(_ application: UIApplication,didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Remote notification support is unavailable due to error: \(error.localizedDescription)")
    }

    func applicationWillResignActive(_ application: UIApplication) {
        print("resign active")
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        print("background")
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        print("foregound")
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        print("active")
//        storingContactToCoredata()
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "CashStash")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    class func getContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
}

