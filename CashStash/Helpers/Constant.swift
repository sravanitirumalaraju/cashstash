//
//  Constant.swift
//  CashStash
//
//  Created by apple on 30/01/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit
import Firebase

var BaseURL = "https://cashstash.me/"
//var BaseURL = "https://cashstash.herokuapp.com/"
var signUpURL = BaseURL + "signUp"
var signInURL = BaseURL + "login"
var GetUserProfileURL = BaseURL + "api/GetUserProfile"
var updateUserProfileURL = BaseURL + "api/UpdateUser"
var createShareWalletURL = BaseURL + "api/CreateAndAddUsersToSharedWallet"
var getShareWalletURL = BaseURL + "api/GetSharedWalletList"
var getUsersShareWalletsRequestsURL = BaseURL + "api/GetUserSharedWalletRequests"
var getShareWalletDetailsURL = BaseURL + "api/GetSharedWalletDetails/"
var updateShareWalletURL = BaseURL + "api/UpdateSharedWallet/"
var acceptUserRequestURL = BaseURL + "api/AcceptUserRequest"
var rejectUserRequestURL = BaseURL + "api/RejectUserRequest"
var makeAdminURL = BaseURL + "api/MakeUserAsAdminInSharedWallet/"
var AddUsersWalletURL = BaseURL + "api/AddUsersToSharedWallet"
var deleteWalletURL = BaseURL + "api/DeleteSharedWallet"
var transactionsURL = BaseURL + "api/GetUserAllTransactions"
var chatNotificationURL = BaseURL + "api/chatNotification"
var appRegisterURL = BaseURL + "api/AppRegistration"
var forgotPasswordURL = BaseURL + "ForgetPassword"
var getSpendReasonURL = BaseURL + "api/GetSpendReasons"
var addSpendReasonURL = BaseURL + "api/AddSpendReason"
var deleteReasonURL = BaseURL + "api/DeleteSpendReasons"
var payMoneyURL = BaseURL + "api/PayMoney"
var getSpendDetailsURL = BaseURL + "api/GetSpendDetails"
var makeLockURL = BaseURL + "api/MakeSharedWalletLockUnlock/"
var uploadSWImageURL = BaseURL + "api/UploadSharedWalletImage/"
var uploadUserImageURL = BaseURL + "api/UploadUserImage"
var loadBalanceURL = BaseURL + "api/LoadBalance"
var bankDetailsURL = BaseURL + "api/UpdateUserBankDetails"
var KycDetailsURL = BaseURL + "api/UploadUserKycDetails"
var ChatPayURL = BaseURL + "api/PayMoneyThroughChat"
var withdrawalURL = BaseURL + "api/WithdrawFromSharedWalletToUser"
var contributeURL = BaseURL + "api/SendFromUserToSharedWallet"
var getNotificationURL = BaseURL + "api/GetNotifications"
var getSWChatNotifyURL = BaseURL + "api/SwChatNotification"
var deleteTokenURL = BaseURL + "api/DeleteRegisterTokenOnSignout"
var deleteAccountURL = BaseURL + "api/DeleteUserProfile"
var updateWithdrawDetailsURL = BaseURL + "api/UpdateWithdrawDetails"
var SWRecentTransactionsURL = BaseURL + "api/GetSharedWalletRecentTransactions"
var updateACHDetailsURL = BaseURL + "api/UpdateUserAchDetails"
var updateOtherDetailsURL = BaseURL + "api/UpdateUserOtherDetails"
var grantPermissionURL = BaseURL + "api/GrantWithdrawPermissionToUser/"
var SWContributionURL = BaseURL + "api/GetSharedWalletContributeWithdrawChart/"
var getMyReferralURL = BaseURL + "api/GetMyReferralCode"

//colors used in application
struct AppColors {
    static let APP_COLOR_BLUE:UIColor = UIColor(red: 36.0/255.0, green: 137.0/255.0, blue: 247.0/255.0, alpha: 1.0)
    static let APP_COLOR_WHITE:UIColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    static let APP_BLURVIEW_COLOR : UIColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.4)
    static let APP_BACKGROUND_COLOR : UIColor = UIColor(red: 249.0/255.0, green: 250.0/255.0, blue: 251.0/255.0, alpha: 1.0)
    static let APP_GREEN_COLOR : UIColor = UIColor(red: 76.0/255.0, green: 217.0/255.0, blue: 100.0/255.0, alpha: 1.0)
    static let APP_RED_COLOR : UIColor = UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
}
//firebase reference
//var ref: DatabaseReference!
let rootRef = Database.database().reference()

enum MessageType {
    case photo
    case text
    case location
}

enum MessageOwner {
    case sender
    case receiver
}

