//
//  sharedClass.swift
//  CashStash
//
//  Created by apple on 28/02/18.
//  Copyright © 2018 havells. All rights reserved.
//
import Foundation

class SharedClass {
    let userDefaults = UserDefaults.standard
    var ChatId = String()
    static let sharedInstance = SharedClass()
//    class var sharedInstance: SharedClass {
//        struct Static {
//            static let instance: SharedClass = SharedClass()
//        }
//        return Static.instance
//    }
    
    //MARK: SAVE TO USER DEFAULTS FOR STRING
    func saveToUserDefaultsWithString(string :String, key: String) {
        userDefaults.setValue(string, forKey: key)
        userDefaults.synchronize()
    }
    //MARK: SAVE TO USER DEFAULTS FOR OBJECTS
    func saveToUserDefaultsWithObject(object :AnyObject, key: String) {
        userDefaults.set(object, forKey: key)
        userDefaults.synchronize()
    }
    //MARK: SAVE TO USER DEFAULTS FOR INTEGER
    func saveToUserDefaultsWithInteger(integer :Int, key: String) {
        userDefaults.set(integer, forKey: key)
        userDefaults.synchronize()
    }
    //MARK: SAVE TO USER DEFAULTS FOR BOOL
    func saveToUserDefaultsWithBool(flag :Bool, key: String) {
        userDefaults.set(flag, forKey: key)
        userDefaults.synchronize()
    }
//    func saveToUserDefaultsWithObject(object :[CNContact], key: String) {
//        userDefaults.set(object, forKey: key)
//        userDefaults.synchronize()
//    }
    //MARK: RETRIVE FROM USER DEFAULTS FOR STRING
    func retrieveFromUserDefaultString(key: String) -> String? {
        if userDefaults.object(forKey: key) != nil{
            return userDefaults.value(forKey: key) as? String
        }
        return ""
    }
    //MARK: RETRIVE FROM USER DEFAULTS FOR OBJECTS
    func retrieveFromUserDefaultsObject(key: String) -> AnyObject? {
        return userDefaults.object(forKey: key) as AnyObject?
    }
    //MARK: RETRIVE FROM USER DEFAULTS FOR INTEGER
    func retrieveFromUserDefaultsInterger(key: String) -> Int? {
        if userDefaults.object(forKey: key) != nil{
            return userDefaults.integer(forKey: key)
        }
        return 0
    }
    //MARK: RETRIVE FROM USER DEFAULTS FOR BOOL
    func retrieveFromUserDefaultsBool(key: String) -> Bool? {
        if userDefaults.object(forKey: key) != nil{
            return userDefaults.bool(forKey: key)
        }
        return false
    }
    
    //MARK: TO REMOVE THE USER DEFAULTS
    func removeUserDefault(key: String) {
        userDefaults.removeObject(forKey: key)
    }
    
}
