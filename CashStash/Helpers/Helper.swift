//
//  Helper.swift
//  CashStash
//
//  Created by apple on 5/24/18.
//  Copyright © 2018 havells. All rights reserved.
//

import Foundation

class Helper{
    static func alertBox (Mymsg:String, view: UIViewController){
        let alert = UIAlertController(title: "", message: Mymsg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        view.present(alert, animated: true, completion: nil)
    }
}
