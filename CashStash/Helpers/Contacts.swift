//
//  Contacts.swift
//  CashStash
//
//  Created by apple on 5/23/18.
//  Copyright © 2018 havells. All rights reserved.
//

import Foundation
import Contacts
import Firebase
import CoreData

class Contact {
    class func getContacts(completion: @escaping (_ completed: Bool) -> Void) {
        let contactStore = CNContactStore()
        let keysToFetch = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactEmailAddressesKey,
            CNContactPhoneNumbersKey,
            CNContactImageDataAvailableKey,
            CNContactThumbnailImageDataKey] as [Any]

        // Get all the containers
        var allContainers: [CNContainer] = []
        do {
            allContainers = try contactStore.containers(matching: nil)
        } catch {
            print("Error fetching containers")
        }

        var results: [CNContact] = []
        // Iterate all containers and append their contacts to our results array
        for container in allContainers {
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            do {
                let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
                results.append(contentsOf: containerResults)
            } catch {
                print("Error fetching results for container")
            }
        }
        for result in results{
            if result.phoneNumbers.count > 0{
                storeContact((["name":(result.givenName + result.familyName),"number":((result.phoneNumbers[0].value).value(forKey: "digits") as! String)]))
            }
        }
        completion(true)
    }
   class func storeContact(_ contacts: [String:Any]) {
        let context =  AppDelegate.getContext()
        let entity = NSEntityDescription.entity(forEntityName: "Contacts", in: context)
        let contact = NSManagedObject(entity: entity!, insertInto: context)
        contact.setValue((contacts["number"]!), forKey: "mobileNumber")
        contact.setValue((contacts["name"]!), forKey: "contactName")
        do {
            try context.save()
        } catch let error as NSError {
            print(error)
        }
    }
    
   class func DeleteAllData(){
        let context =  AppDelegate.getContext()
        let DelAllReqVar = NSBatchDeleteRequest(fetchRequest: NSFetchRequest<NSFetchRequestResult>(entityName: "Contacts"))
        do {
            try context.execute(DelAllReqVar)
        }
        catch {
            print(error)
        }
    }
    class func fetchingCoreData(completion: @escaping (_ results : [NSManagedObject]) -> Void){
        //retrive from coredata
        var coreDataresults: [NSManagedObject]! = []
        let context =  AppDelegate.getContext()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Contacts")
        do {
            coreDataresults = try context.fetch(fetchRequest)
            completion(coreDataresults)
        } catch let error as NSError {
            print(error)
        }
    }
    class func getRegisterUsers(completion: @escaping (_ userDict : NSMutableDictionary) -> Void){
        rootRef.child("users").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.hasChildren(){
                let dic = snapshot.value as! NSMutableDictionary
                completion(dic)
            }else{
                var dict = NSMutableDictionary()
                dict = ["message":"No Contacts found"]
                completion(dict)
            }
        })
    }
}
