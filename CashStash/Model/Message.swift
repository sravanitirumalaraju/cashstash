
//  Message.swift
//  CashStash
//
//  Created by apple on 02/03/18.
//  Copyright © 2018 havells. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class Message {

    //MARK: Properties
    var owner: MessageOwner
    var type: MessageType
    var content: Any
    var senderId : String
    var timestamp: Int = 0
    var isRead: Bool
    var isRequestSent : Bool
    var isCancel : Bool
    var image: UIImage?
    private var toID: String?
    private var fromID: String?
    var allKeys : String?

//    //MARK: Methods
    //everytime you chat
    class func downloadAllMessages(forUserID: String, completion: @escaping (Message) -> Swift.Void) {
        let userId = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "userId")!
        if userId != "" {
            rootRef.child("users").child(userId).child("conversations").child(forUserID).observe(.value, with: { (snapshot) in
                if snapshot.exists() {
                    let data = snapshot.value as! [String: String]
                    let location = data["location"]!
                    rootRef.child("conversations").child(location).observe(.childAdded, with: { (snap) in
                        if snap.exists() {
                            let receivedMessage = snap.value as! [String: Any]
//                            print(receivedMessage)
                            let messageType = receivedMessage["type"] as! String
                            var type = MessageType.text
                            switch messageType {
                                
                            case "photo":
                                type = .photo
                            case "location":
                                type = .location
                            default: break
                                
                            }
                            
                            let content = receivedMessage["content"] as! String
                            let fromID = receivedMessage["fromID"] as! String
                            let timestamp = receivedMessage["timestamp"] as! Int
                            let isRequest = receivedMessage["isRequestSent"] as! Bool
                            let isCancel = receivedMessage["isCancel"] as! Bool
                            if fromID == userId {
                                let message = Message.init(type: type, content: content, owner: .receiver, timestamp: timestamp, isRead: true, senderId: fromID, isRequestSent: isRequest, allKeys: snap.key, isCancel: isCancel)
                                completion(message)
                            } else {
                                let message = Message.init(type: type, content: content, owner: .sender, timestamp: timestamp, isRead: true, senderId: fromID, isRequestSent: isRequest, allKeys: snap.key, isCancel: isCancel)
                                completion(message)
                            }
                        }
                    })
                }else{
                    let message = Message.init(type: MessageType.text, content: "", owner: .receiver, timestamp: 0, isRead: false, senderId: "", isRequestSent: false, allKeys: "0", isCancel: false)
                    completion(message)
                }
            })
        }
    }
    //Chat reloading
    class func reloadAllMessages(forUserID: String, completion: @escaping (Message) -> Swift.Void) {
        let userId = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "userId")!
        if userId != "" {
            rootRef.child("users").child(userId).child("conversations").child(forUserID).observe(.value, with: { (snapshot) in
                if snapshot.exists() {
                    let data = snapshot.value as! [String: String]
                    let location = data["location"]!
                    rootRef.child("conversations").child(location).observeSingleEvent(of: .value, with: { (snap) in
                        if snap.exists() {
                            let Messages = snap.value as! [String: Any]
                            for indMsgs in Messages{
                                let receivedMessage = indMsgs.value as! [String:Any]
//                                print(indMsgs.key)
                                let messageType = receivedMessage["type"] as! String
                                var type = MessageType.text
                                switch messageType {
                                    
                                case "photo":
                                    type = .photo
                                case "location":
                                    type = .location
                                default: break
                                    
                                }
                                
                                let content = receivedMessage["content"] as! String
                                let fromID = receivedMessage["fromID"] as! String
                                let timestamp = receivedMessage["timestamp"] as! Int
                                let isRequest = receivedMessage["isRequestSent"] as! Bool
                                let isCancel = receivedMessage["isCancel"] as! Bool
                                if fromID == userId {
                                    let message = Message.init(type: type, content: content, owner: .receiver, timestamp: timestamp, isRead: true, senderId: fromID, isRequestSent: isRequest, allKeys: indMsgs.key, isCancel: isCancel)
                                    completion(message)
                                } else {
                                    let message = Message.init(type: type, content: content, owner: .sender, timestamp: timestamp, isRead: true, senderId: fromID, isRequestSent: isRequest, allKeys: indMsgs.key, isCancel: isCancel)
                                    completion(message)
                                }
                            }
                            
                        }
                    })
                }else{
                    let message = Message.init(type: MessageType.text, content: "", owner: .receiver, timestamp: 0, isRead: false, senderId: "", isRequestSent: false, allKeys: "0", isCancel: false)
                    completion(message)
                }
            })
        }
    }
    class func downloadAllGroupMessages(forUserID: String, completion: @escaping (Message) -> Swift.Void){
        let userId = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "userId")!
        if userId != "" {
            rootRef.child("groupUsers").child(forUserID).child("conversations").observe(.value, with: { (snapshot) in
                if snapshot.exists() {
                    let data = snapshot.value as! [String: String]
                    let location = data["location"]!
                    rootRef.child("conversations").child(location).observe(.childAdded, with: { (snap) in
                        if snap.exists() {
                            let receivedMessage = snap.value as! [String: Any]
                            let messageType = receivedMessage["type"] as! String
                            var type = MessageType.text
                            switch messageType {
                            case "photo":
                                type = .photo
                            case "location":
                                type = .location
                            default: break
                            }
                            let content = receivedMessage["content"] as! String
                            let fromID = receivedMessage["fromID"] as! String
                            let timestamp = receivedMessage["timestamp"] as! Int
                            let isRequest = receivedMessage["isRequestSent"] as! Bool
                            let isCancel = receivedMessage["isCancel"] as! Bool
                            if fromID == userId {
                                let message = Message.init(type: type, content: content, owner: .receiver, timestamp: timestamp, isRead: true, senderId: fromID, isRequestSent: isRequest, allKeys: snap.key, isCancel: isCancel)
                                completion(message)
                            } else {
                                let message = Message.init(type: type, content: content, owner: .sender, timestamp: timestamp, isRead: true, senderId: fromID, isRequestSent: isRequest, allKeys: snap.key, isCancel: isCancel)
                                completion(message)
                            }
                        }
                    })
                }else{
                    let message = Message.init(type: MessageType.text, content: "", owner: .receiver, timestamp: 0, isRead: false, senderId: "", isRequestSent: false, allKeys: "0", isCancel: false)
                    completion(message)
                }
            })
        }
    }
    
    class func markMessagesRead(forUserID: String)  {
        let userId = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "userId")!
        if userId != "" {
        rootRef.child("users").child(userId).child("conversations").child(forUserID).observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists() {
                    let data = snapshot.value as! [String: String]
                    let location = data["location"]!
                    rootRef.child("conversations").child(location).observeSingleEvent(of: .value, with: { (snap) in
                        if snap.exists() {
                            for item in snap.children {
                                let receivedMessage = (item as! DataSnapshot).value as! [String: Any]
                                let fromID = receivedMessage["fromID"] as! String
                                if fromID != userId {
                                    rootRef.child("conversations").child(location).child((item as! DataSnapshot).key).child("isRead").setValue(true)
                                }
                            }
                        }
                    })
                }
            })
        }
    }
    
    func downloadLastMessage(forLocation: String, completion: @escaping () -> Swift.Void) {
        let userId = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "userId")!
        if userId != "" {
            rootRef.child("conversations").child(forLocation).queryLimited(toLast: 1).observe(.childAdded, with: { (snapshot) in
//                print(snapshot)
                if snapshot.exists() {
                        let receivedMessage = snapshot.value as! [String: Any]
//                        print(receivedMessage)
                        self.content = receivedMessage["content"]!
                        self.timestamp = receivedMessage["timestamp"] as! Int
                        let messageType = receivedMessage["type"] as! String
                        let fromID = receivedMessage["fromID"] as! String
                        self.toID = receivedMessage["toID"] as? String
                        self.isRead = receivedMessage["isRead"] as! Bool
                        var type = MessageType.text
                        switch messageType {
                        case "text":
                            type = .text
                        case "photo":
                            type = .photo
                        case "location":
                            type = .location
                        default: break
                        }
                        self.type = type
                        if userId == fromID {
                            self.owner = .receiver
                        } else {
                            self.owner = .sender
                        }
                        completion()
                }
            })
        }
    }
    //MARK:- individual chat
//    class func send(message: Message, toID: String, completion: @escaping (Bool) -> Swift.Void)  {
//        let userId = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "userId")!
//        if userId != "" {
//            switch message.type {
//            case .location:
//                let values = ["type": "location", "content": message.content, "fromID": userId, "toID": toID, "timestamp": message.timestamp, "isRead": false]
//                Message.uploadMessage(withValues: values, toID: toID, completion: { (status) in
//                    completion(status)
//                })
//            case .photo:
//                let imageData = UIImageJPEGRepresentation((message.content as! UIImage), 0.5)
//                let child = UUID().uuidString
//                Storage.storage().reference().child("messagePics").child(child).putData(imageData!, metadata: nil, completion: { (metadata, error) in
//                    if error == nil {
//                        let path = metadata?.downloadURL()?.absoluteString
//                        let values = ["type": "photo", "content": path!, "fromID": userId, "toID": toID, "timestamp": message.timestamp, "isRead": false] as [String : Any]
//                        Message.uploadMessage(withValues: values, toID: toID, completion: { (status) in
//                            completion(status)
//                        })
//                    }
//                })
//            case .text:
//                let values = ["type": "text", "content": message.content, "fromID": userId, "toID": toID, "timestamp": message.timestamp, "isRead": false,"isRequestSent":message.isRequestSent]
//                Message.uploadMessage(withValues: values, toID: toID, completion: { (status) in
//                    completion(status)
//                })
//            }
//        }
//    }
    
    class func uploadMessage(withValues: [String: Any], toID: String, completion: @escaping (Bool) -> Swift.Void) {
        let userId = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "userId")!
        if userId != "" {
            rootRef.child("users").child(userId).child("conversations").child(toID).observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists() {
                    let data = snapshot.value as! [String: String]
                    let location = data["location"]!
                    rootRef.child("conversations").child(location).childByAutoId().setValue(withValues)
//                    rootRef.child("conversations").child(location).childByAutoId().setValue(withValues, withCompletionBlock: { (error, _) in
//                        if error == nil {
                        completion(true)
//                        } else {
//                            completion(false)
//                        }
//                    })
                    
                }else{
                rootRef.child("conversations").childByAutoId().childByAutoId().setValue(withValues, withCompletionBlock: { (error, reference) in
                    let data = ["location": reference.parent!.key]
                    rootRef.child("users").child(userId).child("conversations").child(toID).updateChildValues(data as [AnyHashable : Any])
                    rootRef.child("users").child(toID).child("conversations").child(userId).updateChildValues(data as [AnyHashable : Any])
                    completion(true)
                    })
                }
            })
        }
    }
    //MARK: Group chat
//    class func sendGroup(message: Message, toID: String, completion: @escaping (Bool) -> Swift.Void){
//        let userId = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "userId")!
//        if userId != "" {
//            switch message.type {
//            case .location:
//                let values = ["type": "location", "content": message.content, "fromID": userId, "toID": toID, "timestamp": message.timestamp, "isRead": false]
//                Message.uploadMessage(withValues: values, toID: toID, completion: { (status) in
//                    completion(status)
//                })
//            case .photo:
//                let imageData = UIImageJPEGRepresentation((message.content as! UIImage), 0.5)
//                let child = UUID().uuidString
//                Storage.storage().reference().child("messagePics").child(child).putData(imageData!, metadata: nil, completion: { (metadata, error) in
//                    if error == nil {
//                        let path = metadata?.downloadURL()?.absoluteString
//                        let values = ["type": "photo", "content": path!, "fromID": userId, "toID": toID, "timestamp": message.timestamp, "isRead": false] as [String : Any]
//                        Message.uploadGroupMessage(withValues: values, toID: toID, completion: { (status) in
//                            completion(status)
//                        })
//                    }
//                })
//            case .text:
//                let values = ["type": "text", "content": message.content, "fromID": message.senderId, "toID": toID, "timestamp": message.timestamp, "isRead": false]
//                Message.uploadGroupMessage(withValues: values, toID: toID, completion: { (status) in
//                    completion(status)
//                })
//            }
//        }
//    }
    
    class func uploadGroupMessage(withValues: [String: Any], toID: String, completion: @escaping (Bool) -> Swift.Void) {
        let userId = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "userId")!
        if userId != "" {
            rootRef.child("groupUsers").child(toID).child("conversations").observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists() {
                    let data = snapshot.value as! [String: String]
                    let location = data["location"]!
                    rootRef.child("conversations").child(location).childByAutoId().setValue(withValues, withCompletionBlock: { (error, _) in
                        if error == nil {
                            completion(true)
                        } else {
                            completion(false)
                        }
                    })
                } else {
                    rootRef.child("conversations").childByAutoId().childByAutoId().setValue(withValues, withCompletionBlock: { (error, reference) in
                        let data = ["location": reference.parent!.key]
                    rootRef.child("groupUsers").child(toID).child("conversations").updateChildValues(data)
                        completion(true)
                    })
                }
            })
        }
    }
    class func isCancelRequest(forUserID: String,autoID:String, completion: @escaping (Bool) -> Swift.Void) {
        let userId = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "userId")!
        if userId != "" {
        rootRef.child("users").child(userId).child("conversations").child(forUserID).observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists() {
                    let data = snapshot.value as! [String: String]
                    let location = data["location"]!
                    rootRef.child("conversations").child(location).observeSingleEvent(of: .value, with: { (snap) in
                        if snap.exists() {
                            let dict = snap.value as! NSMutableDictionary
//                            print(dict)
                            let receivedMessage = dict[autoID] as! [String: Any]
                            rootRef.child("conversations").child(location).child(autoID).child("isCancel").setValue(true)
                            rootRef.child("conversations").child(location).child(autoID).child("fromID").setValue(userId)
                            rootRef.child("conversations").child(location).child(autoID).child("toID").setValue(forUserID)
                            rootRef.child("conversations").child(location).child(autoID).child("content").setValue("Hey! I canceled your request for CSH \(String(describing: receivedMessage["content"] as! String))")
                            
                            completion(true)
                        }
                    })
                }
            })
        }
    }
    
    class func isSendRequest(forUserID: String,autoID:String, completion: @escaping (Bool) -> Swift.Void) {
        let userId = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "userId")!
        if userId != "" {
            rootRef.child("users").child(userId).child("conversations").child(forUserID).observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists() {
                    let data = snapshot.value as! [String: String]
                    let location = data["location"]!
                    rootRef.child("conversations").child(location).observeSingleEvent(of: .value, with: { (snap) in
                        if snap.exists() {
                            let dict = snap.value as! NSMutableDictionary
                            let receivedMessage = dict[autoID] as! [String: Any]
                            rootRef.child("conversations").child(location).child(autoID).child("isRequestSent").setValue(false)
                            rootRef.child("conversations").child(location).child(autoID).child("fromID").setValue(userId)
                            rootRef.child("conversations").child(location).child(autoID).child("toID").setValue(forUserID)
                            rootRef.child("conversations").child(location).child(autoID).child("content").setValue("Hey! I sent your request for CSH \(String(describing: receivedMessage["content"] as! String))")
                            
                            completion(true)
                        }
                    })
                }
            })
        }
    }
    //MARK: Inits
    init(type: MessageType, content: Any, owner: MessageOwner, timestamp: Int, isRead: Bool,senderId : String,isRequestSent:Bool,allKeys:String,isCancel:Bool) {
        self.type = type
        self.content = content
        self.owner = owner
        self.timestamp = timestamp
        self.isRead = isRead
        self.senderId = senderId
        self.isRequestSent = isRequestSent
        self.allKeys = allKeys
        self.isCancel = isCancel
    }
}


