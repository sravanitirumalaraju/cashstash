//
//  Conversation.swift
//  CashStash
//
//  Created by apple on 05/03/18.
//  Copyright © 2018 havells. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class Conversation {
    
    //MARK: Properties
    let user: UserData
    var lastMessage: Message
    
    //MARK: Methods
    class func showConversations(completion: @escaping ([Conversation]) -> Swift.Void) {
        let userId = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "userId")!
        if userId != "" {
            var conversations = [Conversation]()
            rootRef.child("users").child(userId).child("conversations").observe(.childAdded, with: { (snapshot) in
                if snapshot.exists() {
                    let fromID = snapshot.key
                    let values = snapshot.value as! [String: String]
                    let location = values["location"]!
                    UserData.info(forUserID: fromID, completion: { (user) in
                        let emptyMessage = Message.init(type: .text, content: "loading", owner: .sender, timestamp: 0, isRead: true, senderId: userId, isRequestSent: true, allKeys: "0",isCancel: false)
                        let conversation = Conversation.init(user: user, lastMessage: emptyMessage)
                        conversations.append(conversation)
                        conversation.lastMessage.downloadLastMessage(forLocation: location, completion: {
                            completion(conversations)
                        })
                    })
                }
            })
        }
    }
    
    //MARK: Methods
//    class func showConversations(completion: @escaping ([Conversation]) -> Swift.Void) {
//        let userId = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "userId")!
//        if userId != "" {
//            var conversations = [Conversation]()
//            rootRef.child("users").child(userId).child("conversations").observe(.value, with: { (snapshot) in
//                if snapshot.exists() {
//                    let Messages = snapshot.value as! [String: Any]
//                    for indMsgs in Messages{
//                        let fromID = indMsgs.key
//                        let values = indMsgs.value as! [String: String]
//                        let location = values["location"]!
//                        UserData.info(forUserID: fromID, completion: { (user) in
//                            let emptyMessage = Message.init(type: .text, content: "loading", owner: .sender, timestamp: 0, isRead: true, senderId: userId, isRequestSent: true, allKeys: "0",isCancel: false)
//                            let conversation = Conversation.init(user: user, lastMessage: emptyMessage)
//                            conversations.append(conversation)
//                            conversation.lastMessage.downloadLastMessage(forLocation: location, completion: {
//                                completion(conversations)
//                            })
//                        })
//                    }
//                }
//            })
//        }
//    }
    //MARK: Inits
    init(user: UserData, lastMessage: Message) {
        self.user = user
        self.lastMessage = lastMessage
    }
}
