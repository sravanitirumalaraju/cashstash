//
//  UserData.swift
//  CashStash
//
//  Created by apple on 02/03/18.
//  Copyright © 2018 havells. All rights reserved.
//
import Foundation
import UIKit
import Firebase

class UserData : NSObject{

    //MARK: Properties
    let name: String
    let email: String
    let id: String
    let contact : String
    
    class func info(forUserID: String, completion: @escaping (UserData) -> Swift.Void) {
       rootRef.child("users").child(forUserID).child("registers").observeSingleEvent(of: .value, with: { (snapshot) in
            if let data = snapshot.value as? [String: String] {
                let name = data["name"]!
                let email = data["email"]!
                let contact = data["Contact"]!
                let user = UserData.init(name: name, email: email, id: forUserID,contact: contact)
                completion(user)
            }
        })
    }
    
    //MARK: Inits
    init(name: String, email: String, id: String,contact: String) {
        self.name = name
        self.email = email
        self.id = id
        self.contact = contact
    }
}


