//
//  LoginViewController.swift
//  CashStash
//
//  Created by apple on 30/01/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import Firebase

class LoginViewController: UIViewController,UITextFieldDelegate {
    struct Platform {
        static let isSimulator: Bool = {
            var isSim = false
            #if arch(i386) || arch(x86_64)
            isSim = true
            #endif
            return isSim
        }()
    }
    @IBOutlet var passwordTxtFld: FloatLabelTextField!
    @IBOutlet var mobileNoTxtFld: FloatLabelTextField!
    
    var popUpView = UIView()
    let blurView = UIView()
    let enterOTPTxtFld = FloatLabelTextField()
    let enterMobileNoTxtFld = FloatLabelTextField()
    var isOTPClicked = Bool()
    let countrySelectBtn = UIButton()
    var countryCodeVw = UIView()
    let countryCodeLbl = UILabel()
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = UIColor.white
        
        isOTPClicked = false
        countryCodeVw.isHidden = true

        //Create done button
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneButtonAction))
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        mobileNoTxtFld.inputAccessoryView = doneToolbar
        enterOTPTxtFld.inputAccessoryView = doneToolbar
        enterMobileNoTxtFld.inputAccessoryView = doneToolbar
        
        blurView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        blurView.backgroundColor = AppColors.APP_BLURVIEW_COLOR
        self.view.addSubview(blurView)
        blurView.isHidden = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        view.addGestureRecognizer(tap)
        view.isUserInteractionEnabled = true
    }
    //MARK:- function which is triggered when handleTap is called
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
   //MARK:- SIGN IN CLICKED
    @IBAction func signInClicked(_ sender: Any) {
        if mobileNoTxtFld.text! == ""{
            Helper.alertBox(Mymsg: "Enter mobile number", view: self)
        }else if passwordTxtFld.text! == ""{
            Helper.alertBox(Mymsg: "Enter password", view: self)
        }else{
            if NetworkingManager.isConnectedToNetwork(){
                showActivityIndicator()
                let signInParams = ["contactNumber":"\(mobileNoTxtFld.text!)","password":"\(passwordTxtFld.text!)"]
                let userHeaders: HTTPHeaders = ["Content-Type": "application/json"]
                Alamofire.request(signInURL, method: HTTPMethod.post, parameters: signInParams,encoding: JSONEncoding.default, headers: userHeaders).responseJSON { response in
                    switch(response.result) {
                    case .success(let value):
                        let json = JSON(value)
                        if response.response?.statusCode == 200{
                            let destVc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                            self.navigationController?.pushViewController(destVc, animated: true)
                            UserDefaults.standard.setValue(json["walletAmount"].number!, forKey: "amount")
                            UserDefaults.standard.synchronize()
                            let stringValue = json["token"].string!
                            
                            SharedClass.sharedInstance.saveToUserDefaultsWithString(string: stringValue, key: "token")
                            SharedClass.sharedInstance.saveToUserDefaultsWithString(string: json["userId"].string!, key: "userId")
                            SharedClass.sharedInstance.saveToUserDefaultsWithString(string: json["userName"].string!, key: "name")
                            SharedClass.sharedInstance.saveToUserDefaultsWithString(string: json["userContactNumber"].string!, key: "mobile")
                            SharedClass.sharedInstance.saveToUserDefaultsWithString(string: json["userImageUrl"].string!, key: "profile")
                            SharedClass.sharedInstance.saveToUserDefaultsWithString(string: json["accountType"].string!, key: "accountType")
                            //update to firebase
                            let Dict = ["name":json["userName"].string!,"Contact":json["userContactNumber"].string!,"email":json["emailId"].string!,"profilePic":json["userImageUrl"].string!,"accountType":json["accountType"].string!]
                            let regRef = rootRef.child("users").child(json["userId"].string!).child("registers")
                            regRef.updateChildValues(Dict)
                            self.appRegisteration()
                        }else if response.response?.statusCode == 404{
                            Helper.alertBox(Mymsg: json["message"].string!, view: self)
                        }
                    case .failure(_):
                        print(response.result.error!)
                        break
                    }
                    debugPrint(response)
                    hideActivityIndicator()
                }
            }else{
                 NetworkingManager.netWorkFailed(view: self)
            }
        }
    }
    //MARK:- DONE BUTTON CLICKED
    @objc func doneButtonAction() {
        if isOTPClicked == false{
            passwordTxtFld.becomeFirstResponder()
        }else{
           view.endEditing(true)
        }
    }
    //MARK:- TEXTFILED DELEAGTES
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == mobileNoTxtFld{
            isOTPClicked = false
        }else{
            isOTPClicked = true
            if textField == enterOTPTxtFld{
                self.view.frame.origin.y -= 60
            }
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == enterOTPTxtFld{
            self.view.frame.origin.y += 60
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == mobileNoTxtFld {
            self.passwordTxtFld.becomeFirstResponder()
        }else if textField == passwordTxtFld{
            self.passwordTxtFld.resignFirstResponder()
        }
        return true
    }
    
    //MARK:-SIGNUP CLICKED
    @IBAction func signUpClicked(_ sender: Any) {
        if NetworkingManager.isConnectedToNetwork(){
            self.performSegue(withIdentifier: "segueToSignUp", sender: nil)
        }else{
             NetworkingManager.netWorkFailed(view: self)
        }
    }
    //MARK:- APP REGISTERATION
    @objc func appRegisteration(){
        if NetworkingManager.isConnectedToNetwork(){
            if Platform.isSimulator == false {
                if let FCMToken: String = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "FCM_Token"){
                    if FCMToken != "" {
                        let registerationId =            SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "FCM_Token")
                        let retrievedToken: String? = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "token")
                        let tokenString = "Bearer " + retrievedToken!
                        let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
                        let registerParams = ["registrationId":registerationId!]
                        Alamofire.request(appRegisterURL, method: .post, parameters: registerParams, encoding: JSONEncoding.default, headers: Headers).responseJSON { response in
                            switch (response.result){
                            case .success:
                                print(response.result.value!)
                            case .failure:
                                hideActivityIndicator()
                                print(response.result.error!)
                                break
                            }
                            debugPrint(response)
                        }
                    }
                }
            }
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }

    }
    //MARK:-FORGOT PASSWORD CLICKED
    @IBAction func forgotPassClicked(_ sender: Any) {
        blurView.isHidden = false
        popUpView.isHidden = false
        enterOTPTxtFld.isHidden = true
        
        popUpView.frame = CGRect(x: 0, y: 0, width: 300, height: 200)
        popUpView.center = self.blurView.center
        popUpView.backgroundColor = AppColors.APP_COLOR_WHITE
        popUpView.layer.cornerRadius = 10
        popUpView.clipsToBounds = true
        blurView.addSubview(popUpView)
        
        let cancelBtn = UIButton.init(frame: CGRect(x: popUpView.frame.size.width-40, y: 10, width: 30, height: 30))
        cancelBtn.setImage(UIImage(named:"cross_blue"), for: .normal)
        cancelBtn.addTarget(self, action: #selector(cancelClicked), for: .touchUpInside)
        popUpView.addSubview(cancelBtn)
        
        enterMobileNoTxtFld.frame = CGRect(x: 50, y: 40, width: 200, height: 40)
        enterMobileNoTxtFld.placeholder = "Enter Mobile Number"
        enterMobileNoTxtFld.font = UIFont(name: "Nunito-Regular", size: 16)
        enterMobileNoTxtFld.autocorrectionType = .no
        enterMobileNoTxtFld.keyboardType = .numberPad
        enterMobileNoTxtFld.delegate = self
        popUpView.addSubview(enterMobileNoTxtFld)

//        countrySelectBtn.frame = CGRect(x: 3, y: 40, width: 40, height: 40)
//        countrySelectBtn.backgroundColor = UIColor.white
//        countrySelectBtn.setTitle("", for: .normal)
//        countrySelectBtn.addTarget(self, action: #selector(countrySelectionClicked), for: .touchUpInside)
//        popUpView.addSubview(countrySelectBtn)
        
//        countryCodeLbl.frame = CGRect(x: 0, y: 5, width: 25, height: 30)
//        countryCodeLbl.textColor = UIColor.lightGray
//        countryCodeLbl.text = "+1"
//        countryCodeLbl.textAlignment = .right
//        countryCodeLbl.font = UIFont(name: "Nunito-Regular", size: 13)
//        countryCodeLbl.backgroundColor = UIColor.clear
//        countrySelectBtn.addSubview(countryCodeLbl)
        
//        let dropDowmImg = UIImageView.init(frame: CGRect(x: countryCodeLbl.frame.origin.x + countryCodeLbl.frame.size.width, y: 12.5, width: 15, height: 15))
//        dropDowmImg.image = #imageLiteral(resourceName: "dropDown")
//        countrySelectBtn.addSubview(dropDowmImg)
        
        let sendOtpBtn = UIButton.init(frame: CGRect(x: 50, y: enterMobileNoTxtFld.frame.size.height + enterMobileNoTxtFld.frame.origin.y + 20, width: 200, height: 40))
        sendOtpBtn.setTitle("Send OTP", for: .normal)
        sendOtpBtn.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 14)
        sendOtpBtn.layer.cornerRadius = sendOtpBtn.frame.size.height/2
        sendOtpBtn.backgroundColor = UIColor(red: 36.0/255.0, green: 137.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        sendOtpBtn.addTarget(self, action: #selector(sendOTPClicked), for: .touchUpInside)
        popUpView.addSubview(sendOtpBtn)
        
    }
    //MARK:- CANCEL CLICKED
    @objc func cancelClicked(){
        popUpView.isHidden = true
        blurView.isHidden = true
    }
    //MARK:- SEND OTP CLICKED
    @objc func sendOTPClicked(){
        if enterMobileNoTxtFld.text! == ""{
            Helper.alertBox(Mymsg: "Please enter mobile number", view: self)
        }else{
            verifyWithFirebase(phonenumber: enterMobileNoTxtFld.text!)
            enterOTPTxtFld.isHidden = false
            popUpView.frame = CGRect(x: 0, y: 0, width: 300, height: 300)
            popUpView.center = self.blurView.center
            
            enterOTPTxtFld.frame = CGRect(x: 50, y: enterMobileNoTxtFld.frame.size.height + enterMobileNoTxtFld.frame.origin.y + 80, width: 200, height: 40)
            enterOTPTxtFld.placeholder = "Enter OTP"
            enterOTPTxtFld.font = UIFont(name: "Nunito-Regular", size: 16)
            enterOTPTxtFld.autocorrectionType = .no
            enterOTPTxtFld.keyboardType = .numberPad
            enterOTPTxtFld.delegate = self
            popUpView.addSubview(enterOTPTxtFld)
            
            let verifyOTP = UIButton.init(frame: CGRect(x: 50, y: enterOTPTxtFld.frame.size.height + enterOTPTxtFld.frame.origin.y + 20, width: 200, height: 40))
            verifyOTP.setTitle("Verify OTP", for: .normal)
            verifyOTP.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 14)
            verifyOTP.layer.cornerRadius = verifyOTP.frame.size.height/2
            verifyOTP.backgroundColor = UIColor(red: 36.0/255.0, green: 137.0/255.0, blue: 247.0/255.0, alpha: 1.0)
            verifyOTP.addTarget(self, action: #selector(verifyClicked), for: .touchUpInside)
            popUpView.addSubview(verifyOTP)
        }
    }
    //MARK:- VERIFY CLICKED
    @objc func verifyClicked(){
        showActivityIndicator()
        if enterOTPTxtFld.text! == ""{
            hideActivityIndicator()
            Helper.alertBox(Mymsg: "Please enter OTP", view: self)
        }else{
            let credentials : PhoneAuthCredential = PhoneAuthProvider.provider().credential(withVerificationID: UserDefaults.standard.value(forKey: "authVerificationID") as! String, verificationCode: enterOTPTxtFld.text!)
            Auth.auth().signInAndRetrieveData(with: credentials, completion: { (user, error) in
                hideActivityIndicator()
                if (error != nil){
                    print(error.debugDescription)
                }else{
                    let destVc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
                    destVc.mobileNoString = self.enterMobileNoTxtFld.text!
                    self.navigationController?.pushViewController(destVc, animated: true)
                }
            })
//            Auth.auth().signIn(with: credentials) { (user, error) in
//                hideActivityIndicator()
//                if (error != nil){
//                    print(error.debugDescription)
//                }else{
//                    let destVc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
//                    destVc.mobileNoString = self.enterMobileNoTxtFld.text!
//                    self.navigationController?.pushViewController(destVc, animated: true)
//                }
//            }
        }
    }
    //MARK:- COUNTRY SELECTION CLICKED
    @objc func countrySelectionClicked(){
        countryCodeVw.isHidden = false
        countryCodeVw.frame = CGRect(x: countrySelectBtn.frame.origin.x, y: countrySelectBtn.frame.origin.y + countrySelectBtn.frame.size.height, width: countrySelectBtn.frame.size.width, height: (countrySelectBtn.frame.size.height * 2)+1)
        countryCodeVw.backgroundColor = AppColors.APP_BLURVIEW_COLOR
        popUpView.addSubview(countryCodeVw)
        
        let indianCodeBtn = UIButton.init(frame: CGRect(x: 0, y: 0, width: countryCodeVw.frame.size.width, height: countrySelectBtn.frame.size.height))
        indianCodeBtn.setTitle("+91", for: .normal)
        indianCodeBtn.setTitleColor(AppColors.APP_COLOR_WHITE, for: .normal)
        indianCodeBtn.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 14)
        indianCodeBtn.backgroundColor = UIColor.clear
        indianCodeBtn.addTarget(self, action: #selector(indianCodeClicked), for: .touchUpInside)
        countryCodeVw.addSubview(indianCodeBtn)
        
        let lineLbl = UILabel.init(frame: CGRect(x: 0, y: indianCodeBtn.frame.origin.y + indianCodeBtn.frame.size.height, width: countrySelectBtn.frame.size.width, height: 1))
        lineLbl.backgroundColor = UIColor.white
        countryCodeVw.addSubview(lineLbl)
        
        let USCodeBtn = UIButton.init(frame: CGRect(x: 0, y: indianCodeBtn.frame.origin.y + indianCodeBtn.frame.size.height + 1, width: countrySelectBtn.frame.size.width, height: countrySelectBtn.frame.size.height))
        USCodeBtn.setTitle("+1", for: .normal)
        USCodeBtn.backgroundColor = UIColor.clear
        USCodeBtn.setTitleColor(AppColors.APP_COLOR_WHITE, for: .normal)
        USCodeBtn.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 14)
        USCodeBtn.addTarget(self, action: #selector(USCodeClicked), for: .touchUpInside)
        countryCodeVw.addSubview(USCodeBtn)
    }
    //MARK:- INDIAN CODE CLICKED
    @objc func indianCodeClicked(){
        countryCodeLbl.text = "+91"
        countryCodeVw.isHidden = true
    }
    //MARK:- US CODE CLICKED
    @objc func USCodeClicked(){
        countryCodeLbl.text = "+1"
        countryCodeVw.isHidden = true
    }
    //MARK:- VERIFY WITH FIREBASE
    func verifyWithFirebase(phonenumber : String){
        PhoneAuthProvider.provider().verifyPhoneNumber("+1\(phonenumber)", uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                print(error.localizedDescription)
                print(error)
                return
            }
            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
            UserDefaults.standard.synchronize()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
