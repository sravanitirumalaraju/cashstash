//
//  ForgotPasswordViewController.swift
//  CashStash
//
//  Created by apple on 4/28/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ForgotPasswordViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var enterPasswordTxtfld: FloatLabelTextField!
    @IBOutlet weak var reEnterPassTxtfld: FloatLabelTextField!
    @IBOutlet weak var changePassBtn: UIButton!
    
    var mobileNoString = String()
    let blurView = UIView()
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        blurView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        blurView.backgroundColor = AppColors.APP_BLURVIEW_COLOR
        self.view.addSubview(blurView)
        blurView.isHidden = true
        enterPasswordTxtfld.delegate = self
        reEnterPassTxtfld.delegate = self
        enterPasswordTxtfld.isSecureTextEntry = true
        reEnterPassTxtfld.isSecureTextEntry = true
        changePassBtn.layer.cornerRadius = changePassBtn.frame.size.height/2
        changePassBtn.clipsToBounds = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        view.addGestureRecognizer(tap)
        view.isUserInteractionEnabled = true
    }
    //MARK:- function which is triggered when handleTap is called
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    //MARK:-TEXT FILED DELEGATE
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    //MARK:- CHANGE PASSWORD CLICKED
    @IBAction func changepassClicked(_ sender: Any) {
        if enterPasswordTxtfld.text! == ""{
            Helper.alertBox(Mymsg: "Please enter the password", view: self)
        }else if reEnterPassTxtfld.text! == ""{
            Helper.alertBox(Mymsg: "Please re-enter the password", view: self)
        }else if enterPasswordTxtfld.text != reEnterPassTxtfld.text!{
            Helper.alertBox(Mymsg: "Passwords does'nt match", view: self)
        }else{
            showActivityIndicator()
            let userHeaders: HTTPHeaders = ["Content-Type": "application/json"]
            let forgotParams = ["password":enterPasswordTxtfld.text!,"contactNumber":mobileNoString]
            Alamofire.request(forgotPasswordURL, method: HTTPMethod.put, parameters: forgotParams,encoding: JSONEncoding.default, headers: userHeaders).responseJSON { response in
                hideActivityIndicator()
                switch (response.result){
                case .success:
                    let destVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    self.navigationController?.pushViewController(destVc, animated: true)
                case .failure:
                    hideActivityIndicator()
                    print(response.result.error!)
                    break
                }
                debugPrint(response)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
