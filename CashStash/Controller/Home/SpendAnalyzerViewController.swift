//
//  SpendAnalyzerViewController.swift
//  CashStash
//
//  Created by apple on 5/4/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit
import Charts
import Alamofire
import SwiftyJSON
import FSCalendar

class SpendAnalyzerViewController: UIViewController,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,FSCalendarDelegate,FSCalendarDataSource,FSCalendarDelegateAppearance {

    @IBOutlet weak var spendScrollView: UIScrollView!
    let pieChartView = PieChartView()
    var spendDetailsArray = Array<Any>()
    var reasons = [String]()
    var unitsSold = [Double]()
    var colors: [UIColor] = []

    let toTimestamp = Int(Date().timeIntervalSince1970)
    let previousMonth = Calendar.current.date(byAdding: .month, value: -1, to: Date())
    let blurview = UIView()
    let fromButton = UIButton()
    let toButton = UIButton()
    let cancelBtn = UIButton()
    let okButton = UIButton()
    let formatter = DateFormatter()
    var calendar = FSCalendar()
    var selectedTimestamp = Int(Date().timeIntervalSince1970)
    var startTimestamp = Int(Date().timeIntervalSince1970)
    var endTimestamp = Int(Date().timeIntervalSince1970)
    var dateLbl = UILabel()
    let spendDetailsTableVw = UITableView()
    let spendBackgroundVw = UIView()
    var isFromClicked = Bool()
    var isToClicked = Bool()
    var isOKClicked = Bool()
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        isFromClicked = true
        spendBackgroundVw.backgroundColor = UIColor.white
        spendBackgroundVw.layer.cornerRadius = 10
        spendBackgroundVw.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.05).cgColor
        spendBackgroundVw.layer.shadowOffset = CGSize(width: 0, height: 2)
        spendBackgroundVw.layer.shadowOpacity = 1
        spendBackgroundVw.layer.shadowRadius = 10
        spendBackgroundVw.layer.masksToBounds = false
        spendScrollView.addSubview(spendBackgroundVw)
        spendDetailsTableVw.register(UINib(nibName: "spendAnalyzerTableViewCell", bundle: nil), forCellReuseIdentifier: "spendAnalyzerTableViewCell")
        spendScrollView.addSubview(spendDetailsTableVw)

        formatter.timeZone = TimeZone(abbreviation: "GMT")
        formatter.locale = NSLocale.current
        formatter.dateFormat = "MMM dd yyyy"
        
        blurview.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        blurview.backgroundColor = AppColors.APP_BLURVIEW_COLOR
        self.view.addSubview(blurview)
        blurview.isHidden = true
        
        spendScrollView.delegate = self
        getSpendDetails()
    }
    //MARK:- GET SPEND ANALYZER DETAILS
    func  getSpendDetails(){
        if NetworkingManager.isConnectedToNetwork(){
            showActivityIndicator()
            let retrievedToken: String? = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "token")
            let tokenString = "Bearer " + retrievedToken!
            let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
            let myTimeStamp = previousMonth?.timeIntervalSince1970
            let parms = ["fromDate":(Int(myTimeStamp!)),"toDate":toTimestamp] as [String : Any]
            Alamofire.request(getSpendDetailsURL, method: .post, parameters: parms, encoding: JSONEncoding.default, headers: Headers).responseJSON { response in
                hideActivityIndicator()
                switch (response.result){
                case .success:
                    let json = JSON(response.result.value!)
                    self.spendDetailsArray = json.arrayValue as Array<Any>
                    for value in self.spendDetailsArray{
                        let dict = value as! JSON
                        self.reasons.append(dict["reason"].string!)
                        self.unitsSold.append(Double(truncating: dict["amount"].number!))
                    }
                    if self.spendDetailsArray.count != 0{
                       self.setChart(dataPoints: self.reasons, values: self.unitsSold)
                    }else{
                        let noResultLbl = UILabel.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
                        noResultLbl.text = "No Results"
                        noResultLbl.textAlignment = .center
                        self.view.addSubview(noResultLbl)
                    }
                case .failure:
                    print("error")
                }
            }
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }
    }
    //MARK:- SET VALEUS TO ANALYZER
    func setChart(dataPoints: [String], values: [Double]) {
        DispatchQueue.main.async {
            var dataEntries: [ChartDataEntry] = []
            for i in 0..<dataPoints.count {
                let dataEntry1 = ChartDataEntry(x: Double(i), y: values[i], data: dataPoints[i] as AnyObject)
                dataEntries.append(dataEntry1)
            }
            let pieChartDataSet = PieChartDataSet(values: dataEntries, label: "")
            pieChartDataSet.drawValuesEnabled = false
            for _ in 0..<dataPoints.count {
                let red = Double(arc4random_uniform(256))
                let green = Double(arc4random_uniform(256))
                let blue = Double(arc4random_uniform(256))
                
                let color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
                self.colors.append(color)
                pieChartDataSet.colors = self.colors
            }
            // Set data here
            let pieChartData = PieChartData(dataSet: pieChartDataSet)
            self.pieChartView.data = pieChartData
            self.showDetails()
        }
    }
    //MARK:- SHOW DETAILS
    func showDetails() {
            let overViewLbl = UILabel.init(frame: CGRect(x: 20, y: 10, width: 150, height: 25))
            overViewLbl.text = "Spendings Overview"
            overViewLbl.font = UIFont(name: "Barlow-Regular", size: 12.0)
            overViewLbl.textColor = UIColor.init(red: 157.0/255.0, green: 145.0/255.0, blue: 171.0/255.0, alpha: 1.0)
            spendScrollView.addSubview(overViewLbl)
            
            let calendarButton = UIButton.init(frame: CGRect(x: self.view.frame.size.width - 50, y: 10, width: 30, height: 30))
            calendarButton.setImage(#imageLiteral(resourceName: "calendar"), for: .normal)
            calendarButton.setTitle("", for: .normal)
            calendarButton.addTarget(self, action: #selector(calendarClicked), for: .touchUpInside)
            spendScrollView.addSubview(calendarButton)
            
            spendBackgroundVw.frame = CGRect(x: 20, y: overViewLbl.frame.origin.y + overViewLbl.frame.size.height + 10, width: self.view.frame.size.width - 40, height: 260)
        
            dateLbl.frame = CGRect(x: 0, y: 15, width: spendBackgroundVw.frame.size.width, height: 25)
            if unitsSold.count != 0{
                if isOKClicked == false{
                    let myTimeStamp = previousMonth?.timeIntervalSince1970
                    let fromdateTimeStamp = Date(timeIntervalSince1970: TimeInterval(myTimeStamp!))
                    let fromdate = formatter.string(from: fromdateTimeStamp)
                    
                    let todateTimeStamp = Date(timeIntervalSince1970: TimeInterval(toTimestamp))
                    let toDate = formatter.string(from: todateTimeStamp)
                    dateLbl.text = "\(fromdate) to \(toDate)"
                }
            }
            dateLbl.textAlignment = .center
            dateLbl.textColor = UIColor.init(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0)
            dateLbl.font = UIFont(name: "Nunito-Regular", size: 17.0)
            spendBackgroundVw.addSubview(dateLbl)
        
            let spendingLbl = UILabel.init(frame: CGRect(x: (spendBackgroundVw.frame.size.width/2)-50, y: dateLbl.frame.origin.y + dateLbl.frame.size.height + 5, width: 100, height: 25))
            spendingLbl.text = "All Spendings"
            spendingLbl.textAlignment = .center
            spendingLbl.font = UIFont(name: "Barlow-Medium", size: 16.0)
            spendingLbl.textColor = UIColor.init(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0)
            spendBackgroundVw.addSubview(spendingLbl)
        
            let spendDetailsLbl = UILabel.init(frame: CGRect(x: 20, y: spendBackgroundVw.frame.origin.y + spendBackgroundVw.frame.size.height + 10, width: 150, height: 25))
            spendDetailsLbl.text = "Spendings Details"
            spendDetailsLbl.font = UIFont(name: "Barlow-Regular", size: 12.0)
            spendDetailsLbl.textColor = UIColor.init(red: 157.0/255.0, green: 145.0/255.0, blue: 171.0/255.0, alpha: 1.0)
            spendScrollView.addSubview(spendDetailsLbl)
        
            if self.unitsSold.count != 0{
                pieChartView.isHidden = false
                pieChartView.translatesAutoresizingMaskIntoConstraints = false
                pieChartView.noDataText = "No date to display"
                pieChartView.legend.enabled = false
                pieChartView.chartDescription?.text = ""
                pieChartView.drawHoleEnabled = true
                pieChartView.frame = CGRect(x: (spendBackgroundVw.frame.size.width/2)-125, y: spendingLbl.frame.origin.y + spendingLbl.frame.size.height, width: 250, height: 180)
                spendBackgroundVw.addSubview(pieChartView)
                
                spendDetailsTableVw.frame = CGRect(x: 20, y: spendDetailsLbl.frame.origin.y + spendDetailsLbl.frame.size.height + 10, width: self.view.frame.size.width - 40, height: 80 * CGFloat(unitsSold.count))
                spendDetailsTableVw.delegate = self
                spendDetailsTableVw.dataSource = self
                spendDetailsTableVw.separatorStyle = .singleLine
                spendDetailsTableVw.reloadData()
            }else{
                spendDetailsTableVw.frame = CGRect(x: 20, y: spendDetailsLbl.frame.origin.y + spendDetailsLbl.frame.size.height + 10, width: self.view.frame.size.width - 40, height: 80)
                spendDetailsTableVw.separatorStyle = .none
                spendDetailsTableVw.delegate = self
                spendDetailsTableVw.dataSource = self
                spendDetailsTableVw.reloadData()
                pieChartView.isHidden  = true
                
                let noChartLbl = UILabel.init(frame: CGRect(x: (spendBackgroundVw.frame.size.width/2)-125, y: spendingLbl.frame.origin.y + spendingLbl.frame.size.height, width: 250, height: 180))
                noChartLbl.text = "No data available"
                noChartLbl.textAlignment = .center
                noChartLbl.font = UIFont(name: "Nunito-Regular", size: 16.0)
                noChartLbl.textColor = UIColor.orange
                spendBackgroundVw.addSubview(noChartLbl)
            }

            var contentRect = CGRect.zero
            for view in self.spendScrollView.subviews {   //  content size of scrollview
                contentRect = contentRect.union(view.frame)
            }
            spendScrollView.contentSize.height = contentRect.size.height + 20
    }
    //MARK:- CALENDAR CLICKED
    @objc func calendarClicked(){
        blurview.isHidden = false
        isFromClicked = true
        isToClicked = false
        isOKClicked = false
        
        let calendarUIVw = UIView.init(frame: CGRect(x: 0, y: 0, width: 300, height: 450))
        calendarUIVw.backgroundColor = UIColor.white
        calendarUIVw.center = self.view.center
        calendarUIVw.layer.cornerRadius = 10
        calendarUIVw.clipsToBounds = true
        blurview.addSubview(calendarUIVw)
        
        fromButton.frame = CGRect(x: 0, y: 0, width: calendarUIVw.frame.size.width/2, height: 40)
        fromButton.setTitle("FROM", for: .normal)
        fromButton.backgroundColor = AppColors.APP_COLOR_BLUE
        fromButton.setTitleColor(UIColor.white, for: .normal)
        fromButton.addTarget(self, action: #selector(FromClicked), for: .touchUpInside)
        calendarUIVw.addSubview(fromButton)
        
        toButton.frame = CGRect(x: fromButton.frame.size.width, y: 0, width: calendarUIVw.frame.size.width/2, height: 40)
        toButton.setTitleColor(AppColors.APP_COLOR_BLUE, for: .normal)
        toButton.backgroundColor = UIColor.white
        toButton.setTitle("TO", for: .normal)
        toButton.addTarget(self, action: #selector(toClicked), for: .touchUpInside)
        calendarUIVw.addSubview(toButton)
        
        calendar = FSCalendar(frame: CGRect(x: 0, y: 40, width: 300, height: 370))
        calendar.backgroundColor = UIColor.white
        calendar.dataSource = self
        calendar.delegate = self
        calendarUIVw.addSubview(calendar)
        
        okButton.frame = CGRect(x: 0, y: calendarUIVw.frame.size.height - 40, width: calendarUIVw.frame.size.width/2, height: 40)
        okButton.addTarget(self, action: #selector(okClicked), for: .touchUpInside)
        okButton.setTitle("Ok", for: .normal)
        okButton.setTitleColor(AppColors.APP_COLOR_BLUE, for: .normal)
        calendarUIVw.addSubview(okButton)
        
        cancelBtn.frame = CGRect(x: okButton.frame.size.width, y: calendarUIVw.frame.size.height - 40, width: calendarUIVw.frame.size.width/2, height: 40)
        cancelBtn.addTarget(self, action: #selector(cancelBtnClicked), for: .touchUpInside)
        cancelBtn.setTitle("Cancel", for: .normal)
        cancelBtn.setTitleColor(AppColors.APP_COLOR_BLUE, for: .normal)
        calendarUIVw.addSubview(cancelBtn)
    }
    //MARK:- CANCEL CLICKED
    @objc func cancelBtnClicked(){
        blurview.isHidden = true
        isFromClicked = true
        isToClicked = false
        isOKClicked = false
    }
    //MARK:- FROM CLICKED
    @objc func FromClicked(){
        fromButton.backgroundColor = AppColors.APP_COLOR_BLUE
        fromButton.setTitleColor(UIColor.white, for: .normal)
        toButton.setTitleColor(AppColors.APP_COLOR_BLUE, for: .normal)
        toButton.backgroundColor = UIColor.white
        isFromClicked = true
        isToClicked = false
    }
    //MARK:- TO CLICKED
    @objc func toClicked(){
        toButton.setTitleColor(UIColor.white, for: .normal)
        toButton.backgroundColor = AppColors.APP_COLOR_BLUE
        fromButton.setTitleColor(AppColors.APP_COLOR_BLUE, for: .normal)
        fromButton.backgroundColor = UIColor.white
        isToClicked = true
        isFromClicked = false
        self.endTimestamp = Int(Date().timeIntervalSince1970)
        calendar.setCurrentPage(Date(), animated: true)
    }
    //MARK:- OK CLICKED
    @objc func okClicked(){
        blurview.isHidden = true
        isFromClicked = true
        isToClicked = false
        isOKClicked = true
        if NetworkingManager.isConnectedToNetwork(){
            showActivityIndicator()
            let retrievedToken: String? = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "token")
            let tokenString = "Bearer " + retrievedToken!
            let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
            let parms = ["fromDate":startTimestamp,"toDate":endTimestamp] as [String : Any]
            let startdateTimeStamp = Date(timeIntervalSince1970: TimeInterval(startTimestamp))
            let startDate = formatter.string(from: startdateTimeStamp)
            let enddateTimeStamp = Date(timeIntervalSince1970: TimeInterval(endTimestamp))
            let endDate = formatter.string(from: enddateTimeStamp)
            dateLbl.text = "\(startDate) to \(endDate)"
            spendDetailsArray.removeAll()
            reasons.removeAll()
            unitsSold.removeAll()
            Alamofire.request(getSpendDetailsURL, method: .post, parameters: parms, encoding: JSONEncoding.default, headers: Headers).responseJSON { response in
                hideActivityIndicator()
                switch (response.result){
                case .success:
                    let json = JSON(response.result.value!)
                    self.spendDetailsArray = json.arrayValue as Array<Any>
                    for value in self.spendDetailsArray{
                        let dict = value as! JSON
                        self.reasons.append(dict["reason"].string!)
                        self.unitsSold.append(Double(truncating: dict["amount"].number!))
                    }
                    if self.spendDetailsArray.count != 0{
                        self.setChart(dataPoints: self.reasons, values: self.unitsSold)
                    }else{
                        self.showDetails()
                    }
                    self.startTimestamp = Int(Date().timeIntervalSince1970)
                    self.endTimestamp = Int(Date().timeIntervalSince1970)
                case .failure:
                    print("error")
                }
            }
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }
    }
    //MARK:- CALENDAR DELEGATES
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print("did select date \(self.formatter.string(from: date))")
        let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: date)
        selectedTimestamp = Int(tomorrow!.timeIntervalSince1970)
        if isFromClicked == true{
            startTimestamp = selectedTimestamp
        }else if isToClicked == true{
            endTimestamp = selectedTimestamp
        }
    }
    //MARK:- TABLEVIEW DELEGATES
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if unitsSold.count == 0{
           return 1
        }else{
           return unitsSold.count
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let spendTableViewCell = tableView.dequeueReusableCell(withIdentifier: "spendAnalyzerTableViewCell", for: indexPath) as! spendAnalyzerTableViewCell
        spendTableViewCell.selectionStyle = .none
        if unitsSold.count == 0{
            spendTableViewCell.analyzeNameLbl.isHidden = true
            spendTableViewCell.amountLbl.isHidden = true
            spendTableViewCell.colorLbl.isHidden = true
            spendTableViewCell.transacLabel.isHidden = true
            spendTableViewCell.nospendingsLbl.isHidden = false
        }else{
            spendTableViewCell.analyzeNameLbl.isHidden = false
            spendTableViewCell.amountLbl.isHidden = false
            spendTableViewCell.colorLbl.isHidden = false
            spendTableViewCell.transacLabel.isHidden = false
            spendTableViewCell.nospendingsLbl.isHidden = true
            spendTableViewCell.analyzeNameLbl.text = reasons[indexPath.row]
            spendTableViewCell.amountLbl.text = "CSH \(String(unitsSold[indexPath.row]))"
            let indivDict = JSON(spendDetailsArray[indexPath.row])
            spendTableViewCell.transacLabel.text = "Spend CSH \(indivDict["amount"].number!) from CSH \(indivDict["userWalletAmount"].string!)"
            if colors != []{
                spendTableViewCell.colorLbl.backgroundColor = colors[indexPath.row]
            }
        }
        return spendTableViewCell
    }
    //MARK:-HOME CLICKED
    @IBAction func homeClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:-WALLET CLICKED
    @IBAction func walletClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "TransactionsViewController") as! TransactionsViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:-CONTACT CLICKED
    @IBAction func contactClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "FavouriteListViewController") as! FavouriteListViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:-CHAT CLICKED
    @IBAction func chatClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- PROFILE CLICKED
    @IBAction func profileClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- BACK CLICKED
    @IBAction func BackClicked(_ sender: Any) {
       _ = navigationController?.popViewController(animated: true)
    }
    //MARK:- NOTIFICATION CLICKED
    @IBAction func notificationClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
