//
//  HomeViewController.swift
//  CashStash
//
//  Created by apple on 31/01/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class HomeViewController: UIViewController {

    @IBOutlet var userNameLbl: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var amountLbl: UILabel!
    @IBOutlet weak var requestMoneyBtn: UIButton!
    @IBOutlet weak var payNowBtn: UIButton!
    @IBOutlet weak var walletBtn: UIButton!
    @IBOutlet weak var spendAnalyzerBtn: UIButton!
    @IBOutlet weak var shareWalletBtn: UIButton!
    
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = UIColor(red: 5.0/255.0, green: 78.0/255.0, blue: 106.0/255.0, alpha: 1.0)
        
        payNowBtn.layer.shadowColor = UIColor(white: 0.0, alpha: 0.25).cgColor
        payNowBtn.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        payNowBtn.layer.shadowOpacity = 1.0
        payNowBtn.layer.shadowRadius = 3
        payNowBtn.layer.masksToBounds = false
        
        walletBtn.layer.shadowColor = UIColor(white: 0.0, alpha: 0.25).cgColor
        walletBtn.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        walletBtn.layer.shadowOpacity = 1.0
        walletBtn.layer.shadowRadius = 3
        walletBtn.layer.masksToBounds = false

        spendAnalyzerBtn.layer.shadowColor = UIColor(white: 0.0, alpha: 0.25).cgColor
        spendAnalyzerBtn.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        spendAnalyzerBtn.layer.shadowOpacity = 1.0
        spendAnalyzerBtn.layer.shadowRadius = 3
        spendAnalyzerBtn.layer.masksToBounds = false

        shareWalletBtn.layer.shadowColor = UIColor(white: 0.0, alpha: 0.25).cgColor
        shareWalletBtn.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        shareWalletBtn.layer.shadowOpacity = 1.0
        shareWalletBtn.layer.shadowRadius = 3
        shareWalletBtn.layer.masksToBounds = false

        let userName = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "name")!
        userNameLbl.text = String(userName)
//         userNameLbl.text = "Adam Smith"
        self.requestMoneyBtn.isHidden = true
        
    }
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        if NetworkingManager.isConnectedToNetwork(){
            let retrievedToken: String? = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "token")
            let tokenString = "Bearer " + retrievedToken!
            let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
            Alamofire.request(GetUserProfileURL, headers: Headers).responseJSON { response in
//                print(response)
                switch (response.result){
                case .success:
                    let json = JSON(response.result.value!)
                    if let _ = json["message"].string{
                        Helper.alertBox(Mymsg: json["message"].string!, view: self)
                    }else{
                        let userDict = response.result.value! as! NSDictionary
                        let mainWalletDic = userDict["mainWallet"] as! NSDictionary
                        let amountText = NSMutableAttributedString.init(string: "\(String("CSH \(mainWalletDic["amountInMainWallet"] as! NSNumber)"))")
                        amountText.setAttributes([NSAttributedStringKey.font : UIFont(name: "Barlow-Medium", size: 16.0)!,NSAttributedStringKey.foregroundColor : UIColor(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0)], range: NSMakeRange(0, 3))
                        self.amountLbl.attributedText = amountText
                        UserDefaults.standard.setValue((mainWalletDic["amountInMainWallet"] as! NSNumber), forKey: "amount")
                        UserDefaults.standard.synchronize()
                        SharedClass.sharedInstance.saveToUserDefaultsWithBool(flag: (userDict["kycStatus"] as! Bool), key: "kycStatus")
                        SharedClass.sharedInstance.saveToUserDefaultsWithBool(flag: (userDict["bankStatus"] as! Bool), key: "bankStatus")
                    }
                case .failure:
                    print(response.result.error!)
                    break
                }
                debugPrint(response)
            }
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }
        
    }
    //MARK:- REQUEST MONEY CLICKED
    @IBAction func requestMoneyClicked(_ sender: Any) {
        if NetworkingManager.isConnectedToNetwork(){
            showActivityIndicator()
            let retrievedToken: String? = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "token")
            let tokenString = "Bearer " + retrievedToken!
            let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
            Alamofire.request(loadBalanceURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: Headers).responseJSON{ response in
                hideActivityIndicator()
                switch (response.result){
                case .success:
                    let json = JSON(response.result.value!)
                    var mainWalletDict = [String:Any]()
                    mainWalletDict = json["mainWallet"].dictionary! as [String:Any]
                    let amountText = NSMutableAttributedString.init(string: "CSH \(mainWalletDict["amountInMainWallet"]!)")
                    amountText.setAttributes([NSAttributedStringKey.font : UIFont(name: "Barlow-Medium", size: 14.0)!,NSAttributedStringKey.foregroundColor : UIColor(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0)], range: NSMakeRange(0, 3))
                    self.amountLbl.attributedText = amountText
                    let number = json["mainWallet"].dictionary!["amountInMainWallet"]?.number!
                    UserDefaults.standard.setValue(number!, forKey: "amount")
                    UserDefaults.standard.synchronize()
                case .failure:
                    hideActivityIndicator()
                    print("error")
                }
            }
        }
    }
    //MARK:- PAYNOW CLICKED
    @IBAction func payNowClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "QRScanPayViewController") as! QRScanPayViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    
    @IBAction func homeBtnClicked(_ sender: Any) {
    }
    //MARK:-WALLET CLICKED
    @IBAction func walletClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "TransactionsViewController") as! TransactionsViewController
        destVc.isRecentClicked = false
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:-CONTACT CLICKED
    @IBAction func contactClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "FavouriteListViewController") as! FavouriteListViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:-CHAT CLICKED
    @IBAction func chatClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:-PROFILE CLICKED
    @IBAction func profileClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    
    //MARK:- ADD WALLET CLICKED
    @IBAction func addWalletClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "WalletViewController") as! WalletViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:-TRANSACTION CLICKED
    @IBAction func transactionClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "ShareWalletViewController") as! ShareWalletViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:-SPEND ANALYZER CLICKED
    @IBAction func spendAnalyzerClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "SpendAnalyzerViewController") as! SpendAnalyzerViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- NOTIFICATION CLICKED
    @IBAction func notificationClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    
    }

    
}
