//
//  FavouriteListViewController.swift
//  CashStash
//
//  Created by apple on 01/02/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit
import Firebase
import Kingfisher

class FavouriteListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate {

    @IBOutlet var notificationBtn: UIButton!
    @IBOutlet var screenNameLbl: UILabel!
    @IBOutlet var taskBarView: UIView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var favouriteTblVw: UITableView!
    var (phoneNumbersArray,namesArray) = (Array<Any>(),NSMutableArray())
    var (tempPhoneArray,tempAllNamesArray) = (Array<Any>(),NSMutableArray())
    var filtered = [String]()
    let blurView = UIView()
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewDidAppear(true)
        taskBarView.isHidden = false
        notificationBtn.isHidden = false
        
        blurView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        blurView.backgroundColor = AppColors.APP_BLURVIEW_COLOR
        view.addSubview(blurView)
        blurView.isHidden = true
        
    }
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true

        if NetworkingManager.isConnectedToNetwork(){
            //getting all contacts in device
            showActivityIndicator()
            var PhoneNumber = String()
            var allNumbersDict = NSMutableDictionary()
            self.phoneNumbersArray.removeAll()
            self.namesArray.removeAllObjects()
            self.tempPhoneArray.removeAll()
            self.tempAllNamesArray.removeAllObjects()
            Contact.fetchingCoreData(completion: { (contactResults) in
                if contactResults.count == 0{
                    hideActivityIndicator()
                    let alert = UIAlertController(title: "", message: "No access given to contacts", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { UIAlertAction in
                        _ = self.navigationController?.popViewController(animated: true)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }else{
                    Contact.getRegisterUsers(completion: {(RegistersDict) in
                        if let _ = RegistersDict["message"] as? String{
                        }else{
                            for result in contactResults {
                                PhoneNumber = (result.value(forKey: "mobileNumber")!) as! String
                                let contactName = (result.value(forKey: "contactName")!)
                                for (_,value) in RegistersDict.enumerated(){
                                    let contactDict = value.value as! NSMutableDictionary
                                    let numberDict = contactDict.value(forKey: "registers") as! NSMutableDictionary
                                    if (PhoneNumber).contains(numberDict["Contact"] as! String) {
                                        let mobileNo = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "mobile")
                                        if numberDict["Contact"] as? String != mobileNo{
                                            allNumbersDict = ["name":contactName,"image":numberDict["profilePic"] as! String,"phonenumber":numberDict["Contact"] as! String]
                                            self.phoneNumbersArray.append(numberDict["Contact"] as! String)
                                            self.namesArray.add(allNumbersDict)
                                            self.tempAllNamesArray = self.namesArray
                                            self.tempPhoneArray = self.phoneNumbersArray
                                        }
                                    }
                                }
                            }
                        }
                        DispatchQueue.main.async {
                            self.favouriteTblVw.delegate = self
                            self.favouriteTblVw.dataSource = self
                            self.favouriteTblVw.reloadData()
                        }
                        hideActivityIndicator()
                    })
                }
            })
            
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }
    }
    //MARK:- TABLEVIEW DELEGATES
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tempAllNamesArray.count != 0{
            return tempAllNamesArray.count
        }else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: FavouriteTableViewCell = tableView.dequeueReusableCell(withIdentifier:"Cell", for: indexPath as IndexPath) as! FavouriteTableViewCell
        if tempAllNamesArray.count != 0{
            cell.contactNameLbl.isHidden = false
            cell.phoneNumberLbl.isHidden = false
            cell.profilePictureImg.isHidden = false
            cell.payBtn.isHidden = false
            
            let dict = tempAllNamesArray[indexPath.row] as! NSDictionary
            cell.contactNameLbl.text = dict["name"] as! String?
            cell.phoneNumberLbl.text = dict["phonenumber"] as? String
            if dict["image"] as! String != ""{
                let url = URL(string: dict["image"] as! String)
                cell.profilePictureImg.kf.setImage(with: url, placeholder: UIImage(named: "manProfile"), options: nil, progressBlock: nil, completionHandler: nil)
            }else{
                cell.profilePictureImg.image = #imageLiteral(resourceName: "manProfile")
            }
            cell.payBtn.tag = indexPath.row
            cell.payBtn.addTarget(self, action: #selector(payNowClicked), for: .touchUpInside)
//            cell.profilePictureImg.image = dict["image"] as! UIImage?
            
        }else{
            cell.contactNameLbl.isHidden = true
            cell.phoneNumberLbl.isHidden = true
            cell.profilePictureImg.isHidden = true
            cell.payBtn.isHidden = true
            let nooResults_label = UILabel.init(frame:CGRect(x: 0, y: cell.frame.size.height/3, width: self.view.frame.size.width, height: 50))
            nooResults_label.textColor = UIColor.lightGray
            nooResults_label.text = "No Contacts Found"
            nooResults_label.textAlignment = .center
            nooResults_label.numberOfLines = 0
            cell.contentView.addSubview(nooResults_label)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tempAllNamesArray.count != 0{
            return 80
        }else{
            return self.view.frame.size.height-178
        }
    }
    //MARK:- PAYNW CLICKED
    @objc func payNowClicked(sender:UIButton){
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "PayNowViewController") as! PayNowViewController
        let indexPath = IndexPath.init(row: sender.tag, section: 0)
        let cell = favouriteTblVw.cellForRow(at: indexPath) as! FavouriteTableViewCell
//        print(cell.phoneNumberLbl.text!)
        destVc.mobileNumberString = cell.phoneNumberLbl.text!
        destVc.paidToNameString = cell.contactNameLbl.text!
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- SEARCHBAR DELEGATE
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText != ""{ //filtering based on letter typed
            var nameArray = [String]()
            let filteredArray = NSMutableArray()
            var filteredPhoneArray = Array<Any>()
            nameArray.removeAll()
            filteredArray.removeAllObjects()
            filteredPhoneArray.removeAll()
            tempAllNamesArray = namesArray
            for index in tempAllNamesArray{
                let tempDict = index as! NSDictionary
                let name = tempDict["name"] as! String
                nameArray.append(name)
                filtered = searchText.isEmpty ? nameArray : nameArray.filter { (item: String) -> Bool in
                    return item.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
                }
                for i in filtered{
                    if i == name{
                        filteredArray.add(tempDict)
                        let number = tempDict["phonenumber"] as! String
                        filteredPhoneArray.append(number)
                    }
                }
            }
            tempAllNamesArray = filteredArray
            tempPhoneArray = filteredPhoneArray
            self.favouriteTblVw.reloadData()
        }else{
            tempAllNamesArray = namesArray
            tempPhoneArray = phoneNumbersArray
            self.favouriteTblVw.reloadData()
        }
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.favouriteTblVw.reloadData()
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
        self.favouriteTblVw.reloadData()
    }
    
    //MARK:- HOME CLICKED
    @IBAction func homeClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- WALLET CLICKED
    @IBAction func walletClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "TransactionsViewController") as! TransactionsViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- CONTACT CLICKED
    @IBAction func contactClicked(_ sender: Any) {
       
    }
    //MARK:- CHAT CLICKED
    @IBAction func chatClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- PROFILE CLICKED
    @IBAction func profileClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- BACK CLICKED
    @IBAction func backClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func notificationClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
}
