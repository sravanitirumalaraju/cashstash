//
//  ReceivedPaymentViewController.swift
//  CashStash
//
//  Created by apple on 01/02/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit

class ReceivedPaymentViewController: UIViewController,UITabBarDelegate{
    
    @IBOutlet var receivedTabBar: UITabBar!
    
    @IBOutlet var receivedPaymentTbleVw: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell: ReceivedTableViewCell = tableView.dequeueReusableCell(withIdentifier:"Cell", for: indexPath as IndexPath) as! ReceivedTableViewCell
//        return cell
//    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    @IBAction func homeClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    
    @IBAction func walletClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "TransactionsViewController") as! TransactionsViewController
        self.navigationController?.pushViewController(destVc, animated: true)
//        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "WalletViewController") as! WalletViewController
//        self.navigationController?.pushViewController(destVc, animated: true)
    }
    
    @IBAction func contactClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "FavouriteListViewController") as! FavouriteListViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    
    @IBAction func chatClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    
    @IBAction func profileClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
   
}
