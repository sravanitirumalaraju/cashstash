//
//  MyQRCodeViewController.swift
//  CashStash
//
//  Created by apple on 4/30/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit

class MyQRCodeViewController: UIViewController {

    @IBOutlet weak var payNameLbl: UILabel!
    @IBOutlet weak var myQRCodeImgVw: UIImageView!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var QRbackground: UIView!
    
    var nameString = String()
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        payNameLbl.text = "To pay \(nameString)"
        let mobileNumber = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "mobile")
        let generatedImage = generateQRCode(from: "\(mobileNumber!)@cashstash.me")
        myQRCodeImgVw.image = generatedImage
    }
    //MARK:- GENERATE QR CODE
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.isoLatin1)
        if let filter = CIFilter(name: "CIQRCodeGenerator"){
            filter.setValue(data, forKey: "inputMessage")
            
            guard let qrCodeImage = filter.outputImage else { return nil }
            let scaleX = myQRCodeImgVw.frame.size.width / qrCodeImage.extent.size.width
            let scaleY = myQRCodeImgVw.frame.size.height / qrCodeImage.extent.size.height
            
            let transform = CGAffineTransform(scaleX: scaleX, y: scaleY)
            
            if let output = filter.outputImage?.transformed(by: transform){
                return UIImage(ciImage: output)
            }
        }
        return nil
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK:- BACK CLICKED
    @IBAction func backClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func shareClicked(_ sender: Any) {
        
        UIGraphicsBeginImageContextWithOptions(QRbackground.frame.size, true, 0.0)
        self.QRbackground.drawHierarchy(in: QRbackground.bounds, afterScreenUpdates: false)
        let imageToShare = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let activityViewController = UIActivityViewController(activityItems: [imageToShare!], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ,UIActivityType.saveToCameraRoll]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
}
