//
//  KYCViewController.swift
//  CashStash
//
//  Created by apple on 7/26/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class KYCViewController: UIViewController,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIScrollViewDelegate {

    @IBOutlet weak var segmentBar: UISegmentedControl!
    @IBOutlet weak var KYCScrollview: UIScrollView!
    
    var BankDetailsView = UIView()
    var DocumentsDetailsView = UIView()
    let cameraView = UIView()
    let BorderView = CAShapeLayer()
    var imagePicker = UIImagePickerController()
    var cameraButton = UIButton()
    var selectIDTypeBtn = UIButton()
    var selectIdTypeView = UIView()
    var docSaveButton = UIButton()
    let accountHolderNameTxtFld = FloatLabelTextField()
    let bankAccountNoTxtFld = FloatLabelTextField()
    let ABARoutingTxtFld = FloatLabelTextField()
    let bankAccountTypeTxtFld = FloatLabelTextField()
    let bankNameTxtFld = FloatLabelTextField()
    let SSNBankTxtfld = FloatLabelTextField()
    let SSNTxtFld = FloatLabelTextField()
    let retrievedToken: String? = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "token")
    var selectedImage: UIImage? = nil
    var KYCDictionary = NSDictionary()
    var BankDictionary = NSDictionary()
    var bankIdTypeView = UIView()
    let bankSaveButton = UIButton()
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        cameraView.isHidden = true
        docSaveButton.isHidden = true
        let viewTap = UITapGestureRecognizer(target: self, action: #selector(self.mainViewTapped(_:)))
        view.addGestureRecognizer(viewTap)
        ScrollViewContent()
        KYCScrollview.delegate = self
        segmentBar.addTarget(self, action: #selector(segmentedControlValueChanged(segment:)), for: .valueChanged)
    }
    //MARK:- SCROLL VIEW CONTENT
    func ScrollViewContent(){
        if #available(iOS 11.0, *) {
            BankDetailsView.frame = CGRect(x: 0, y: 0, width: CGFloat(self.view.frame.size.width), height: CGFloat(self.KYCScrollview.frame.size.height + (UIApplication.shared.keyWindow?.safeAreaInsets.top)! + (UIApplication.shared.keyWindow?.safeAreaInsets.bottom)!))
            
            DocumentsDetailsView.frame = CGRect(x: CGFloat(self.view.frame.size.width), y: 0, width: CGFloat(self.view.frame.size.width), height: CGFloat(self.KYCScrollview.frame.size.height + (UIApplication.shared.keyWindow?.safeAreaInsets.top)! + (UIApplication.shared.keyWindow?.safeAreaInsets.bottom)!))
        }else{
            BankDetailsView.frame = CGRect(x: 0, y: 0, width: CGFloat(self.view.frame.size.width), height: CGFloat(self.KYCScrollview.frame.size.height))
            
            DocumentsDetailsView.frame = CGRect(x: CGFloat(self.view.frame.size.width), y: 0, width: CGFloat(self.view.frame.size.width), height: CGFloat(self.KYCScrollview.frame.size.height))
        }
        BankDetailsView.backgroundColor =  AppColors.APP_BACKGROUND_COLOR
        KYCScrollview.addSubview(BankDetailsView)
        
        BankDetailsContent()
        
        DocumentsDetailsView.backgroundColor =  AppColors.APP_BACKGROUND_COLOR
        KYCScrollview.addSubview(DocumentsDetailsView)
        
        DocumentsContent()
        
        self.KYCScrollview.contentSize = CGSize(width: CGFloat(self.view.frame.size.width) * 2, height: (self.KYCScrollview.frame.size.height))
    }
    //MARK:- BANK DETAILS CONTENT
    @objc func BankDetailsContent(){
        accountHolderNameTxtFld.frame = CGRect(x: 20, y: 20, width: self.BankDetailsView.frame.size.width - 40, height: 40)
        accountHolderNameTxtFld.autocorrectionType = .no
        accountHolderNameTxtFld.placeholder = "Account Holder Name"
        accountHolderNameTxtFld.delegate = self
        if BankDictionary.count != 0{
            accountHolderNameTxtFld.text = BankDictionary["accountHolderName"] as? String
        }
        BankDetailsView.addSubview(accountHolderNameTxtFld)
        
        let lineLbl1 = UILabel.init(frame: CGRect(x: 20, y: accountHolderNameTxtFld.frame.origin.y + accountHolderNameTxtFld.frame.size.height, width: self.BankDetailsView.frame.size.width - 40, height: 1))
        lineLbl1.backgroundColor = UIColor(red: 238.0/255.0, green: 228.0/255.0, blue: 249.0/255.0, alpha: 1.0)
        BankDetailsView.addSubview(lineLbl1)
        
        bankAccountNoTxtFld.frame = CGRect(x: 20, y: lineLbl1.frame.origin.y + lineLbl1.frame.size.height + 20, width: self.BankDetailsView.frame.size.width - 40, height: 40)
        bankAccountNoTxtFld.autocorrectionType = .no
        bankAccountNoTxtFld.placeholder = "Bank Account Number"
        bankAccountNoTxtFld.delegate = self
        if BankDictionary.count != 0{
            bankAccountNoTxtFld.text = BankDictionary["bankAccountNumber"] as? String
        }
        BankDetailsView.addSubview(bankAccountNoTxtFld)
        
        let lineLbl2 = UILabel.init(frame: CGRect(x: 20, y: bankAccountNoTxtFld.frame.origin.y + bankAccountNoTxtFld.frame.size.height, width: self.BankDetailsView.frame.size.width - 40, height: 1))
        lineLbl2.backgroundColor = UIColor(red: 238.0/255.0, green: 228.0/255.0, blue: 249.0/255.0, alpha: 1.0)
        BankDetailsView.addSubview(lineLbl2)
        
        ABARoutingTxtFld.frame = CGRect(x: 20, y: lineLbl2.frame.origin.y + lineLbl2.frame.size.height + 20, width: self.BankDetailsView.frame.size.width - 40, height: 40)
        ABARoutingTxtFld.autocorrectionType = .no
        ABARoutingTxtFld.placeholder = "ABA Routing Number"
        ABARoutingTxtFld.delegate = self
        if BankDictionary.count != 0{
            ABARoutingTxtFld.text = BankDictionary["abaRoutingNumber"] as? String
        }
        BankDetailsView.addSubview(ABARoutingTxtFld)
        
        let lineLbl3 = UILabel.init(frame: CGRect(x: 20, y: ABARoutingTxtFld.frame.origin.y + ABARoutingTxtFld.frame.size.height, width: self.BankDetailsView.frame.size.width - 40, height: 1))
        lineLbl3.backgroundColor = UIColor(red: 238.0/255.0, green: 228.0/255.0, blue: 249.0/255.0, alpha: 1.0)
        BankDetailsView.addSubview(lineLbl3)
        
        bankAccountTypeTxtFld.frame = CGRect(x: 20, y: lineLbl3.frame.origin.y + lineLbl3.frame.size.height + 20, width: self.BankDetailsView.frame.size.width - 40, height: 40)
        bankAccountTypeTxtFld.isUserInteractionEnabled = true
        bankAccountTypeTxtFld.autocorrectionType = .no
        bankAccountTypeTxtFld.placeholder = "Bank Account Type"
        bankAccountTypeTxtFld.delegate = self
        if BankDictionary.count != 0{
            bankAccountTypeTxtFld.text = BankDictionary["bankAccountType"] as? String
        }
        BankDetailsView.addSubview(bankAccountTypeTxtFld)
        
        let bankAccountTypeBtn = UIButton.init(frame: CGRect(x: 0, y: 0, width: self.bankAccountTypeTxtFld.frame.size.width, height: self.bankAccountTypeTxtFld.frame.size.height))
        bankAccountTypeBtn.setTitle("", for: .normal)
        bankAccountTypeBtn.addTarget(self, action: #selector(bankAccountTypeClicked), for: .touchUpInside)
        bankAccountTypeTxtFld.addSubview(bankAccountTypeBtn)
        
        let lineLbl4 = UILabel.init(frame: CGRect(x: 20, y: bankAccountTypeTxtFld.frame.origin.y + bankAccountTypeTxtFld.frame.size.height, width: self.BankDetailsView.frame.size.width - 40, height: 1))
        lineLbl4.backgroundColor = UIColor(red: 238.0/255.0, green: 228.0/255.0, blue: 249.0/255.0, alpha: 1.0)
        BankDetailsView.addSubview(lineLbl4)
        
        bankNameTxtFld.frame = CGRect(x: 20, y: lineLbl4.frame.origin.y + lineLbl4.frame.size.height + 20, width: self.BankDetailsView.frame.size.width - 40, height: 40)
        bankNameTxtFld.autocorrectionType = .no
        bankNameTxtFld.placeholder = "Bank Name"
        bankNameTxtFld.delegate = self
//        bankSaveButton.setTitle("SAVE", for: .normal)
        if BankDictionary.count != 0{
            bankNameTxtFld.text = BankDictionary["bankName"] as? String
//            bankSaveButton.setTitle("EDIT", for: .normal)
        }
        BankDetailsView.addSubview(bankNameTxtFld)
        
        let lineLbl5 = UILabel.init(frame: CGRect(x: 20, y: bankNameTxtFld.frame.origin.y + bankNameTxtFld.frame.size.height, width: self.BankDetailsView.frame.size.width - 40, height: 1))
        lineLbl5.backgroundColor = UIColor(red: 238.0/255.0, green: 228.0/255.0, blue: 249.0/255.0, alpha: 1.0)
        BankDetailsView.addSubview(lineLbl5)
        
        SSNBankTxtfld.frame = CGRect(x: 20, y: lineLbl5.frame.origin.y + lineLbl5.frame.size.height + 20, width: self.BankDetailsView.frame.size.width - 40, height: 40)
        SSNBankTxtfld.autocorrectionType = .no
        SSNBankTxtfld.placeholder = "Social Security Number"
        SSNBankTxtfld.delegate = self
        bankSaveButton.setTitle("SAVE", for: .normal)
//        print(BankDictionary)
        if BankDictionary.count != 0{
            SSNBankTxtfld.text = BankDictionary["ssnNumber"] as? String
            bankSaveButton.setTitle("EDIT", for: .normal)
        }
        BankDetailsView.addSubview(SSNBankTxtfld)
        
        bankSaveButton.frame = CGRect(x: 0, y: SSNBankTxtfld.frame.origin.y + SSNBankTxtfld.frame.size.height + 20, width: 200, height: 40)
        bankSaveButton.center.x = self.BankDetailsView.center.x
        bankSaveButton.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 18.0)
        bankSaveButton.layer.cornerRadius = bankSaveButton.frame.size.height/2
        bankSaveButton.clipsToBounds = true
        bankSaveButton.backgroundColor = AppColors.APP_COLOR_BLUE
        bankSaveButton.addTarget(self, action: #selector(bankSaveClicked), for: .touchUpInside)
        BankDetailsView.addSubview(bankSaveButton)
    }
    //MARK:- BANK ACCOUNT TYPE CLICKED
    @objc func bankAccountTypeClicked(){
        bankIdTypeView.isHidden = false
        self.view.endEditing(true)
        bankIdTypeView.frame = CGRect(x: 0, y: bankAccountTypeTxtFld.frame.origin.y + bankAccountTypeTxtFld.frame.size.height + 2, width: 250, height: 100)
        bankIdTypeView.center.x = self.BankDetailsView.center.x
        bankIdTypeView.backgroundColor = UIColor.white
        bankIdTypeView.layer.shadowColor = UIColor.black.cgColor
        bankIdTypeView.layer.shadowOpacity = 0.5
        bankIdTypeView.layer.shadowOffset = CGSize.zero
        bankIdTypeView.layer.shadowRadius = 5
        BankDetailsView.addSubview(bankIdTypeView)
        
        let checkingBtn = UIButton.init(frame: CGRect(x: 0, y: 0, width: bankIdTypeView.frame.size.width, height: 50))
        checkingBtn.setTitle("Checking", for: .normal)
        checkingBtn.setTitleColor(UIColor(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0), for: .normal)
        checkingBtn.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 16.0)
        checkingBtn.addTarget(self, action: #selector(checkingClicked(_:)), for: .touchUpInside)
        bankIdTypeView.addSubview(checkingBtn)
        
        let savingsBtn = UIButton.init(frame: CGRect(x: 0, y: checkingBtn.frame.origin.y + checkingBtn.frame.size.height, width: bankIdTypeView.frame.size.width, height: 50))
        savingsBtn.setTitle("Savings", for: .normal)
        savingsBtn.setTitleColor(UIColor(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0), for: .normal)
        savingsBtn.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 16.0)
        savingsBtn.addTarget(self, action: #selector(savingsClicked(_:)), for: .touchUpInside)
        bankIdTypeView.addSubview(savingsBtn)
    }
    //MARK:- CHECKING CLICKED
    @objc func checkingClicked(_ sender : UIButton){
        bankIdTypeView.isHidden = true
        bankAccountTypeTxtFld.text = "Checking"
    }
    //MARK:- SAVINGS CLICKED
    @objc func savingsClicked(_ sender : UIButton){
        bankIdTypeView.isHidden = true
        bankAccountTypeTxtFld.text = "Savings"
    }
    //MARK:- DOCUMENT CONTENT
    @objc func DocumentsContent(){
        SSNTxtFld.frame = CGRect(x: 20, y: 30, width: self.DocumentsDetailsView.frame.size.width - 40, height: 40)
        SSNTxtFld.placeholder = "Social Security Number"
        SSNTxtFld.delegate = self
        if KYCDictionary.count != 0 { // KYC document is updated
            cameraView.isHidden = false
            docSaveButton.isHidden = false
            SSNTxtFld.isUserInteractionEnabled = false
            SSNTxtFld.text = KYCDictionary["ssn"] as? String
        }
        DocumentsDetailsView.addSubview(SSNTxtFld)
        
        let lineLbl1 = UILabel.init(frame: CGRect(x: 20, y: SSNTxtFld.frame.origin.y + SSNTxtFld.frame.size.height, width: self.DocumentsDetailsView.frame.size.width - 40, height: 1))
        lineLbl1.backgroundColor = UIColor(red: 238.0/255.0, green: 228.0/255.0, blue: 249.0/255.0, alpha: 1.0)
        DocumentsDetailsView.addSubview(lineLbl1)
        
        selectIDTypeBtn.frame = CGRect(x: 0, y: lineLbl1.frame.origin.y + lineLbl1.frame.size.height + 20, width: 250, height: 40)
        selectIDTypeBtn.center.x = self.view.center.x
        selectIDTypeBtn.backgroundColor = UIColor.white
        if KYCDictionary.count != 0 { // KYC document is updated
            selectIDTypeBtn.isUserInteractionEnabled = false
            selectIDTypeBtn.setTitle(KYCDictionary["kycType"] as? String, for: .normal)
        }else{ // KYC document is not updated
            selectIDTypeBtn.setTitle("Select ID Type", for: .normal)
        }
        selectIDTypeBtn.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 18.0)
        selectIDTypeBtn.setTitleColor(UIColor(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0), for: .normal)
        selectIDTypeBtn.addTarget(self, action: #selector(selectIDClicked(_:)), for: .touchUpInside)
        DocumentsDetailsView.addSubview(selectIDTypeBtn)
        
        cameraView.frame = CGRect(x: 0, y: selectIDTypeBtn.frame.origin.y + selectIDTypeBtn.frame.size.height + 30, width: 200, height: 180)
        cameraView.center.x = self.view.center.x
        DocumentsDetailsView.addSubview(cameraView)
        
        BorderView.strokeColor = UIColor(red: 238.0/255.0, green: 228.0/255.0, blue: 249.0/255.0, alpha: 1.0).cgColor
        BorderView.lineDashPattern = [12, 5]
        BorderView.lineWidth = 2
        BorderView.fillColor = UIColor.clear.cgColor
        BorderView.strokeStart = 0
        BorderView.strokeEnd = 1
        BorderView.frame = cameraView.bounds
        BorderView.path = UIBezierPath(rect: BorderView.bounds).cgPath
        cameraView.layer.addSublayer(BorderView)
        
        cameraButton.frame = CGRect(x: 50, y: 20, width: 100, height: 100)
        if KYCDictionary.count != 0{ // KYC document is updated
            docSaveButton.isHidden = true
            if KYCDictionary["kycImage"] as? String != ""{
                if let kycImageString = KYCDictionary["kycImage"] as? String{
                    cameraButton.isUserInteractionEnabled = false
                    let url = URL(string: kycImageString)
                    self.cameraButton.kf.setImage(with: url, for: .normal, placeholder: #imageLiteral(resourceName: "camera"), options: nil, progressBlock: nil, completionHandler: nil)
                }
            }
        }else{ // KYC document is not updated
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
            cameraView.addGestureRecognizer(tap)
            cameraButton.addTarget(self, action: #selector(cameraBtnClicked(_:)), for: .touchUpInside)
            cameraButton.setImage(#imageLiteral(resourceName: "camera"), for: .normal)
        }
        cameraView.addSubview(cameraButton)
        
        let selectIdLbl = UILabel.init(frame: CGRect(x: 0, y: cameraButton.frame.origin.y + cameraButton.frame.size.height + 20, width: cameraView.frame.size.width, height: 20))
        selectIdLbl.text = "Select Id"
        selectIdLbl.textAlignment = .center
        selectIdLbl.textColor = UIColor(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0)
        selectIdLbl.font = UIFont(name: "Nunito-Regular", size: 16.0)
        cameraView.addSubview(selectIdLbl)
        if KYCDictionary.count != 0{ // KYC document is updated
            selectIdLbl.isHidden = true
        }
        
        docSaveButton.frame = CGRect(x: 0, y: cameraView.frame.origin.y + cameraView.frame.size.height + 40, width: 200, height: 40)
        docSaveButton.center.x = self.view.center.x
        docSaveButton.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 18.0)
        docSaveButton.setTitle("SAVE", for: .normal)
        docSaveButton.layer.cornerRadius = docSaveButton.frame.size.height/2
        docSaveButton.clipsToBounds = true
        docSaveButton.backgroundColor = AppColors.APP_COLOR_BLUE
        docSaveButton.addTarget(self, action: #selector(documentsSaveClicked), for: .touchUpInside)
        DocumentsDetailsView.addSubview(docSaveButton)
    }
    //MARK:- SEGMENT CONTROL VALUE CHANGED
    @objc func segmentedControlValueChanged(segment: UISegmentedControl) {
        if segment.selectedSegmentIndex == 0 {
            let frame :CGRect = CGRect(x: CGFloat(self.view.frame.width) * 0, y: 0, width: CGFloat(self.view.frame.width), height: KYCScrollview.frame.size.height)
            self.KYCScrollview.scrollRectToVisible(frame, animated: true)
        }else{
            let frame :CGRect = CGRect(x: CGFloat(self.view.frame.width) * 1, y: 0, width: CGFloat(self.view.frame.width), height: KYCScrollview.frame.size.height)
            self.KYCScrollview.scrollRectToVisible(frame, animated: true)
        }
    }
    //MARK:- SCROLLVIEW DELEGATES
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if KYCScrollview.contentOffset.x == 0{
            segmentBar.selectedSegmentIndex = 0
        } else {
            segmentBar.selectedSegmentIndex = 1
        }
    }
    
    //MARK:- CHOOSE FROM CAMERA
    func chooseFromCamera() {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.cameraCaptureMode = .photo
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    //MARK:- CHOOSE FROM LIBRARY
    func chooseFromPhotoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    //MARK:- IMAGE PICKER DELEGATES
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        selectedImage = info[UIImagePickerControllerEditedImage] as? UIImage
        self.cameraButton.setImage(selectedImage, for: .normal)
        self.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    //MARK:- HADLETAP TAGGESTURE
    @objc func handleTap(_ sender:UITapGestureRecognizer){
        let actionSheetController: UIAlertController = UIAlertController (title: "Choose an Option", message: nil, preferredStyle: .actionSheet)
        actionSheetController.addAction( UIAlertAction (title: "Cancel", style: .cancel, handler: nil))
        actionSheetController.addAction( UIAlertAction (title: "Photo Library", style: .default, handler:{ (alert: UIAlertAction!) in self.chooseFromPhotoLibrary()}
        ))
        actionSheetController.addAction( UIAlertAction (title: "Take Picture", style: .default, handler: {(alert: UIAlertAction!) in self.chooseFromCamera()}
        ))
        self.view.endEditing(true)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    //MARK:- CAMERA BUTTON CLICKED
    @IBAction func cameraBtnClicked(_ sender: Any) {
        let actionSheetController: UIAlertController = UIAlertController (title: "Choose an Option", message: nil, preferredStyle: .actionSheet)
        actionSheetController.addAction( UIAlertAction (title: "Cancel", style: .cancel, handler: nil))
        actionSheetController.addAction( UIAlertAction (title: "Photo Library", style: .default, handler:{ (alert: UIAlertAction!) in self.chooseFromPhotoLibrary()}
        ))
        actionSheetController.addAction( UIAlertAction (title: "Take Picture", style: .default, handler: {(alert: UIAlertAction!) in self.chooseFromCamera()}
        ))
        self.view.endEditing(true)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    //MARK:- SELECT ID CLICKED
    @IBAction func selectIDClicked(_ sender: Any) {
        selectIdTypeView.isHidden = false
        selectIdTypeView.frame = CGRect(x: selectIDTypeBtn.frame.origin.x, y: selectIDTypeBtn.frame.origin.y + selectIDTypeBtn.frame.size.height + 2, width: selectIDTypeBtn.frame.size.width, height: 100)
        selectIdTypeView.backgroundColor = UIColor.white
        selectIdTypeView.layer.shadowColor = UIColor.black.cgColor
        selectIdTypeView.layer.shadowOpacity = 0.5
        selectIdTypeView.layer.shadowOffset = CGSize.zero
        selectIdTypeView.layer.shadowRadius = 5
        DocumentsDetailsView.addSubview(selectIdTypeView)
        
        let passportBtn = UIButton.init(frame: CGRect(x: 0, y: 0, width: selectIdTypeView.frame.size.width, height: 50))
        passportBtn.setTitle("Passport", for: .normal)
        passportBtn.setTitleColor(UIColor(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0), for: .normal)
        passportBtn.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 16.0)
        passportBtn.addTarget(self, action: #selector(passportClicked(_:)), for: .touchUpInside)
        selectIdTypeView.addSubview(passportBtn)
        
        let licenseBtn = UIButton.init(frame: CGRect(x: 0, y: passportBtn.frame.origin.y + passportBtn.frame.size.height, width: selectIdTypeView.frame.size.width, height: 50))
        licenseBtn.setTitle("Driver's License", for: .normal)
        licenseBtn.setTitleColor(UIColor(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0), for: .normal)
        licenseBtn.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 16.0)
        licenseBtn.addTarget(self, action: #selector(licenseClicked(_:)), for: .touchUpInside)
        selectIdTypeView.addSubview(licenseBtn)
    }
    //MARK:- PASSPORT CLICKED
    @objc func passportClicked(_ sender : UIButton) {
        selectIdTypeView.isHidden = true
        selectIDTypeBtn.setTitle("Passport", for: .normal)
        cameraView.isHidden = false
        docSaveButton.isHidden = false
    }
    //MARK:- LICENSE CLICKED
    @objc func licenseClicked(_ sender : UIButton){
        selectIdTypeView.isHidden = true
        selectIDTypeBtn.setTitle("Driving License", for: .normal)
        cameraView.isHidden = false
        docSaveButton.isHidden = false
    }
    //MARK:- MAIN VIEW TAPPED
    @objc func mainViewTapped(_ sender: UITapGestureRecognizer){
        selectIdTypeView.isHidden = true
        bankIdTypeView.isHidden = true
        self.view.endEditing(true)
    }
   
    //MARK:- BANK SAVE CLICKED
    @objc func bankSaveClicked(){
        if NetworkingManager.isConnectedToNetwork(){
            if accountHolderNameTxtFld.text! == "" && bankAccountNoTxtFld.text! == "" && ABARoutingTxtFld.text! == "" && bankAccountTypeTxtFld.text! == "" && bankNameTxtFld.text! == "" && SSNBankTxtfld.text! == ""{
                Helper.alertBox(Mymsg: "please enter all fields", view: self)
            }else if accountHolderNameTxtFld.text! == ""{
                Helper.alertBox(Mymsg: "Enter Account Holder Name", view: self)
            }else if bankAccountNoTxtFld.text! == ""{
                Helper.alertBox(Mymsg: "Enter Bank Account Number", view: self)
            }else if ABARoutingTxtFld.text! == ""{
                Helper.alertBox(Mymsg: "Enter ABA Routing Number", view: self)
            }else if bankAccountTypeTxtFld.text! == ""{
                Helper.alertBox(Mymsg: "Enter Bank Account Type", view: self)
            }else if bankNameTxtFld.text! == ""{
                Helper.alertBox(Mymsg: "Enter Bank Name", view: self)
            }else if SSNBankTxtfld.text! == ""{
                Helper.alertBox(Mymsg: "Enter Social Security Number", view: self)
            }
            else{
                showActivityIndicator()
                let tokenString = "Bearer " + retrievedToken!
                let Headers: HTTPHeaders = ["Authorization": tokenString, "Content-Type": "application/json"]
                let detailsParams = ["accountHolderName":accountHolderNameTxtFld.text!,"bankAccountNumber":bankAccountNoTxtFld.text!,"abaRoutingNumber":ABARoutingTxtFld.text!,"bankAccountType":bankAccountTypeTxtFld.text!,"bankName":bankNameTxtFld.text!,"ssnNumber":SSNBankTxtfld.text!] as [String : Any]
                Alamofire.request(bankDetailsURL, method: .post, parameters: detailsParams, encoding: JSONEncoding.default, headers: Headers).responseJSON { response in
                    hideActivityIndicator()
                    switch(response.result) {
                    case .success (let value):
                        let json = JSON(value)
                        if response.response?.statusCode == 200{
                            Helper.alertBox(Mymsg: json["message"].string!, view: self)
                        }else{
                            Helper.alertBox(Mymsg: json["message"].string!, view: self)
                        }
                    case .failure(_):
                        hideActivityIndicator()
                        print(response.result.error!)
                        break
                    }
                    debugPrint(response)
                }
            }
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }
    }
    //MARK:- DOCUMENT SAVE CLICKED
    @objc func documentsSaveClicked(){
        if NetworkingManager.isConnectedToNetwork(){
            if SSNTxtFld.text! == "" && selectIDTypeBtn.titleLabel?.text! == ""{
                Helper.alertBox(Mymsg: "please enter all fields", view: self)
            }else if SSNTxtFld.text! == ""{
                Helper.alertBox(Mymsg: "Enter Social Security Number", view: self)
            }else if selectIDTypeBtn.titleLabel?.text! == ""{
                Helper.alertBox(Mymsg: "Select ID Proof Type", view: self)
            }else if cameraButton.currentImage == #imageLiteral(resourceName: "camera"){
                Helper.alertBox(Mymsg: "Select ID Proof Image", view: self)
            }else{
                showActivityIndicator()
                let tokenString = "Bearer " + retrievedToken!
                let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
                let URL = try! URLRequest(url: "\(KycDetailsURL)", method: .post, headers: Headers)
                var parameters = [String:AnyObject]()
                parameters = ["ssn":SSNTxtFld.text!,"kycType":selectIDTypeBtn.titleLabel?.text!] as [String : AnyObject]
                Alamofire.upload(multipartFormData: { multipartFormData in
                    multipartFormData.append(UIImageJPEGRepresentation(self.selectedImage!,0.3)!, withName: "file", fileName: "file.png", mimeType: "image/jpg/jpeg/png")
                    for (key, value) in parameters {
                        multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
                    }
                }, with: URL, encodingCompletion: {
                    encodingResult in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            let json = JSON(response.result.value!)
                            if response.response?.statusCode == 200{
                                Helper.alertBox(Mymsg: json["message"].string!, view: self)
                            } else {
                                Helper.alertBox(Mymsg: json["message"].string!, view: self)
                            }
                            hideActivityIndicator()
                        }
                    case .failure(let encodingError):
                        hideActivityIndicator()
                        print(encodingError)
                    }
                })
            }
        }else{
           NetworkingManager.netWorkFailed(view: self)
        }
    }
    //MARK:-TETXFIELD DELEGATES
    func textFieldDidBeginEditing(_ textField: UITextField) {
        bankIdTypeView.isHidden = true
        if self.view.frame.size.height <= 667{
            if textField == bankAccountTypeTxtFld{
                self.view.frame.origin.y -= 50
            }else if textField == bankNameTxtFld{
                self.view.frame.origin.y -= 50
            }else if textField == SSNBankTxtfld{
                self.view.frame.origin.y -= 50
            }
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if self.view.frame.size.height <= 667{
            if textField == bankAccountTypeTxtFld{
                self.view.frame.origin.y += 50
            }else if textField == bankNameTxtFld{
                self.view.frame.origin.y += 50
            }else if textField == SSNBankTxtfld{
                self.view.frame.origin.y += 50
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK:- BACK CLICKED
    @IBAction func backClicked(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
}
