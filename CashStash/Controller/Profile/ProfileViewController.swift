//
//  ProfileViewController.swift
//  CashStash
//
//  Created by apple on 01/02/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher

class ProfileViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate  {

    @IBOutlet weak var KYCButton: UIButton!
    @IBOutlet var profileView: UIView!
    @IBOutlet var KYCView: UIView!
    @IBOutlet var userName: UILabel!
    @IBOutlet var changePasswd: UITextField!
    @IBOutlet var profileButton: UIButton!
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var saveButton: UIButton!
    @IBOutlet var mainBalanceLbl: UILabel!
    
    var imagePicker = UIImagePickerController()
    var passwordTxt = String()
    let blurView = UIView()
    var userId = String()
    let retrievedToken: String? = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "token")
    var bankDict = NSDictionary()
    var kycDict = NSDictionary()
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        userId = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "userId")!
        blurView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        blurView.backgroundColor = AppColors.APP_BLURVIEW_COLOR
        view.addSubview(blurView)
        blurView.isHidden = true
        changePasswd.isUserInteractionEnabled = true
        changePasswd.delegate = self
        profileButton.isUserInteractionEnabled = true
        imagePicker.delegate = self
        profileImageView.layer.cornerRadius = profileImageView.frame.size.height/2
        profileImageView.clipsToBounds = true
        saveButton.layer.cornerRadius = saveButton.frame.size.height/2
        saveButton.clipsToBounds = true
        
        profileView.layer.shadowColor = UIColor(white: 0.0, alpha: 0.25).cgColor
        profileView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        profileView.layer.shadowOpacity = 1.0
        profileView.layer.shadowRadius = 10
        profileView.layer.masksToBounds = false
        
        KYCButton.layer.shadowColor = UIColor(white: 0.0, alpha: 0.25).cgColor
        KYCButton.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        KYCButton.layer.shadowOpacity = 1.0
        KYCButton.layer.shadowRadius = 10
        KYCButton.layer.masksToBounds = false
    }
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        fetchUserProfileDetails()
        self.navigationController?.isNavigationBarHidden = true
    }
    //MARK:- FETCH USER PROFILE DETAILS
    func fetchUserProfileDetails(){
        if NetworkingManager.isConnectedToNetwork(){
            showActivityIndicator()
            let tokenString = "Bearer " + retrievedToken!
            let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
            Alamofire.request(GetUserProfileURL, headers: Headers).responseJSON { response in
                hideActivityIndicator()
                switch (response.result){
                case .success:
                    let json = JSON(response.result.value!)
                    if let _ = json["message"].string{
                        Helper.alertBox(Mymsg: json["message"].string!, view: self)
                    }else{
                        let userDict = response.result.value! as! NSDictionary
                        let mainWalletDic = userDict["mainWallet"] as! NSDictionary
                        self.kycDict = userDict["kycDetails"] as! NSDictionary
                        self.bankDict = userDict["userBankDetails"] as! NSDictionary
                        self.mainBalanceLbl.text = String("CSH \(mainWalletDic["amountInMainWallet"] as! NSNumber)")
                        UserDefaults.standard.setValue((mainWalletDic["amountInMainWallet"] as! NSNumber), forKey: "amount")
                        UserDefaults.standard.synchronize()
                        self.userName.text = userDict["name"] as? String
                        SharedClass.sharedInstance.saveToUserDefaultsWithString(string: userDict["name"] as! String, key: "name")
                        if userDict["userImage"] as? String != ""{
                            if let userImageString = userDict["userImage"] as? String{
                                let url = URL(string: userImageString)
                                self.profileImageView.kf.setImage(with: url, placeholder: UIImage(named: "manProfile"), options: nil, progressBlock: nil, completionHandler: nil)
                            }
                        }
                    }
                case .failure:
                    hideActivityIndicator()
                    print(response.result.error!)
                    break
                }
                debugPrint(response)
            }
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }
    }
    //MARK:-PREPARE FOR SEGUE
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if NetworkingManager.isConnectedToNetwork(){
            let destVC: KYCViewController = segue.destination as! KYCViewController
            if (self.kycDict["ssn"] as! String) != ""{
                destVC.KYCDictionary = self.kycDict
            }
            if (self.bankDict["accountHolderName"] as! String) != ""{
                destVC.BankDictionary = self.bankDict
            }
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }
    }
    //MARK:- TEXTFIELD DELEGATES
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return  true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        saveButton.isHidden = false
        changePasswd.autocorrectionType = .no
        changePasswd.isSecureTextEntry = true
        changePasswd.text = ""
        changePasswd.placeholder = "Enter Password"
        changePasswd.becomeFirstResponder()
    }
    //MARK:- EDIT CLICKED
    @IBAction func editClicked(_ sender: Any) {
        profileBtnClicked(UIButton.self)
    }
    //MARK:-PROFILE BUTTON CLICKED
    @IBAction func profileBtnClicked(_ sender: Any) {
        let actionSheetController: UIAlertController = UIAlertController (title: "Choose an Option", message: nil, preferredStyle: .actionSheet)
        actionSheetController.addAction( UIAlertAction (title: "Cancel", style: .cancel, handler: nil))
        actionSheetController.addAction( UIAlertAction (title: "Photo Library", style: .default, handler:{ (alert: UIAlertAction!) in self.chooseFromPhotoLibrary()}
        ))
        actionSheetController.addAction( UIAlertAction (title: "Take Picture", style: .default, handler: {(alert: UIAlertAction!) in self.chooseFromCamera()}
        ))
        self.view.endEditing(true)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    //MARK:- CHOOSE FROM CAMERA
    func chooseFromCamera() {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.cameraCaptureMode = .photo
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    //MARK:- CHOOSE FROM LIBRARY
    func chooseFromPhotoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    //MARK:- IMAGE PICKER DELEGATES
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        showActivityIndicator()
        var selectedImage: UIImage?
        profileImageView.contentMode = .scaleAspectFit
        selectedImage = info[UIImagePickerControllerEditedImage] as? UIImage
        if let selectedImages = selectedImage {
            if NetworkingManager.isConnectedToNetwork(){
                showActivityIndicator()
                let tokenString = "Bearer " + retrievedToken!
                let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
                let URL = try! URLRequest(url: "\(uploadUserImageURL)", method: .post, headers: Headers)
                Alamofire.upload(multipartFormData: { multipartFormData in
                    multipartFormData.append(UIImageJPEGRepresentation(selectedImages,0.3)!, withName: "file", fileName: "file.png", mimeType: "image/jpg/jpeg/png")
                }, with: URL, encodingCompletion: {
                    encodingResult in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            self.profileImageView.image = selectedImage
                            let json = JSON(response.result.value!)
                        rootRef.child("users").child(self.userId).child("registers").updateChildValues(["profilePic":json["imageUrl"].string!])
                            SharedClass.sharedInstance.saveToUserDefaultsWithString(string: json["imageUrl"].string!, key: "profile")
                            Helper.alertBox(Mymsg: "user image uploaded succesfully", view: self)
                            debugPrint(response)
                            hideActivityIndicator()
                        }
                    case .failure(let encodingError):
                        hideActivityIndicator()
                        print(encodingError)
                    }
                })
            }else{
                hideActivityIndicator()
//                Helper.alertBox(Mymsg: "No internet", view: self)
                NetworkingManager.netWorkFailed(view: self)
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    //MARK:- HOME CLICKED
    @IBAction func homeClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- WALLET CLICKED
    @IBAction func walletClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "TransactionsViewController") as! TransactionsViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- CONTACT CLICKED
    @IBAction func contactClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "FavouriteListViewController") as! FavouriteListViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- CHAT CLICKED
    @IBAction func chatClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- BACK CLICKED
    @IBAction func backClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    //MARK:- SAVE BUTTON CLICKED
    @IBAction func saveBtnClicked(_ sender: Any) {
        if NetworkingManager.isConnectedToNetwork(){
            if changePasswd.text! == ""{
                Helper.alertBox(Mymsg: "No update Found", view: self)
            }else{
                showActivityIndicator()
                saveButton.isHidden = true
                changePasswd.isSecureTextEntry = false
                changePasswd.text = "Change Password"
                let tokenString = "Bearer " + retrievedToken!
                let userHeaders: HTTPHeaders = ["Authorization": tokenString, "Content-Type": "application/json"]
                let updatedUserParams = ["password":changePasswd.text!] as [String : Any]
                Alamofire.request(updateUserProfileURL, method: .put, parameters: updatedUserParams, encoding: JSONEncoding.default, headers: userHeaders).responseJSON { response in
                    hideActivityIndicator()
                    switch(response.result) {
                    case .success:
                        Helper.alertBox(Mymsg: "updated user details successfully", view: self)
                    case .failure(_):
                        hideActivityIndicator()
                        print(response.result.error!)
                        break
                    }
                    debugPrint(response)
                }
            }
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }
    }
    //MARK:- SIGNOUT CLICKED
    @IBAction func signOutClicked(_ sender: Any) {
        if NetworkingManager.isConnectedToNetwork(){
            showActivityIndicator()
            let retrievedToken: String? = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "token")
            let tokenString = "Bearer " + retrievedToken!
            let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
            Alamofire.request(deleteTokenURL, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: Headers).responseJSON{ response in
                switch (response.result){
                case .success(_):
                    SharedClass.sharedInstance.saveToUserDefaultsWithString(string: "", key: "token")
                    SharedClass.sharedInstance.saveToUserDefaultsWithString(string: "", key: "userId")
                    SharedClass.sharedInstance.saveToUserDefaultsWithString(string: "", key: "name")
                    SharedClass.sharedInstance.saveToUserDefaultsWithString(string: "", key: "mobile")
                    SharedClass.sharedInstance.saveToUserDefaultsWithString(string: "", key: "profile")
                    SharedClass.sharedInstance.saveToUserDefaultsWithString(string: "", key: "accountType")
                    let destVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    self.navigationController?.pushViewController(destVc, animated: true)
                    hideActivityIndicator()
                    break
                case .failure(_):
                    print(response.result.error!)
                    hideActivityIndicator()
                    break
                }
            }
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }
    }
    //MARK:- MY QRCODE CLICKED
    @IBAction func myQrCodeClicked(_ sender: Any) {
        if NetworkingManager.isConnectedToNetwork(){
            let destVc = self.storyboard?.instantiateViewController(withIdentifier: "MyQRCodeViewController") as! MyQRCodeViewController
            destVc.nameString = self.userName.text!
            self.navigationController?.pushViewController(destVc, animated: true)
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }
    }
    //MARK:- DELETE ACCOUNT CLICKED
    @IBAction func deleteAccClicked(_ sender: Any) {
        let alert = UIAlertController(title: "", message: "Please make sure your coins are withdrawn to your Bank account before you delete this account", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "DELETE", style: .default, handler: { UIAlertAction in
            if NetworkingManager.isConnectedToNetwork(){
                showActivityIndicator()
                let tokenString = "Bearer " + self.retrievedToken!
                let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
                Alamofire.request(deleteAccountURL, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: Headers).responseJSON { response in
                    hideActivityIndicator()
                    switch (response.result){
                    case .success:
                        var json = JSON(response.result.value!)
                        if response.response?.statusCode == 200{
                            if json["message"].string! == "USER DELETED"{
                                Alamofire.request(deleteTokenURL, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: Headers).responseJSON{ response in
                                    switch (response.result){
                                    case .success(_):
                                        rootRef.child("users").child(self.userId).removeValue()
                                        SharedClass.sharedInstance.saveToUserDefaultsWithString(string: "", key: "token")
                                        SharedClass.sharedInstance.saveToUserDefaultsWithString(string: "", key: "userId")
                                        SharedClass.sharedInstance.saveToUserDefaultsWithString(string: "", key: "name")
                                        SharedClass.sharedInstance.saveToUserDefaultsWithString(string: "", key: "mobile")
                                        SharedClass.sharedInstance.saveToUserDefaultsWithString(string: "", key: "profile")
                                        SharedClass.sharedInstance.saveToUserDefaultsWithString(string: "", key: "accountType")
                                        
                                        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                                        self.navigationController?.pushViewController(destVc, animated: true)
                                        break
                                    case .failure(_):
                                        print(response.result.error!)
                                        break
                                    }
                                }
                            }else{
                                Helper.alertBox(Mymsg: "delete failed please try again later", view: self)
                            }
                        }else{
                            Helper.alertBox(Mymsg: "delete failed please try again later", view: self)
                        }
                        
                    case .failure:
                        hideActivityIndicator()
                        print(response.result.error!)
                        break
                    }
                    debugPrint(response)
                }
            }else{
                NetworkingManager.netWorkFailed(view: self)
            }
        }))
        alert.addAction(UIAlertAction(title: "CANCEL", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    //MARK:- NOTIFICATION CLICKED
    @IBAction func notificationClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func shareClicked(_ sender: Any) {
        if NetworkingManager.isConnectedToNetwork(){
            let tokenString = "Bearer " + retrievedToken!
            let userHeaders: HTTPHeaders = ["Authorization": tokenString, "Content-Type": "application/json"]
            Alamofire.request(getMyReferralURL, method: .put, parameters: nil, encoding: JSONEncoding.default, headers: userHeaders).responseJSON { response in
                print(response)
                switch(response.result) {
                case .success:
                    var json = JSON(response.result.value!)
                    let activityViewController = UIActivityViewController(activityItems: ["Please Download the CashStash App from these links and use my referral code while signing Up: \"\(json["code"].string!)\" \nAndroid: https://play.google.com/store/apps/details?id=me.cashstash.android&hl=en_IN \niOS: https://itunes.apple.com/us/app/cashstash-payments/id1436295624?mt=8"], applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
                    
                    // exclude some activity types from the list (optional)
                    activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ,UIActivityType.saveToCameraRoll]
                    
                    // present the view controller
                    self.present(activityViewController, animated: true, completion: nil)
                //                Helper.alertBox(Mymsg: "updated user details successfully", view: self)
                case .failure(_):
                    hideActivityIndicator()
                    print(response.result.error!)
                    break
                }
                debugPrint(response)
            }
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }
    }
}
