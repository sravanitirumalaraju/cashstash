//
//  ChatViewController.swift
//  CashStash
//
//  Created by apple on 07/02/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit
import Firebase
import Kingfisher

class ChatViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate {

    @IBOutlet var chatScrollview: UIScrollView!
    @IBOutlet var recentBtn: UIButton!
    @IBOutlet var favouriteBtn: UIButton!
    @IBOutlet var recentView: UIView!
    @IBOutlet var favouriteVw: UIView!
    
    var recentTableView = UITableView()
    var favouriteTableView = UITableView()
    var (phoneNumbersArray,allDetailsArray) = (Array<Any>(),NSMutableArray())
    var toIDArray = Array<Any>()
    var items = [Conversation]()
    let blurView = UIView()
    var profileArray = [String]()
    var isContactFound = Bool()
        
    //MARK:-VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        
        blurView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        blurView.backgroundColor = AppColors.APP_BLURVIEW_COLOR
        view.addSubview(blurView)
        blurView.isHidden = true
        
        chatScrollview.showsHorizontalScrollIndicator = false
        // MARK:- scrollview content
        recentTableView.frame = CGRect(x: 0, y: 0, width: CGFloat(self.view.frame.size.width), height: self.view.frame.size.height-176)
        recentTableView.separatorStyle = .none
        recentTableView.register(TableViewCell.self, forCellReuseIdentifier: "WalletTableViewCell")
        recentTableView.delegate = self
        recentTableView.dataSource = self
        recentTableView.tag = 0
        recentTableView.bounces = false
        chatScrollview.addSubview(recentTableView)
        
        favouriteTableView.frame = CGRect(x: CGFloat(self.view.frame.size.width), y: 0, width: CGFloat(self.view.frame.size.width), height: self.view.frame.size.height-176)
        favouriteTableView.separatorStyle = .none
        favouriteTableView.register(FavouriteTableViewCell.self, forCellReuseIdentifier: "Cell")
        favouriteTableView.tag = 1
        favouriteTableView.bounces = false
        chatScrollview.addSubview(favouriteTableView)
        
        self.chatScrollview.contentSize = CGSize(width: CGFloat(self.view.frame.size.width) * 2, height: CGFloat(self.view.frame.size.height)-176)
        
        getRecentChat()
        getContacts()
    }
    //MARK:-VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK:- GET CONTACTS
    func getContacts(){
        if NetworkingManager.isConnectedToNetwork(){
            showActivityIndicator()
            var PhoneNumber = String()
            var allNumbersDict = NSMutableDictionary()
            Contact.fetchingCoreData(completion: { (contactResults) in
                if contactResults.count == 0{
                    hideActivityIndicator()
                    let alert = UIAlertController(title: "", message: "No access given to contacts", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { UIAlertAction in
                        _ = self.navigationController?.popViewController(animated: true)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }else{
                    Contact.getRegisterUsers(completion: {(RegistersDict) in
                        if let _ = RegistersDict["message"] as? String{
                        }else{
                            for result in contactResults {
                                PhoneNumber = (result.value(forKey: "mobileNumber")!) as! String
                                let contactName = (result.value(forKey: "contactName")!)
                                for (_,value) in RegistersDict.enumerated(){
                                    let contactDict = value.value as! NSMutableDictionary
                                    let numberDict = contactDict.value(forKey: "registers") as! NSMutableDictionary
                                    if PhoneNumber.contains(numberDict["Contact"] as! String) {
                                        let mobileNo = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "mobile")
                                        if numberDict["Contact"] as? String != mobileNo{
                                            allNumbersDict = ["name":contactName,"image":numberDict["profilePic"] as! String,"phonenumber":numberDict["Contact"] as! String,"userId":value.key]
                                            self.phoneNumbersArray.append(numberDict["Contact"] as! String)
                                            self.allDetailsArray.add(allNumbersDict)
                                            self.toIDArray.append(value.key)
                                        }
                                    }
                                }
                            }
//                            hideActivityIndicator()
                        }
                        DispatchQueue.main.async {
                            self.favouriteTableView.delegate = self
                            self.favouriteTableView.dataSource = self
                            self.favouriteTableView.reloadData()
                        }
                        hideActivityIndicator()
                    })
                }
            })
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }
    }
    //MARK:- GET RECENT CHAT
    func getRecentChat() {
        //fetching the resent conversation list
        if NetworkingManager.isConnectedToNetwork(){
            Conversation.showConversations { (conversations) in
                self.items = conversations
                self.items.sort{ $0.lastMessage.timestamp > $1.lastMessage.timestamp }
                DispatchQueue.main.async {
                    self.profileArray.removeAll()
                    self.recentTableView.reloadData()
                }
            }
        }else{
             NetworkingManager.netWorkFailed(view: self)
        }
    }
    
    // MARK:- TABLE VIEW DELEGATE && DATA SOURCE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 0 {
            if self.items.count == 0 {
                return 1
            }else{
                return self.items.count
            }
        }else{
            if phoneNumbersArray.count != 0{
                return phoneNumbersArray.count
            }else{
                return 1
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 0{
            let WalletTableViewCell = tableView.dequeueReusableCell(withIdentifier: "WalletTableViewCell", for: indexPath) as! TableViewCell
            WalletTableViewCell.selectionStyle = .none
            for cellView in WalletTableViewCell.contentView.subviews{
                cellView.removeFromSuperview()
            }
            WalletTableViewCell.backgroundColor = UIColor.white
            if self.items.count != 0{
                let profileImage = UIImageView.init(frame: CGRect(x: 20, y: 20, width: 50, height: 50))
                WalletTableViewCell.contentView.addSubview(profileImage)
                
                let chatNameLbl = UILabel.init(frame: CGRect(x: profileImage.frame.origin.x + profileImage.frame.size.width + 15  , y: 22.5, width: WalletTableViewCell.contentView.frame.size.width - 170, height: 20))
                WalletTableViewCell.contentView.addSubview(chatNameLbl)

                WalletTableViewCell.textLabel?.text = (self.items[indexPath.row].user.contact)
                WalletTableViewCell.textLabel?.isHidden = true
                
                profileImage.image = #imageLiteral(resourceName: "manProfile")
                chatNameLbl.text = self.items[indexPath.row].user.contact
                profileArray.append("")
                for value in self.allDetailsArray {
                    let dictionary = value as! NSMutableDictionary
                    if dictionary["phonenumber"] as! String == (self.items[indexPath.row].user.contact) {
                        if dictionary["image"] as? String != ""{
                            let url = URL(string: dictionary["image"] as! String)
                            profileImage.kf.setImage(with: url, placeholder: UIImage(named: "manProfile"), options: nil, progressBlock: nil, completionHandler: nil)
                        }else{
                            profileImage.image = #imageLiteral(resourceName: "manProfile")
                        }
                        profileArray.remove(at: indexPath.row)
                        profileArray.append(dictionary["image"] as! String)
                        chatNameLbl.text = dictionary["name"] as? String
                        WalletTableViewCell.textLabel?.text = dictionary["name"] as? String
                        break
                    }
                }
                profileImage.layer.cornerRadius = profileImage.frame.size.height / 2
                profileImage.clipsToBounds = true

                chatNameLbl.font = UIFont(name: "Barlow-Medium", size: 16.0)
                chatNameLbl.textColor = UIColor.init(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0)
                
                let lastMsgLbl = UILabel.init(frame: CGRect(x: profileImage.frame.origin.x + profileImage.frame.size.width + 15 , y: chatNameLbl.frame.origin.y + chatNameLbl.frame.size.height + 5, width: WalletTableViewCell.contentView.frame.size.width - 170, height: 20))
                switch self.items[indexPath.row].lastMessage.type {
                case .text:
                    let message = self.items[indexPath.row].lastMessage.content as! String
                    lastMsgLbl.text = message
                case .location:
                    lastMsgLbl.text = "Location"
                default:
                    lastMsgLbl.text = "Media"
                }
                lastMsgLbl.textColor = UIColor.init(red: 165.0/255.0, green: 165.0/255.0, blue: 165.0/255.0, alpha: 1.0)
                lastMsgLbl.font = UIFont(name: "Nunito-Regular", size: 12.0)
                WalletTableViewCell.contentView.addSubview(lastMsgLbl)
                
                let timeLbl = UILabel.init(frame: CGRect(x: WalletTableViewCell.contentView.frame.size.width - 80, y: 25, width: 70, height: 20))
                let messageDate = Date.init(timeIntervalSince1970: TimeInterval(self.items[indexPath.row].lastMessage.timestamp))
                let dataformatter = DateFormatter.init()
                dataformatter.timeStyle = .short
                dataformatter.locale = NSLocale.current
                dataformatter.dateFormat = "MM/dd/yyyy"
//                dataformatter.dateFormat = "MM/dd/yyyy hh:mm a"
                let date = dataformatter.string(from: messageDate)
                timeLbl.text = date
                timeLbl.textColor = UIColor.init(red: 165.0/255.0, green: 165.0/255.0, blue: 165.0/255.0, alpha: 1.0)
                timeLbl.textAlignment = .right
                timeLbl.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
                WalletTableViewCell.contentView.addSubview(timeLbl)
                
                let lineView = UIView.init(frame: CGRect(x: 0, y: WalletTableViewCell.contentView.frame.size.height - 1, width: WalletTableViewCell.frame.size.width, height: 1))
                lineView.backgroundColor = UIColor.init(red:  238.0/255.0, green: 228.0/255.0, blue: 249.0/255.0, alpha: 1.0)
                WalletTableViewCell.contentView.addSubview(lineView)
            }else{
                let nooResults_label = UILabel.init(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.recentTableView.frame.size.height))
                nooResults_label.textColor = UIColor.lightGray
                nooResults_label.text = "No Recent Chat"
                nooResults_label.textAlignment = .center
                nooResults_label.numberOfLines = 0
                WalletTableViewCell.contentView.addSubview(nooResults_label)
            }
            return WalletTableViewCell
        }else{
            let FavouriteTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! FavouriteTableViewCell
            FavouriteTableViewCell.selectionStyle = .none
            for cellView in FavouriteTableViewCell.contentView.subviews{
                cellView.removeFromSuperview()
            }
            FavouriteTableViewCell.backgroundColor = UIColor.white
            if phoneNumbersArray.count != 0{
                let dict = allDetailsArray[indexPath.row] as! NSDictionary
                
                let profileImage = UIImageView.init(frame: CGRect(x: 20, y: 20, width: 50, height: 50))
                
                if dict["image"] as? String != ""{
                    let url = URL(string: dict["image"] as! String)
                    profileImage.kf.setImage(with: url, placeholder: UIImage(named: "manProfile"), options: nil, progressBlock: nil, completionHandler: nil)
                }else{
                    profileImage.image = #imageLiteral(resourceName: "manProfile")
                }
                
                profileImage.layer.cornerRadius = profileImage.frame.size.height / 2
                profileImage.clipsToBounds = true
                FavouriteTableViewCell.contentView.addSubview(profileImage)
                
                let contactNameLbl = UILabel.init(frame: CGRect(x: profileImage.frame.origin.x + profileImage.frame.size.width + 15   , y: 22.5, width: FavouriteTableViewCell.contentView.frame.size.width - 80, height: 20))
                contactNameLbl.text = dict["name"] as! String?
                contactNameLbl.font = UIFont(name: "Barlow-Medium", size: 16.0)
                contactNameLbl.textColor = UIColor.init(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0)
                FavouriteTableViewCell.contentView.addSubview(contactNameLbl)
                
                let numberLbl = UILabel.init(frame: CGRect(x: profileImage.frame.origin.x + profileImage.frame.size.width + 15 , y: contactNameLbl.frame.origin.y + contactNameLbl.frame.size.height + 5, width: FavouriteTableViewCell.contentView.frame.size.width - 80, height: 20))
                numberLbl.text = dict["phonenumber"] as? String
                numberLbl.font = UIFont(name: "Nunito-Regular", size: 12.0)
                numberLbl.textColor = UIColor.init(red: 157.0/255.0, green: 145.0/255.0, blue: 171.0/255.0, alpha: 1.0)
                FavouriteTableViewCell.contentView.addSubview(numberLbl)
                
                let lineView = UIView.init(frame: CGRect(x: 0, y: FavouriteTableViewCell.contentView.frame.size.height - 1, width: FavouriteTableViewCell.frame.size.width, height: 1))
                lineView.backgroundColor = UIColor.init(red:  238.0/255.0, green: 228.0/255.0, blue: 249.0/255.0, alpha: 1.0)
                FavouriteTableViewCell.contentView.addSubview(lineView)
            }else{
                let nooResults_label = UILabel.init(frame:CGRect(x: 0, y: FavouriteTableViewCell.frame.size.height/3, width: self.view.frame.size.width, height: 50))
                nooResults_label.textColor = UIColor.lightGray
                nooResults_label.text = "No Contacts Found"
                nooResults_label.textAlignment = .center
                nooResults_label.numberOfLines = 0
                FavouriteTableViewCell.contentView.addSubview(nooResults_label)
            }
            return FavouriteTableViewCell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 0{
            if self.items.count != 0{
                let destVc = self.storyboard?.instantiateViewController(withIdentifier: "RecentChatViewController") as! RecentChatViewController
                destVc.toId = self.items[indexPath.row].user.id
                let cell = self.recentTableView.cellForRow(at: indexPath)
                destVc.senderDisplayName = cell?.textLabel?.text!
                destVc.outcomingImageString = profileArray[indexPath.row]
                self.navigationController?.pushViewController(destVc, animated: true)
            }
        }else{
            if phoneNumbersArray.count != 0{
                let destVc = self.storyboard?.instantiateViewController(withIdentifier: "RecentChatViewController") as! RecentChatViewController
                destVc.toId = toIDArray[indexPath.row] as! String
                let dict = allDetailsArray[indexPath.row] as! NSDictionary
                destVc.senderDisplayName = dict["name"] as? String
                destVc.outcomingImageString = dict["image"] as! String
                self.navigationController?.pushViewController(destVc, animated: true)
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 0{
            if self.items.count == 0{
                return self.view.frame.size.height-178
            }else{
                return 90
            }
        }else{
            if phoneNumbersArray.count == 0{
                return self.view.frame.size.height-178
            }else{
                return 90
            }
        }
    }
    //MARK:- HOME CLICKED
    @IBAction func homeClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- WALLET CLICKED
    @IBAction func walletClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "TransactionsViewController") as! TransactionsViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- CONTACT CLICKED
    @IBAction func contactClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "FavouriteListViewController") as! FavouriteListViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- PROFILE CLICKED
    @IBAction func profileClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK:- RECENT BUTTON CLICKED
    @IBAction func recentBtnClicked(_ sender: Any) {
        favouriteBtn.setTitleColor(UIColor.init(red: 157.0/255.0, green: 145.0/255.0, blue: 171.0/255.0, alpha: 1.0), for: .normal)
        favouriteVw.backgroundColor = UIColor.clear
        
        recentBtn.setTitleColor(UIColor.init(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0), for: .normal)
        recentView.backgroundColor = UIColor.init(red: 36.0/255.0, green: 137.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        
        let frame :CGRect = CGRect(x: CGFloat(self.view.frame.width) * 0, y: 124, width: CGFloat(self.view.frame.width), height: chatScrollview.frame.size.height)
        self.chatScrollview.scrollRectToVisible(frame, animated: true)
    }
    //MARK:- FAVOURITE BUTTON CLICKED
    @IBAction func favouriteBtnClicked(_ sender: Any) {
        recentBtn.setTitleColor(UIColor.init(red: 157.0/255.0, green: 145.0/255.0, blue: 171.0/255.0, alpha: 1.0), for: .normal)
        recentView.backgroundColor = UIColor.clear
        
        favouriteBtn.setTitleColor(UIColor.init(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0), for: .normal)
        favouriteVw.backgroundColor = UIColor.init(red: 36.0/255.0, green: 137.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        
        let frame :CGRect = CGRect(x: CGFloat(self.view.frame.width) * 1, y: 124, width: CGFloat(self.view.frame.width), height: chatScrollview.frame.size.height)
        self.chatScrollview.scrollRectToVisible(frame, animated: true)
    }
    //MARK:- SCROLLVIEW DELEGATES
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if chatScrollview.contentOffset.x == 0{
            favouriteBtn.setTitleColor(UIColor.init(red: 157.0/255.0, green: 145.0/255.0, blue: 171.0/255.0, alpha: 1.0), for: .normal)
            favouriteVw.backgroundColor = UIColor.clear
            
            recentBtn.setTitleColor(UIColor.init(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0), for: .normal)
            recentView.backgroundColor = UIColor.init(red: 36.0/255.0, green: 137.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        }else{
            recentBtn.setTitleColor(UIColor.init(red: 157.0/255.0, green: 145.0/255.0, blue: 171.0/255.0, alpha: 1.0), for: .normal)
            recentView.backgroundColor = UIColor.clear
            
            favouriteBtn.setTitleColor(UIColor.init(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0), for: .normal)
            favouriteVw.backgroundColor = UIColor.init(red: 36.0/255.0, green: 137.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        }
    }
    //MARK:- BACK CLICKED
    @IBAction func backClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    //MARK:- NOTIFICATION CLICKED
    @IBAction func notificationClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
}
//MARK:- removing repeated values in an array
extension Array where Element: Equatable {
    mutating public func uniqInPlace() {
        var seen = [Element]()
        var index = 0
        for element in self {
            if seen.contains(element) {
                remove(at: index)
            } else {
                seen.append(element)
                index += 1
            }
        }
    }
}
