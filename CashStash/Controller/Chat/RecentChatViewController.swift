//
//  ChatWindowViewController.swift
//  CashStash
//
//  Created by apple on 05/02/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import Firebase
import FirebaseDatabase
import Alamofire
import Kingfisher
import SwiftyJSON
import FirebaseStorage

class RecentChatViewController: JSQMessagesViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet var profileView: UIView!
    @IBOutlet var msgTxtFld: UITextField!
    //setting color to outgoing bubble
    lazy var outgoingBubble: JSQMessagesBubbleImage = {
        return JSQMessagesBubbleImageFactory()!.outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleBlue())
    }()
    //setting color to incoming bubble
    lazy var incomingBubble: JSQMessagesBubbleImage = {
        return JSQMessagesBubbleImageFactory()!.incomingMessagesBubbleImage(with: UIColor.white)
    }()
    var userId = String()
    var contactNumber = String()
    var chatArray = NSMutableArray()
    var noDataLbl = UILabel()
    
    var id = String()
    var name = String()
    var text = String()
    var toId = String()
    var items = [Message]()
    var messages = [JSQMessage]()
    var isRequestClicked = Bool()
    let blurView = UIView()
    var outcomingImageString = String()
    var incomingImageString = String()
    let retrievedToken: String? = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "token")
    var allMsgKeys = [String]()
    var isCancelClicked = Bool()
    var isAttachClicked = Bool()
    var attachView = UIView()
    var imagePicker = UIImagePickerController()
    var galleryBtn = UIButton()
    var gifBtn = UIButton()
    var cameraBtn = UIButton()
    var galleryLbl = UILabel()
    var gifLbl = UILabel()
    var cameraLbl = UILabel()
    
    let tappedImageView = UIImageView()
    let scrollImg: UIScrollView = UIScrollView()

    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        //chat view content
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = UIColor(red: 5.0/255.0, green: 78.0/255.0, blue: 106.0/255.0, alpha: 1.0)
        
        blurView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        blurView.backgroundColor = AppColors.APP_BLURVIEW_COLOR
        view.addSubview(blurView)
        blurView.isHidden = true
        
        incomingImageString = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "profile")!
 
        self.collectionView.register(UINib(nibName: "ImageOutgoingCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageOutgoingCollectionViewCell")
        self.collectionView.register(UINib(nibName: "CustomOutgoingCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CustomOutgoingCollectionViewCell")
        self.collectionView.register(UINib(nibName: "CustomInComingCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CustomInComingCollectionViewCell")
        self.collectionView.register(UINib(nibName: "GalleryOutgoingCollectionViewCell", bundle: nil
        ), forCellWithReuseIdentifier: "GalleryOutgoingCollectionViewCell")
        self.collectionView.register(UINib(nibName: "GalleryIncomingCollectionViewCell", bundle: nil
        ), forCellWithReuseIdentifier: "GalleryIncomingCollectionViewCell")
        self.collectionView.backgroundColor = UIColor.init(red: 249.0/255.0, green: 250.0/255.0, blue:251.0/255.0, alpha: 1.0)
        
        collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        collectionView.collectionViewLayout.incomingAvatarViewSize = CGSize(width: 35, height: 35)
        
        self.collectionView?.collectionViewLayout.messageBubbleFont = UIFont(name: "Nunito-Regular", size: 16.0)!
        userId = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "userId")!
        contactNumber = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "mobile")!
        senderId = userId
        title = "Chat: \(senderDisplayName!)"
        self.title = senderDisplayName
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 10.0/255.0, green: 60.0/255.0, blue: 88.0/255.0, alpha: 1.0)
//        self.navigationController?.navigationBar.barTintColor = UIColor(red: 20.0/255.0, green: 60.0/255.0, blue: 89.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]

        inputToolbar.contentView.leftBarButtonItem = nil
        let imageWidth: CGFloat = 27
        let image = UIImage(named: "send")
        
        inputToolbar.contentView.rightBarButtonItemWidth = imageWidth
        inputToolbar.contentView.rightBarButtonItem.setImage(image, for: .normal)
        inputToolbar.contentView.textView.font = UIFont(name: "Nunito-Regular", size: 14.0)
        inputToolbar.contentView.textView.autocorrectionType = .yes
        let moneyView = UIView.init(frame: CGRect(x: 0, y: 0, width: inputToolbar.contentView.frame.size.width, height: 40))
        inputToolbar.contentView.addSubview(moneyView)
        
        let requestBtn = UIButton.init(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
        requestBtn.setTitle("Request Money", for: .normal)
        requestBtn.setTitleColor(UIColor.green, for: .normal)
        requestBtn.center.x = moneyView.center.x
        requestBtn.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 16.0)
        requestBtn.addTarget(self, action: #selector(requestMoneyClicked), for: .touchUpInside)
        moneyView.addSubview(requestBtn)
        
        let attachmentBtn = UIButton.init(frame: CGRect(x: moneyView.frame.size.width - 40, y: 0, width: 40, height: 40))
        attachmentBtn.setImage(UIImage(named: "attach"), for: .normal)
        attachmentBtn.addTarget(self, action: #selector(attachmentClicked), for: .touchUpInside)
        moneyView.addSubview(attachmentBtn)
        
        inputToolbar.contentView.textView.keyboardType = .default
        self.collectionView?.collectionViewLayout.sectionInset = UIEdgeInsets(top: 10, left: 0, bottom: 35, right: 0)

        scrollToBottom(animated: true)
        setupBackButton()
        self.fetchData()
        
        
        imagePicker.delegate = self
    }
    @objc func attachmentClicked(){

        let actionSheetController: UIAlertController = UIAlertController (title: "Choose an Option", message: nil, preferredStyle: .actionSheet)
        actionSheetController.addAction( UIAlertAction (title: "Cancel", style: .cancel, handler: nil))

        actionSheetController.addAction( UIAlertAction (title: "Photo Library", style: .default, handler:{ (alert: UIAlertAction!) in self.chooseFromPhotoLibrary()}
        ))
        actionSheetController.addAction( UIAlertAction (title: "Take Picture", style: .default, handler: {(alert: UIAlertAction!) in self.chooseFromCamera()}
        ))
        self.view.endEditing(true)
        self.present(actionSheetController, animated: true, completion: nil)
        
//        if isAttachClicked == false{
//            isAttachClicked = true
//            attachView.isHidden = false
//            attachView.frame = CGRect(x: 0, y: -80, width: inputToolbar.contentView.frame.size.width, height: 80)
//            attachView.backgroundColor = UIColor.white
//            inputToolbar.contentView.addSubview(attachView)
//
//            let innerView = UIView.init(frame: CGRect(x: 0, y: 0, width: 200, height: 80))
//            innerView.center.x = attachView.center.x
//            attachView.addSubview(innerView)
//
//            galleryBtn.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
//            galleryBtn.setImage(UIImage(named: "gallery"), for: .normal)
//            galleryBtn.addTarget(self, action: #selector(galleryClicked), for: .touchUpInside)
//            galleryBtn.backgroundColor = UIColor.red
//            innerView.addSubview(galleryBtn)
//
//            galleryLbl.frame = CGRect(x: 0, y: galleryBtn.frame.origin.y + galleryBtn.frame.size.height, width: 60, height: 20)
//            galleryLbl.text = "Gallery"
//            galleryLbl.textColor = AppColors.APP_COLOR_BLUE
//            galleryLbl.font = UIFont(name: "Nunito-Regular", size: 12)
//            galleryLbl.textAlignment = .center
//            innerView.addSubview(galleryLbl)
//
//            gifBtn.frame = CGRect(x: galleryBtn.frame.size.width + 10, y: 0, width: 60, height: 60)
//            gifBtn.setImage(UIImage(named: "gif"), for: .normal)
//            innerView.addSubview(gifBtn)
//
//            gifLbl.frame = CGRect(x: galleryBtn.frame.size.width + 10, y: gifBtn.frame.origin.y + gifBtn.frame.size.height, width: 60, height: 20)
//            gifLbl.text = "Gif"
//            gifLbl.textColor = AppColors.APP_COLOR_BLUE
//            gifLbl.font = UIFont(name: "Nunito-Regular", size: 12)
//            gifLbl.textAlignment = .center
//            innerView.addSubview(gifLbl)
//
//            cameraBtn.frame = CGRect(x: 140, y: 0, width: 60, height: 60)
//            cameraBtn.setImage(UIImage(named: "cameraBlue"), for: .normal)
//            cameraBtn.addTarget(self, action: #selector(cameraClicked), for: .touchUpInside)
//            cameraBtn.backgroundColor = UIColor.red
//            innerView.addSubview(cameraBtn)
//
//            cameraLbl.frame = CGRect(x: 140, y: cameraBtn.frame.origin.y + cameraBtn.frame.size.height, width: 60, height: 20)
//            cameraLbl.text = "Camera"
//            cameraLbl.textColor = AppColors.APP_COLOR_BLUE
//            cameraLbl.font = UIFont(name: "Nunito-Regular", size: 12)
//            cameraLbl.textAlignment = .center
//            innerView.addSubview(cameraLbl)
//        }else{
//            isAttachClicked = false
//           attachView.isHidden = true
//        }
    }
    //MARK:- CHOOSE FROM CAMERA
    func chooseFromCamera() {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.cameraCaptureMode = .photo
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    //MARK:- CHOOSE FROM LIBRARY
    func chooseFromPhotoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    //MARK:- IMAGE PICKER DELEGATES
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
//        var selectedImage: UIImage?
        if  NetworkingManager.isConnectedToNetwork(){
        showActivityIndicator()
        let pickedImage = info[UIImagePickerControllerEditedImage]
        let imageData = UIImageJPEGRepresentation(pickedImage as! UIImage, 0.5)
        let storageRef = Storage.storage().reference().child("\(String(Date().timeIntervalSince1970)).png")
        let metaData = StorageMetadata()
        metaData.contentType = "image/png/jpeg/jpg"
        storageRef.putData(imageData!, metadata: metaData, completion: { (metadata, error) in
            if error == nil{
                print("sucess")
                storageRef.downloadURL(completion: { (url, error) in
                    print(url!.absoluteString)
                    let values = ["type": "photo", "content": (url!.absoluteString), "fromID": self.senderId, "toID": self.toId, "timestamp": Int(Date().timeIntervalSince1970), "isRead":false,"isRequestSent":false,"isCancel":false] as [String : Any]
                    Message.uploadMessage(withValues: values, toID: self.toId, completion: { (_) in
                    })
                    let tokenString = "Bearer " + self.retrievedToken!
                    let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
                    let chatNotifyParams = ["userId":self.toId,"message":""] as [String : Any]
                    Alamofire.request(chatNotificationURL, method: .post, parameters: chatNotifyParams, encoding: JSONEncoding.default, headers: Headers).responseJSON { response in
                        switch (response.result){
                        case .success:break
                        case .failure:
                            hideActivityIndicator()
                            break
                        }
                        debugPrint(response)
                    }
                })
            }else{
                print(error!)
                hideActivityIndicator()
            }
            
        })
        
//        Storage.storage().reference().child("messagePics").child(child).putData(imageData!, metadata: nil, completion: { (metadata, error) in
//            if error == nil {
//                let path = metadata?.downloadURL()?.absoluteString
//                print(path!)
//
//                let values = ["type": "photo", "content": path!, "fromID": senderId, "toID": toId, "timestamp": Int(Date().timeIntervalSince1970), "isRead":false,"isRequestSent":false,"isCancel":false] as [String : Any]
//                Message.uploadMessage(withValues: values, toID: toId, completion: { (_) in
//                })
//                let tokenString = "Bearer " + self.retrievedToken!
//                let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
//                let chatNotifyParams = ["userId":self.toId,"message":""] as [String : Any]
//                Alamofire.request(chatNotificationURL, method: .post, parameters: chatNotifyParams, encoding: JSONEncoding.default, headers: Headers).responseJSON { response in
//                    switch (response.result){
//                    case .success:break
//                    case .failure:
//                        hideActivityIndicator()
//                        break
//                    }
//                    debugPrint(response)
//                }
//            }
//        })
        
        self.dismiss(animated: true, completion: nil)
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }

    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    //MARK:- TEXTVIEW DELEGATES
    override func textViewDidBeginEditing(_ textView: UITextView) {
        if isRequestClicked != true {
            inputToolbar.contentView.textView.keyboardType = .default
            inputToolbar.contentView.textView.becomeFirstResponder()
        }
        if (self.automaticallyScrollsToMostRecentMessage) {
            self.collectionView?.collectionViewLayout.sectionInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
            scrollToBottom(animated: true)
        }
    }
    override func textViewDidEndEditing(_ textView: UITextView) {
        if inputToolbar.contentView.textView.keyboardType != .default{
            inputToolbar.contentView.textView.keyboardType = .default
            isRequestClicked = false
        }
    }
    //MARK:- REQUEST MONEY CLICKED
    @objc func requestMoneyClicked(){
        isRequestClicked = true
        if inputToolbar.contentView.textView.keyboardType == .default{
            inputToolbar.contentView.textView.resignFirstResponder()
        }
        inputToolbar.contentView.textView.keyboardType = .decimalPad
        inputToolbar.contentView.textView.becomeFirstResponder()
        if (self.automaticallyScrollsToMostRecentMessage) {
            self.collectionView?.collectionViewLayout.sectionInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
            scrollToBottom(animated: true)
        }
    }
    //MARK:- VIEW DID DISAPPEAR
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    //MARK:- VIEW WILL DISAPPEAR
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Message.markMessagesRead(forUserID: toId)
    }
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    //MARK:-Downloads all messages
    func fetchData() {
        if NetworkingManager.isConnectedToNetwork(){
            showActivityIndicator()
//            Message.downloadAllMessages(forUserID: toId, completion: {[weak weakSelf = self] (allMessages) in
            Message.downloadAllMessages(forUserID: toId, completion: { (allMessages) in
                if allMessages.content as! String == ""{
                    hideActivityIndicator()
                    self.noDataLbl.isHidden = false
                    self.noDataLbl.frame = CGRect(x: 0, y: (self.view.frame.size.height/2)-30, width: self.view.frame.size.width, height: 20)
                    self.noDataLbl.text = "No chat was found"
                    self.noDataLbl.textAlignment = .center
                    self.view.addSubview(self.noDataLbl)
                }else{
                    self.noDataLbl.isHidden = true
                    self.allMsgKeys.append(allMessages.allKeys!)
                    self.allMsgKeys.uniqInPlace()
                    self.items.append(allMessages)
                    self.items.sort{ $0.timestamp < $1.timestamp }
//                    DispatchQueue.main.async {
//                        if let state = weakSelf?.items.isEmpty, state == false {
                        self.messages = []
                        for data in self.items {
                            if let message = JSQMessage(senderId: data.senderId, displayName: self.name, text: data.content as? String){
                                self.messages.append(message)
                            }
                        }
                        self.finishReceivingMessage(animated: false)
//                        }
//                    }
                    hideActivityIndicator()
                    
                }
                Message.markMessagesRead(forUserID: self.toId)
            })
        }else{
             NetworkingManager.netWorkFailed(view: self)
        }
    }
    
    //MARK:- COLLECTIONVIEW DELEGATES
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData!{
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource!{
        return messages[indexPath.item].senderId == senderId ? outgoingBubble : incomingBubble
    }
    
//    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellBottomLabelAt indexPath: IndexPath!) -> NSAttributedString! {
//        let messageDate = Date.init(timeIntervalSince1970: TimeInterval(self.items[indexPath.row].timestamp))
//        let dataformatter = DateFormatter.init()
//        dataformatter.timeStyle = .short
//        dataformatter.locale = NSLocale.current
////        dataformatter.dateFormat = "hh:mm a"
//        dataformatter.dateFormat = "MM/dd/yyyy hh:mma"
//        let date = dataformatter.string(from: messageDate)
//        return NSAttributedString(string:date)
//    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellBottomLabelAt indexPath: IndexPath!) -> CGFloat {
        //        if indexPath.row + 1 == messages.count{
        //            return 0
        //        }
        if self.items[indexPath.row].isRequestSent == false{
            return 20
        } else {
            if self.items[indexPath.row].isCancel == false{
                return 110
            }else{
                return 20
            }
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource!{
        return JSQMessagesAvatarImageFactory.avatarImage(with: #imageLiteral(resourceName: "manProfile"), diameter: 30)
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        if indexPath.row + 1 == messages.count{
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageOutgoingCollectionViewCell", for: indexPath) as! ImageOutgoingCollectionViewCell
//            cell.userProfileImg.layer.cornerRadius = cell.userProfileImg.frame.size.height / 2
//            cell.userProfileImg.clipsToBounds = true
//            let url = URL(string: incomingImageString)
//            cell.userProfileImg.kf.setImage(with: url, placeholder: UIImage(named: "manProfile"), options: nil, progressBlock: nil, completionHandler: nil)
//            cell.backgroundColor = UIColor.init(red: 249.0/255.0, green: 250.0/255.0, blue:251.0/255.0, alpha: 1.0)
//            return cell
//        }
        let messageDate = Date.init(timeIntervalSince1970: TimeInterval(self.items[indexPath.row].timestamp))
        let dataformatter = DateFormatter.init()
        dataformatter.timeStyle = .short
        dataformatter.locale = NSLocale.current
        dataformatter.dateFormat = "MM/dd/yyyy hh:mma"
        let date = dataformatter.string(from: messageDate)
        if self.items[indexPath.item].isRequestSent == true{ //when request sent is done
            if self.items[indexPath.item].senderId == senderId{//if user is same with login user
                if self.items[indexPath.item].isCancel == true{ //when cancel request is done
                    let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
                    cell.cellBottomLabel.text = "\(date)"
//                    cell.backgroundColor = UIColor.init(red: 249.0/255.0, green: 250.0/255.0, blue:251.0/255.0, alpha: 1.0)
                    if self.items[indexPath.item].senderId == senderId{
                        cell.textView!.textColor = UIColor.white
                        cell.avatarImageView.isHidden = true
                    }else{
                        cell.textView!.textColor = UIColor.init(red: 23.0/255.0, green: 32.0/255.0, blue: 63.0/255.0, alpha: 1.0)
                        if self.items.count != indexPath.row + 1{
                            let nextValue = self.items[indexPath.item + 1].senderId
                            if nextValue != self.items[indexPath.item].senderId {
                                cell.avatarImageView.isHidden = false
//                                cell.avatarImageView.layer.cornerRadius = cell.avatarImageView.frame.size.height / 2
//                                cell.avatarImageView.clipsToBounds = true
                                let url = URL(string: outcomingImageString)
                                cell.avatarImageView.kf.setImage(with: url, placeholder: UIImage(named: "manProfile"), options: nil, progressBlock: nil, completionHandler: nil)
                            }else{
                                cell.avatarImageView.isHidden = true
                            }
                        }else{
                            if senderId != self.items[indexPath.item].senderId{
                                cell.avatarImageView.isHidden = false
//                                cell.avatarImageView.layer.cornerRadius = cell.avatarImageView.frame.size.height / 2
//                                cell.avatarImageView.clipsToBounds = true
                                let url = URL(string: outcomingImageString)
                                cell.avatarImageView.kf.setImage(with: url, placeholder: UIImage(named: "manProfile"), options: nil, progressBlock: nil, completionHandler: nil)
                            }else{
                                cell.avatarImageView.isHidden = true
                            }
                        }
                    }
                    cell.avatarImageView.layer.cornerRadius = cell.avatarImageView.frame.size.height / 2
                    cell.avatarImageView.clipsToBounds = true
                    cell.textView.font = UIFont(name: "Nunito-Regular", size: 16.0)
                    cell.cellBottomLabel.font = UIFont(name: "Nunito-Regular", size: 8.0)
                    cell.cellBottomLabel.textAlignment = .right
                    return cell
                }else{ //when cancel request is not done
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomOutgoingCollectionViewCell", for: indexPath) as! CustomOutgoingCollectionViewCell
//                    cell.backgroundColor = UIColor.init(red: 249.0/255.0, green: 250.0/255.0, blue:251.0/255.0, alpha: 1.0)
                    cell.moneyLbl.text = "CSH \(self.items[indexPath.item].content)"
                    let messageDate = Date.init(timeIntervalSince1970: TimeInterval(self.items[indexPath.row].timestamp))
                    let dataformatter = DateFormatter.init()
                    dataformatter.timeStyle = .short
                    dataformatter.locale = NSLocale.current
//                    dataformatter.dateFormat = "hh:mm a"
                    dataformatter.dateFormat = "MM/dd/yyyy hh:mma"
                    let date = dataformatter.string(from: messageDate)
                    cell.timeLabel.text = date
                    return cell
                }
            }else{//if user is not same with login user
                if self.items[indexPath.item].isCancel == true{
                    let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
                    cell.cellBottomLabel.text = "\(date)"
//                    cell.backgroundColor = UIColor.init(red: 249.0/255.0, green: 250.0/255.0, blue:251.0/255.0, alpha: 1.0)
                    if self.items[indexPath.item].senderId == senderId{
                        cell.textView!.textColor = UIColor.white
                        cell.avatarImageView.isHidden = true
                    }else{
                        cell.textView!.textColor = UIColor.init(red: 23.0/255.0, green: 32.0/255.0, blue: 63.0/255.0, alpha: 1.0)
                        if self.items.count != indexPath.row + 1{
                            let nextValue = self.items[indexPath.item + 1].senderId
                            if nextValue != self.items[indexPath.item].senderId {
                                cell.avatarImageView.isHidden = false
//                                cell.avatarImageView.layer.cornerRadius = cell.avatarImageView.frame.size.height / 2
//                                cell.avatarImageView.clipsToBounds = true
                                let url = URL(string: outcomingImageString)
                                cell.avatarImageView.kf.setImage(with: url, placeholder: UIImage(named: "manProfile"), options: nil, progressBlock: nil, completionHandler: nil)
                            }else{
                                cell.avatarImageView.isHidden = true
                            }
                        }else{
                            if senderId != self.items[indexPath.item].senderId{
                                cell.avatarImageView.isHidden = false
//                                cell.avatarImageView.layer.cornerRadius = cell.avatarImageView.frame.size.height / 2
//                                cell.avatarImageView.clipsToBounds = true
                                let url = URL(string: outcomingImageString)
                                cell.avatarImageView.kf.setImage(with: url, placeholder: UIImage(named: "manProfile"), options: nil, progressBlock: nil, completionHandler: nil)
                            }else{
                                cell.avatarImageView.isHidden = true
                            }
                        }
                    }
                    cell.avatarImageView.layer.cornerRadius = cell.avatarImageView.frame.size.height / 2
                    cell.avatarImageView.clipsToBounds = true
                    cell.textView.font = UIFont(name: "Nunito-Regular", size: 16.0)
                    cell.cellBottomLabel.font = UIFont(name: "Nunito-Regular", size: 8.0)
                    cell.cellBottomLabel.textAlignment = .right
                    return cell
                }else{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomInComingCollectionViewCell", for: indexPath) as! CustomInComingCollectionViewCell
//                    cell.backgroundColor = UIColor.init(red: 249.0/255.0, green: 250.0/255.0, blue:251.0/255.0, alpha: 1.0)
                    if self.items.count != indexPath.row + 1{
                        let nextValue = self.items[indexPath.item + 1].senderId
                        //                    print(nextValue)
                        //                    print(self.items[indexPath.item].senderId)
                        if nextValue != self.items[indexPath.item].senderId{
                            cell.userImageVw.isHidden = false
//                            cell.userImageVw.layer.cornerRadius = cell.userImageVw.frame.size.height / 2
//                            cell.userImageVw.clipsToBounds = true
                            let url = URL(string: outcomingImageString)
                            cell.userImageVw.kf.setImage(with: url, placeholder: UIImage(named: "manProfile"), options: nil, progressBlock: nil, completionHandler: nil)
                        }else{
                            cell.userImageVw.isHidden = true
                        }
                    }else{
                        if senderId != self.items[indexPath.item].senderId{
                            cell.userImageVw.isHidden = false
//                            cell.userImageVw.layer.cornerRadius = cell.userImageVw.frame.size.height / 2
//                            cell.userImageVw.clipsToBounds = true
                            let url = URL(string: outcomingImageString)
                            cell.userImageVw.kf.setImage(with: url, placeholder: UIImage(named: "manProfile"), options: nil, progressBlock: nil, completionHandler: nil)
                        }else{
                            cell.userImageVw.isHidden = true
                        }
                    }
                    cell.moneyLbl.text = "CSH \(self.items[indexPath.item].content)"
                    cell.timeLbl.text = "\(date)"
                    cell.cancelButton.tag = indexPath.row
                    cell.sendButton.tag = indexPath.row
                    cell.sendButton.addTarget(self, action: #selector(reqSendClicked), for: .touchUpInside)
                    cell.cancelButton.addTarget(self, action: #selector(reqCancelClicked), for: .touchUpInside)
                    cell.userImageVw.layer.cornerRadius = cell.userImageVw.frame.size.height / 2
                    cell.userImageVw.clipsToBounds = true
                    return cell
                }
            }
        }else{ //when request sent is not done
            if self.items[indexPath.item].type == .photo{
                if self.items[indexPath.item].senderId == senderId{//if user is same with login user
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryOutgoingCollectionViewCell", for: indexPath) as! GalleryOutgoingCollectionViewCell
                    let url = URL(string: self.items[indexPath.item].content as! String)
                    cell.galleryOutgoingImgVw.kf.setImage(with: url, placeholder: UIImage(named: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
                    cell.dateAndTimeLabel.text = "\(date)"
                    return cell
                }else{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryIncomingCollectionViewCell", for: indexPath) as! GalleryIncomingCollectionViewCell
                    let url = URL(string: self.items[indexPath.item].content as! String)
                    cell.galleryIncomingImgVw.kf.setImage(with: url, placeholder: UIImage(named: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
                    cell.userProfileImg.layer.cornerRadius = cell.userProfileImg.frame.size.height / 2
                    cell.userProfileImg.clipsToBounds = true
                    cell.dateAndNameLbl.text = "\(date)"
                    if self.items.count != indexPath.row + 1{
                        let nextValue = self.items[indexPath.item + 1].senderId
                        if nextValue != self.items[indexPath.item].senderId {
                            cell.userProfileImg.isHidden = false
                            let url = URL(string: outcomingImageString)
                            cell.userProfileImg.kf.setImage(with: url, placeholder: UIImage(named: "manProfile"), options: nil, progressBlock: nil, completionHandler: nil)
                        }else{
                            cell.userProfileImg.isHidden = true
                        }
                    }else{
                        if senderId != self.items[indexPath.item].senderId{
                            cell.userProfileImg.isHidden = false
                            let url = URL(string: outcomingImageString)
                            cell.userProfileImg.kf.setImage(with: url, placeholder: UIImage(named: "manProfile"), options: nil, progressBlock: nil, completionHandler: nil)
                        }else{
                            cell.userProfileImg.isHidden = true
                        }
                    }
                    return cell
                }
            }else{
                let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
                cell.cellBottomLabel.text = "\(date)"
                //            cell.backgroundColor = UIColor.init(red: 249.0/255.0, green: 250.0/255.0, blue:251.0/255.0, alpha: 1.0)
                cell.avatarImageView.layer.cornerRadius = cell.avatarImageView.frame.size.height / 2
                cell.avatarImageView.clipsToBounds = true
                cell.cellBottomLabel.textAlignment = .right
                if self.items[indexPath.item].senderId == senderId{
                    cell.textView!.textColor = UIColor.white
                    cell.avatarImageView.isHidden = true
                }else{
                    cell.textView!.textColor = UIColor.init(red: 23.0/255.0, green: 32.0/255.0, blue: 63.0/255.0, alpha: 1.0)
                    if self.items.count != indexPath.row + 1{
                        let nextValue = self.items[indexPath.item + 1].senderId
                        if nextValue != self.items[indexPath.item].senderId {
                            cell.avatarImageView.isHidden = false
                            //                        cell.avatarImageView.layer.cornerRadius = cell.avatarImageView.frame.size.height / 2
                            //                        cell.avatarImageView.clipsToBounds = true
                            let url = URL(string: outcomingImageString)
                            cell.avatarImageView.kf.setImage(with: url, placeholder: UIImage(named: "manProfile"), options: nil, progressBlock: nil, completionHandler: nil)
                        }else{
                            cell.avatarImageView.isHidden = true
                        }
                    }else{
                        if senderId != self.items[indexPath.item].senderId{
                            cell.avatarImageView.isHidden = false
                            //                        cell.avatarImageView.layer.cornerRadius = cell.avatarImageView.frame.size.height / 2
                            //                        cell.avatarImageView.clipsToBounds = true
                            let url = URL(string: outcomingImageString)
                            cell.avatarImageView.kf.setImage(with: url, placeholder: UIImage(named: "manProfile"), options: nil, progressBlock: nil, completionHandler: nil)
                        }else{
                            cell.avatarImageView.isHidden = true
                        }
                    }
                    cell.cellBottomLabel.textAlignment = .left
                }
                cell.textView.font = UIFont(name: "Nunito-Regular", size: 16.0)
                cell.cellBottomLabel.font = UIFont(name: "Nunito-Regular", size: 10.0)
                return cell
            }
        }
    }
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        if self.items[indexPath.item].type == .photo{
            self.view.endEditing(true)
            let alert = UIAlertController(title: "", message: "Save Image?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { UIAlertAction in
                let url = URL(string: self.items[indexPath.item].content as! String)
                self.tappedImageView.kf.setImage(with: url, placeholder: UIImage(named: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
                self.save()
            }))
            alert.addAction(UIAlertAction(title: "NO", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false
        }else{
            if action == #selector(copy(_:)){
                return true
            }else{
                return false
            }
        }
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.items[indexPath.item].type == .photo{
            scrollImg.delegate = self
            scrollImg.frame = CGRect(x: 0, y: 64, width: self.view.frame.width, height: self.view.frame.height - 64)
            scrollImg.backgroundColor = UIColor(red: 90, green: 90, blue: 90, alpha: 0.90)
            scrollImg.alwaysBounceVertical = false
            scrollImg.alwaysBounceHorizontal = false
            scrollImg.showsVerticalScrollIndicator = true
            scrollImg.flashScrollIndicators()
            scrollImg.minimumZoomScale = 1.0
            scrollImg.maximumZoomScale = 10.0
            self.view.addSubview(scrollImg)
            
            let url = URL(string: self.items[indexPath.item].content as! String)
            tappedImageView.kf.setImage(with: url, placeholder: UIImage(named: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
            tappedImageView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: scrollImg.frame.height)
            tappedImageView.backgroundColor = .black
            tappedImageView.contentMode = .scaleAspectFit
            tappedImageView.isUserInteractionEnabled = true
            scrollImg.addSubview(tappedImageView)
            
            let cancelBtn = UIButton()
            cancelBtn.frame = CGRect(x: self.view.frame.size.width - 40, y: (self.navigationController?.navigationBar.frame.origin.y)! + (self.navigationController?.navigationBar.frame.size.height)! + 10, width: 30, height: 30)
            cancelBtn.addTarget(self, action: #selector(cancelBtnClicked), for: .touchUpInside)
            cancelBtn.setImage(UIImage(named: "cross_blue"), for: .normal)
            self.tappedImageView.addSubview(cancelBtn)
        }
    }
    //MARK: - Saving Image here
    func save() {
        guard let selectedImage = tappedImageView.image else {
            print("Image not found!")
            return
        }
        UIImageWriteToSavedPhotosAlbum(selectedImage, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    //MARK: - Add image to Library
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            showAlertWith(title: "Save error", message: error.localizedDescription)
        } else {
            showAlertWith(title: "Saved!", message: "Your image has been saved to your photos.")
        }
    }
    
    func showAlertWith(title: String, message: String){
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
    }
    //MARK:- CANCEL CLICKED
    @objc func cancelBtnClicked(){
        scrollImg.removeFromSuperview()
    }
    
    override func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.tappedImageView
    }
    //MARK:- SEND MESSAGE CLICKED
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!){
        if NetworkingManager.isConnectedToNetwork(){
        if isRequestClicked == true{ // request money is clicked
            let values = ["type": "text", "content": text, "fromID": senderId, "toID": toId, "timestamp": Int(Date().timeIntervalSince1970), "isRead": false,"isRequestSent":true,"isCancel":false] as [String : Any]
            Message.uploadMessage(withValues: values, toID: toId, completion: { (_) in
            })
            let tokenString = "Bearer " + retrievedToken!
            let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
            let chatNotifyParams = ["userId":self.toId,"message":String(describing: "CSH \(text!) requested")] as [String : Any]
            Alamofire.request(chatNotificationURL, method: .post, parameters: chatNotifyParams, encoding: JSONEncoding.default, headers: Headers).responseJSON { response in
                switch (response.result){
                case .success:break
                case .failure:
                    hideActivityIndicator()
                    break
                }
                debugPrint(response)
            }
        }else{ // normal message
            let values = ["type": "text", "content": text, "fromID": senderId, "toID": toId, "timestamp": Int(Date().timeIntervalSince1970), "isRead": false,"isRequestSent":false,"isCancel":false] as [String : Any]
            Message.uploadMessage(withValues: values, toID: toId, completion: { (_) in
            })
            let tokenString = "Bearer " + retrievedToken!
            let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
            let updatedUserParams = ["userId":self.toId,"message":text] as [String : Any]
            Alamofire.request(chatNotificationURL, method: .post, parameters: updatedUserParams, encoding: JSONEncoding.default, headers: Headers).responseJSON { response in
                switch (response.result){
                case .success:break
                case .failure:
                    hideActivityIndicator()
                    break
                }
                debugPrint(response)
            }
            
//            let message = Message.init(type: .text,content: text, owner: .sender, timestamp: Int(Date().timeIntervalSince1970), isRead: false, senderId: senderId, isRequestSent: false)
//            Message.send(message: message, toID: toId, completion: {(_) in
//                self.sendButtonAction(myMessage: text)
//            })
        }
//        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        }
    }
    //MARK:- REQUEST CANCEL CLICKED
    @objc func reqCancelClicked(sender:UIButton){
        self.items.removeAll()
        Message.isCancelRequest(forUserID: toId, autoID: allMsgKeys[sender.tag] , completion: { (_) in
            Message.reloadAllMessages(forUserID: self.toId, completion: { (allMessages) in
                if allMessages.content as! String == ""{
                    hideActivityIndicator()
                    self.noDataLbl.isHidden = false
                    self.noDataLbl.frame = CGRect(x: 0, y: (self.view.frame.size.height/2)-30, width: self.view.frame.size.width, height: 20)
                    self.noDataLbl.text = "No chat was found"
                    self.noDataLbl.textAlignment = .center
                    self.view.addSubview(self.noDataLbl)
                }else{
                    self.noDataLbl.isHidden = true
                    self.items.append(allMessages)
                    self.items.sort{ $0.timestamp < $1.timestamp }
                    self.messages = []
                    for data in self.items {
                        if let message = JSQMessage(senderId: data.senderId, displayName: self.name, text: data.content as? String){
                            self.messages.append(message)
                        }
                    }
                    self.finishReceivingMessage(animated: false)
                }
                Message.markMessagesRead(forUserID: self.toId)
            })
        })
    }
    //MARK:- REQUEST SEND CLICKED
    @objc func reqSendClicked(sender:UIButton){
        if NetworkingManager.isConnectedToNetwork(){
        let keyStatus = SharedClass.sharedInstance.retrieveFromUserDefaultsBool(key: "kycStatus")
        let bankStatus = SharedClass.sharedInstance.retrieveFromUserDefaultsBool(key: "bankStatus")
        if keyStatus == false || bankStatus == false{
            let alert = UIAlertController(title: "", message: "Please update you bank details & KYC to Add money", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { UIAlertAction in
                let destVc = self.storyboard?.instantiateViewController(withIdentifier: "KYCViewController") as! KYCViewController
                self.navigationController?.pushViewController(destVc, animated: true)
            }))
            alert.addAction(UIAlertAction(title: "CANCEL", style: .default, handler: { UIAlertAction in
                _ = self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }else{
            let amount = UserDefaults.standard.value(forKey: "amount") as! NSNumber
            let enteredString = (self.items[sender.tag].content as! String)
            if let myInteger = Double(enteredString) {
                let enteredNumber = NSNumber(value:myInteger)
                if Double(truncating: enteredNumber) <= Double(truncating: amount){
                    Message.isSendRequest(forUserID: toId, autoID: allMsgKeys[sender.tag], completion: { (_) in
                        self.items.removeAll()
                        Message.reloadAllMessages(forUserID: self.toId, completion: { (allMessages) in
                            if allMessages.content as! String == ""{
                                hideActivityIndicator()
                                self.noDataLbl.isHidden = false
                                self.noDataLbl.frame = CGRect(x: 0, y: (self.view.frame.size.height/2)-30, width: self.view.frame.size.width, height: 20)
                                self.noDataLbl.text = "No chat was found"
                                self.noDataLbl.textAlignment = .center
                                self.view.addSubview(self.noDataLbl)
                            }else{
                                self.noDataLbl.isHidden = true
                                self.items.append(allMessages)
                                self.items.sort{ $0.timestamp < $1.timestamp }
                                self.messages = []
                                for data in self.items {
                                    if let message = JSQMessage(senderId: data.senderId, displayName: self.name, text: data.content as! String){
                                        self.messages.append(message)
                                    }
                                }
                                self.finishReceivingMessage(animated: false)
                            }
                            Message.markMessagesRead(forUserID: self.toId)
                        })
                    })
                    let tokenString = "Bearer " + retrievedToken!
                    let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
                    //                print(self.items[sender.tag].content)
                    let updatedUserParams = ["fromId":senderId,"toId":toId,"amount":self.items[sender.tag].content] as [String : Any]
                    Alamofire.request(ChatPayURL, method: .post, parameters: updatedUserParams, encoding: JSONEncoding.default, headers: Headers).responseJSON { response in
                        switch (response.result){
                        case .success:
                            let json = JSON(response.result.value!)
                            Helper.alertBox(Mymsg: json["message"].string!, view: self)
                        case .failure:
                            hideActivityIndicator()
                            break
                        }
                        debugPrint(response)
                    }
                }else{
                    hideActivityIndicator()
                    Helper.alertBox(Mymsg: "Insufficient balance please buy more coins", view: self)
                }
            }
        }
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }
    }

    func sendButtonAction(myMessage:String) {
        if myMessage.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
            let message6 = JSQMessage(senderId: senderId, displayName: senderDisplayName, text: myMessage)
            self.messages.append(message6!)
            self.finishSendingMessage(animated: true)
        }
    }
    //MARK:- SETUP BACK BUTTON
    func setupBackButton() {
        let backButton: UIButton = UIButton(type: UIButtonType.custom)
        backButton.setImage(UIImage(named: "back_icon"), for: UIControlState.normal)
        backButton.addTarget(self, action:  #selector(backClicked(_:)), for: UIControlEvents.touchUpInside)
        backButton.frame = CGRect(x: 5, y: 20, width: 40, height: 30)
        let barButton = UIBarButtonItem(customView: backButton)
        navigationItem.leftBarButtonItem = barButton
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK:- BACK CLICKED
    @IBAction func backClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
}
//MARK:- JSQMESSAGE TOOLBAR
extension JSQMessagesInputToolbar {
    override open func didMoveToWindow() {
        super.didMoveToWindow()
        guard let window = window else { return }
        if #available(iOS 11.0, *) {
            let anchor = window.safeAreaLayoutGuide.bottomAnchor
            bottomAnchor.constraintLessThanOrEqualToSystemSpacingBelow(anchor, multiplier: 1.0).isActive = true
        }
    }
}
