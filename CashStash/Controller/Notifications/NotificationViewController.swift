//
//  NotificationViewController.swift
//  CashStash
//
//  Created by apple on 8/6/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class NotificationViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var notificationTableVw: UITableView!
    
    var notificationArray = Array<Any>()
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchNotification()
    }
    //MARK:- FETCH NOTIFICATIONS
    func fetchNotification(){                   // getting all notifications
        if NetworkingManager.isConnectedToNetwork(){
            showActivityIndicator()
            let retrievedToken: String? = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "token")
            let tokenString = "Bearer " + retrievedToken!
            let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
            Alamofire.request(getNotificationURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: Headers).responseJSON{ response in
                hideActivityIndicator()
                switch (response.result){
                case .success(let value):
                    let json = JSON(value)
                    if response.response?.statusCode == 200{
                        if let sucessValue = json["success"].string{
                            Helper.alertBox(Mymsg: sucessValue, view: self)
                        }else{
                            self.notificationArray = json.arrayValue as Array<Any>
                            self.notificationArray.reverse()  //sravani
                            if self.notificationArray.count == 0{
                                let nooResults_label = UILabel.init(frame:CGRect(x: 0, y: (self.view.frame.size.height/2)-50, width: self.view.frame.size.width, height: 50))
                                nooResults_label.textColor = UIColor.lightGray
                                nooResults_label.text = "No Transactions Found"
                                nooResults_label.textAlignment = .center
                                nooResults_label.numberOfLines = 0
                                self.view.addSubview(nooResults_label)
                            }else{
                                self.notificationTableVw.delegate = self
                                self.notificationTableVw.dataSource = self
                                self.notificationTableVw.reloadData()
                            }
                        }
                    }
                case .failure(_):
                    print(response.result.error!)
                    break
                }
            }
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }
    }
    //MARK:- TABLEVIEW DELEGATE & DATASOURCE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: NotifyTableViewCell = tableView.dequeueReusableCell(withIdentifier:"NotifyTableViewCell", for: indexPath as IndexPath) as! NotifyTableViewCell
        for cellView in cell.contentView.subviews{
            cellView.removeFromSuperview()
        }
        cell.selectionStyle = .none
        let dict = JSON(notificationArray[indexPath.row])
        
        let dateLbl = UILabel()
        dateLbl.frame = CGRect(x: cell.contentView.frame.size.width - 110, y: 15, width: 95, height: 25)
        dateLbl.font = UIFont(name: "Nunito-Regular", size: 16.0)
        let dateTimeStamp = NSDate(timeIntervalSince1970:Double(dict["createdAt"].int64Value)/1000)
        let dateFormatter = DateFormatter.init()
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let date = dateFormatter.string(from: dateTimeStamp as Date)
        dateLbl.text = date
        dateLbl.textAlignment = .right
        dateLbl.textColor = UIColor(red: 157.0/255.0, green: 145.0/255.0, blue: 171.0/255.0, alpha: 1.0)
        cell.contentView.addSubview(dateLbl)
        
        let titleLbl = UILabel()
        titleLbl.frame = CGRect(x: 20, y: 15, width: cell.contentView.frame.size.width - (dateLbl.frame.size.width + 35), height: 25)
        titleLbl.textColor = UIColor(red: 23.0/255.0, green: 32.0/255.0, blue: 63.0/255.0, alpha: 1.0)
        titleLbl.font = UIFont(name: "Nunito-Regular", size: 18.0)
        titleLbl.text = dict["notificationTitle"].string!
        cell.contentView.addSubview(titleLbl)
        
        let notifyDescriptionLbl = UILabel()
        notifyDescriptionLbl.frame = CGRect(x: 20, y: titleLbl.frame.origin.y + titleLbl.frame.size.height + 10, width: cell.contentView.frame.size.width - 40, height: notifyDescriptionLbl.frame.height)
        notifyDescriptionLbl.text = dict["message"].string!
        notifyDescriptionLbl.textColor = UIColor(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0)
        notifyDescriptionLbl.font = UIFont(name: "Barlow-Regular", size: 16.0)
        notifyDescriptionLbl.numberOfLines = 0
        notifyDescriptionLbl.sizeToFit()
        cell.contentView.addSubview(notifyDescriptionLbl)
        
        let lineView = UIView.init(frame: CGRect(x: 0, y: cell.contentView.frame.size.height - 1, width: cell.frame.size.width, height: 1))
        lineView.backgroundColor = UIColor.init(red:  238.0/255.0, green: 228.0/255.0, blue: 249.0/255.0, alpha: 1.0)
        cell.contentView.addSubview(lineView)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let notifyDescriptionLbl = UILabel()
        notifyDescriptionLbl.frame = CGRect(x: 20, y: 50, width: self.view.frame.size.width - 40, height: notifyDescriptionLbl.frame.height)
        let dict = JSON(notificationArray[indexPath.row])
        notifyDescriptionLbl.text = dict["message"].string!
        notifyDescriptionLbl.textColor = UIColor(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0)
        notifyDescriptionLbl.font = UIFont(name: "Barlow-Regular", size: 16.0)
        notifyDescriptionLbl.numberOfLines = 0
        notifyDescriptionLbl.sizeToFit()
        return 70 + (notifyDescriptionLbl.frame.height)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK:- BACK CLICKED
    @IBAction func backClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
}
