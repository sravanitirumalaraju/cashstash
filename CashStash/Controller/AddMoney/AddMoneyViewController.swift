//
//  AddMoneyViewController.swift
//  CashStash
//
//  Created by apple on 7/29/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class AddMoneyViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var enterAmountTxtFld: FloatLabelTextField!
    @IBOutlet weak var payButton: UIButton!
    @IBOutlet weak var paymentGatewayBtn: UIButton!
    @IBOutlet weak var ACHButton: UIButton!
    @IBOutlet weak var paymentLineVw: UIView!
    @IBOutlet weak var ACHLineVw: UIView!
    @IBOutlet weak var paymentTypeLbl: UILabel!
    
    var KYCDictionary = NSDictionary()
    var BankDictionary = NSDictionary()
    var userDict = NSDictionary()
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        enterAmountTxtFld.delegate = self
        payButton.layer.cornerRadius = payButton.frame.size.height/2
        payButton.clipsToBounds = true
        payButton.layer.shadowColor = UIColor(white: 0.0, alpha: 0.25).cgColor
        payButton.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        payButton.layer.shadowOpacity = 1.0
        payButton.layer.shadowRadius = 3
        payButton.layer.masksToBounds = false
        
        fetchUserProfileDetails()
    }
    
    //MARK:- FETCH USER PROFILE DETAILS
    func fetchUserProfileDetails(){
        if NetworkingManager.isConnectedToNetwork(){
            showActivityIndicator()
            let retrievedToken: String? = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "token")
            let tokenString = "Bearer " + retrievedToken!
            let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
            Alamofire.request(GetUserProfileURL, headers: Headers).responseJSON { response in
                print(response)
                hideActivityIndicator()
                switch (response.result){
                case .success:
                    let json = JSON(response.result.value!)
                    if let _ = json["message"].string{
                        Helper.alertBox(Mymsg: json["message"].string!, view: self)
                    }else{
                        self.userDict = response.result.value! as! NSDictionary
                        self.KYCDictionary = self.userDict["kycDetails"] as! NSDictionary
                        self.BankDictionary = self.userDict["userBankDetails"] as! NSDictionary
                    }
                case .failure:
                    print(response.result.error!)
                    break
                }
                debugPrint(response)
            }
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }
    }
    //MARK:- TEXTFIELD DELEGATE
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    //MARK:- PAY CLICKED
    @IBAction func payClicked(_ sender: Any) {
        if enterAmountTxtFld.text! != ""{
        let keyStatus = SharedClass.sharedInstance.retrieveFromUserDefaultsBool(key: "kycStatus")
        let bankStatus = SharedClass.sharedInstance.retrieveFromUserDefaultsBool(key: "bankStatus")
        if keyStatus == false || bankStatus == false {
            let alert = UIAlertController(title: "", message: "Please update you bank details & KYC to Add money", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { UIAlertAction in
                let destVc = self.storyboard?.instantiateViewController(withIdentifier: "KYCViewController") as! KYCViewController
                print(self.KYCDictionary)
                if (self.KYCDictionary["ssn"] as! String) != ""{
                    destVc.KYCDictionary = self.KYCDictionary
                }
                if (self.BankDictionary["accountHolderName"] as! String) != ""{
                    destVc.BankDictionary = self.BankDictionary
                }
                self.navigationController?.pushViewController(destVc, animated: true)
            }))
            alert.addAction(UIAlertAction(title: "CANCEL", style: .default, handler: { UIAlertAction in
                _ = self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }else if self.userDict["address"] as! String == ""{
            let alert = UIAlertController(title: "", message: "Please update you user details", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { UIAlertAction in
                let destVc = self.storyboard?.instantiateViewController(withIdentifier: "userDetailsViewController") as! userDetailsViewController
                self.navigationController?.pushViewController(destVc, animated: true)
            }))
            alert.addAction(UIAlertAction(title: "CANCEL", style: .default, handler: { UIAlertAction in
                _ = self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }
        else{
                //KYC is done then updating addmoney details
            if NetworkingManager.isConnectedToNetwork(){
                let retrievedToken: String? = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "token")
                let tokenString = "Bearer " + retrievedToken!
                let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
                showActivityIndicator()
                let parms = ["amountRequested":enterAmountTxtFld.text!] as [String:Any]
                Alamofire.request(updateACHDetailsURL, method: .put, parameters: parms, encoding: JSONEncoding.default, headers: Headers).responseJSON{ response in
                    hideActivityIndicator()
                    print(response)
                    switch (response.result){
                    case .success:
                        let json = JSON(response.result.value!)
//                                print(json)
                        Helper.alertBox(Mymsg: json["message"].string!, view: self)
//                                let alert = UIAlertController(title: "", message: json["message"].string!, preferredStyle: .alert)
//                                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { UIAlertAction in
//                                    let destVc = self.storyboard?.instantiateViewController(withIdentifier: "WalletViewController") as! WalletViewController
//                                    self.navigationController?.pushViewController(destVc, animated: true)
//                                }))
//                                self.present(alert, animated: true, completion: nil)
                        self.enterAmountTxtFld.text = ""
                    case .failure:
                        print("error")
                    }
                }
            }else{
                NetworkingManager.netWorkFailed(view: self)
            }
            
        }
        }else{
            Helper.alertBox(Mymsg: "Please enter Amount", view: self)
        }
    }
    //MARK:- BACK CLICKED
    @IBAction func backClicked(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    //MARK:-WALLET BUTTON CLICKED
    @IBAction func paymentGatewayClicked(_ sender: Any) {
       paymentTypeLbl.text = "Payment Gateway"
        ACHButton.setTitleColor(UIColor.init(red: 157.0/255.0, green: 145.0/255.0, blue: 171.0/255.0, alpha: 1.0), for: .normal)
        ACHLineVw.backgroundColor = UIColor.white
        
        paymentGatewayBtn.setTitleColor(UIColor.init(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0), for: .normal)
        paymentLineVw.backgroundColor = UIColor.init(red: 36.0/255.0, green: 137.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        
    }
    //MARK:- REQUEST BUTTON CLICKED
    @IBAction func ACHClicked(_ sender: Any) {
        paymentTypeLbl.text = "ACH"
        paymentGatewayBtn.setTitleColor(UIColor.init(red: 157.0/255.0, green: 145.0/255.0, blue: 171.0/255.0, alpha: 1.0), for: .normal)
        paymentLineVw.backgroundColor = UIColor.white
        
        ACHButton.setTitleColor(UIColor.init(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0), for: .normal)
        ACHLineVw.backgroundColor = UIColor.init(red: 36.0/255.0, green: 137.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
