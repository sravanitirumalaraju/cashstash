//
//  QRScanPayViewController.swift
//  CashStash
//
//  Created by apple on 4/25/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit
import AVFoundation

class QRScanPayViewController: UIViewController,AVCaptureMetadataOutputObjectsDelegate,UITextFieldDelegate {
    struct Platform {
        static let isSimulator: Bool = {
            var isSim = false
            #if arch(i386) || arch(x86_64)
            isSim = true
            #endif
            return isSim
        }()
    }
    @IBOutlet weak var mobileBackgroundVw: UIView!
    @IBOutlet weak var scanBackgroundVw: UIView!
    @IBOutlet weak var MobileNoTxtFld: UITextField!
    var objCaptureSession:AVCaptureSession?
    var objCaptureVideoPreviewLayer:AVCaptureVideoPreviewLayer?
    var vwQRCode:UIView?
    let mobileNo = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "mobile")
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad(){
        super.viewDidLoad()
        MobileNoTxtFld.delegate = self
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneButtonAction))
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        MobileNoTxtFld.inputAccessoryView = doneToolbar
    }
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        print(scanBackgroundVw.frame.size.height)
        print(scanBackgroundVw.frame.origin.y)
        if Platform.isSimulator == false{
            if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
                self.configureVideoCapture()
                self.addVideoPreviewLayer()
                self.initializeQRView()
            } else {
                AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                    if granted {
                        self.configureVideoCapture()
                        self.addVideoPreviewLayer()
                        self.initializeQRView()
                    } else {
                        Helper.alertBox(Mymsg: "No access has given to camera", view: self)
                    }
                })
            }
        }
//        else{
//            scanBackgroundVw.backgroundColor = UIColor.orange
//        }
    }
    //MARK:- DONE BUTTON CLICKED
    @objc func doneButtonAction() {
        MobileNoTxtFld.resignFirstResponder()
    }
    //MARK:- PROCEED CLICKED
    @IBAction func proceedClicked(_ sender: Any) {
        if MobileNoTxtFld.text == ""{
            Helper.alertBox(Mymsg: "Please enter mobile number to proceed", view: self)
        }else{
            let destVc = self.storyboard?.instantiateViewController(withIdentifier: "PayNowViewController") as! PayNowViewController
            destVc.mobileNumberString = MobileNoTxtFld.text!
            self.navigationController?.pushViewController(destVc, animated: true)
        }
    }
   
    //MARK:- CONFIGURE VIDEO CAPTURE
    func configureVideoCapture() {
        let objCaptureDevice = AVCaptureDevice.default(for: AVMediaType.video)
        var error:NSError?
        let objCaptureDeviceInput: AnyObject!
        do {
            objCaptureDeviceInput = try AVCaptureDeviceInput(device: objCaptureDevice!) as AVCaptureDeviceInput
        } catch let error1 as NSError {
            error = error1
            objCaptureDeviceInput = nil
        }
        
        if (error != nil) {
            let alert = UIAlertController(title: "Device Error", message: "Device not Supported for this Application", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        objCaptureSession = AVCaptureSession()
        objCaptureSession?.addInput(objCaptureDeviceInput as! AVCaptureInput)
        let objCaptureMetadataOutput = AVCaptureMetadataOutput()
        objCaptureSession?.addOutput(objCaptureMetadataOutput)
        objCaptureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        objCaptureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
    }
    func addVideoPreviewLayer(){
        objCaptureVideoPreviewLayer = AVCaptureVideoPreviewLayer(session: objCaptureSession!)
        objCaptureVideoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        let cameraHeight = mobileBackgroundVw.frame.origin.y + mobileBackgroundVw.frame.size.height
        objCaptureVideoPreviewLayer?.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height - cameraHeight)

//            objCaptureVideoPreviewLayer?.frame = CGRect(x: 0, y: 0, width: scanBackgroundVw.frame.size.width, height: scanBackgroundVw.frame.size.height)
//        objCaptureVideoPreviewLayer?.frame = scanBackgroundVw.bounds
        self.scanBackgroundVw.layer.insertSublayer(self.objCaptureVideoPreviewLayer!, at: 0) // sravani
//        self.view.layer.addSublayer(objCaptureVideoPreviewLayer!)
//        scanBackgroundVw.layer.addSublayer(objCaptureVideoPreviewLayer!)
        objCaptureSession?.startRunning()
    }
    //MARK:- QRVIEW INITIALIZE
    func initializeQRView() {
        vwQRCode = UIView()
        vwQRCode?.layer.borderColor = AppColors.APP_COLOR_BLUE.cgColor
        vwQRCode?.layer.borderWidth = 2
        scanBackgroundVw.addSubview(vwQRCode!)
        
        let orLabel = UILabel()
        orLabel.text = "Or"
        orLabel.frame = CGRect(x: 0, y: 20, width: 100, height: 20)
        orLabel.center.x = self.view.center.x
        orLabel.font = UIFont(name: "AvenirNext-Regular", size: 16.0)
        orLabel.textAlignment = .center
        orLabel.textColor = UIColor.white
        vwQRCode?.addSubview(orLabel)
        
        let scanLabel = UILabel()
        scanLabel.text = "Scan QR Code for Payment"
        scanLabel.frame = CGRect(x: 0, y: orLabel.frame.origin.y + orLabel.frame.size.height + 20, width: 250, height: 20)
        scanLabel.center.x = self.view.center.x
        scanLabel.font = UIFont(name: "AvenirNext-Regular", size: 16.0)
        scanLabel.textAlignment = .center
        scanLabel.textColor = UIColor.white
        vwQRCode?.addSubview(scanLabel)
    }
    //MARK:- SCAN QRCODE
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if metadataObjects.isEmpty == true || metadataObjects.count == 0 {
            return
        }
        let objMetadataMachineReadableCodeObject = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        if objMetadataMachineReadableCodeObject.type == AVMetadataObject.ObjectType.qr {
            let objBarCode = objCaptureVideoPreviewLayer?.transformedMetadataObject(for: objMetadataMachineReadableCodeObject as AVMetadataMachineReadableCodeObject) as! AVMetadataMachineReadableCodeObject
            vwQRCode?.frame = objBarCode.bounds
            let string = (objMetadataMachineReadableCodeObject.stringValue!)
            if let range = string.range(of: "@cashstash.me") {
                let mobileString = string[string.startIndex..<range.lowerBound]
                if mobileString != "" && String(mobileString) != mobileNo{
                    let destVc = self.storyboard?.instantiateViewController(withIdentifier: "PayNowViewController") as! PayNowViewController
                    destVc.mobileNumberString = String(mobileString)
                    self.navigationController?.pushViewController(destVc, animated: true)
                }else{
                     _ = self.navigationController?.popViewController(animated: true)
                }
            }else{
                _ = self.navigationController?.popViewController(animated: true)
            }
            self.objCaptureSession?.stopRunning()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:- BACK CLICKED
    @IBAction func backClicked(_ sender: Any) {
      _ = self.navigationController?.popViewController(animated: true)
    }
}
