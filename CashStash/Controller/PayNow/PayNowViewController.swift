//
//  PayNowViewController.swift
//  CashStash
//
//  Created by apple on 31/01/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class PayNowViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource{

    @IBOutlet var mobileNoTxtFld: UITextField!
    @IBOutlet var enterAmountTxtFld: FloatLabelTextField!
    @IBOutlet var reasonTxtFld: FloatLabelTextField!
    @IBOutlet var descripTxtFld: FloatLabelTextField!
    @IBOutlet weak var reasonTableView: UITableView!
    @IBOutlet weak var selectReasonBtn: UIButton!
    @IBOutlet weak var selectReasonView: UIView!
    @IBOutlet weak var xlmLabel: UILabel!
    
    var reasonsArray = [String]()
    var blurView = UIView()
    var isAdded = Bool()
    var mobileNumberString = String()
    let retrievedToken: String? = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "token")
    var paidToNameString = String()
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = UIColor(red: 5.0/255.0, green: 78.0/255.0, blue: 106.0/255.0, alpha: 1.0)
        
        mobileNoTxtFld.text = mobileNumberString
        selectReasonView.isHidden = true
        self.reasonTableView.separatorStyle = .none
        reasonTxtFld.returnKeyType = .go
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneButtonAction))
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        mobileNoTxtFld.inputAccessoryView = doneToolbar
        enterAmountTxtFld.inputAccessoryView = doneToolbar
        enterAmountTxtFld.keyboardType = .decimalPad
        
        blurView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        blurView.backgroundColor = AppColors.APP_BLURVIEW_COLOR
        view.addSubview(blurView)
        blurView.isHidden = true
        
        getReasonInfo()
        xlmLabel.text = ""
    }
    //MARK:- TEXT FIELD DELEGATES
    @IBAction func textFiledEditing(_ sender: AnyObject) {
        if enterAmountTxtFld.text! != "" {
            xlmLabel.textColor = UIColor(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0)
            let amountText = NSMutableAttributedString.init(string: "CSH\(enterAmountTxtFld.text!)")
            amountText.setAttributes([NSAttributedStringKey.font : UIFont(name: "Barlow-Medium", size: 12.0)!,NSAttributedStringKey.foregroundColor : UIColor(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0)], range: NSMakeRange(0, 3))
            xlmLabel.attributedText = amountText
        }else{
            xlmLabel.text = ""
        }
    }
    //MARK:- GET REASON INFO
    func getReasonInfo(){
        if NetworkingManager.isConnectedToNetwork(){
            let tokenString = "Bearer " + retrievedToken!
            let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
            Alamofire.request(getSpendReasonURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: Headers).responseJSON{ response in
                self.reasonsArray.removeAll()
                switch (response.result){
                case .success:
                    let json = response.result.value
                    self.reasonsArray = json as! [String]
                    if self.isAdded == true{
                        self.selectReasonView.isHidden = false
                        self.reasonTableView.reloadData()
                    }
                case .failure:
                    hideActivityIndicator()
                    print("error")
                }
            }
        }
    }
    //MARK:-function which is triggered when handleTap is called
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        let keyStatus = SharedClass.sharedInstance.retrieveFromUserDefaultsBool(key: "kycStatus")
        let bankStatus = SharedClass.sharedInstance.retrieveFromUserDefaultsBool(key: "bankStatus")
        if keyStatus == false || bankStatus == false{
            let alert = UIAlertController(title: "", message: "Please update you bank details & KYC to Add money", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { UIAlertAction in
                let destVc = self.storyboard?.instantiateViewController(withIdentifier: "KYCViewController") as! KYCViewController
                self.navigationController?.pushViewController(destVc, animated: true)
            }))
            alert.addAction(UIAlertAction(title: "CANCEL", style: .default, handler: { UIAlertAction in
                 _ = self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    //MARK:- DONE BUTTON ACTION
    @objc func doneButtonAction() {
        mobileNoTxtFld.resignFirstResponder()
        enterAmountTxtFld.resignFirstResponder()
    }
    //MARK:- TEXTFIELD DELEGATES
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == reasonTxtFld{
            addReason()
            isAdded = true
        }else{
           textField.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == enterAmountTxtFld{
            guard let oldText = enterAmountTxtFld.text, let r = Range(range, in: oldText) else {
                return true
            }
            
            let newText = oldText.replacingCharacters(in: r, with: string)
            let isNumeric = newText.isEmpty || (Double(newText) != nil)
            let numberOfDots = newText.components(separatedBy: ".").count - 1
            
            let numberOfDecimalDigits: Int
            if let dotIndex = newText.index(of: ".") {
                numberOfDecimalDigits = newText.distance(from: dotIndex, to: newText.endIndex) - 1
            } else {
                numberOfDecimalDigits = 0
            }
            
            return isNumeric && numberOfDots <= 1 && numberOfDecimalDigits <= 2
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == reasonTxtFld{
            textField.isUserInteractionEnabled = true
        }
    }
    //MARK:- SELECT REASON CLICKED
    @IBAction func selectReasonClicked(_ sender: Any) {
        selectReason()
    }
    
    func selectReason(){
        reasonTxtFld.resignFirstResponder()
        self.selectReasonView.isHidden = false
        self.reasonTableView.reloadData()
        if isAdded == true{
            getReasonInfo()
        }
    }
    //MARK:-CLOSE REASON CLICKED
    @IBAction func closeReasonClicked(_ sender: Any) {
         self.selectReasonView.isHidden = true
    }
    //MARK:- TABLEVIEW DELEGATES
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if reasonsArray.count != 0{
            return reasonsArray.count
        }else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reasonTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ReasonsTableViewCell", for: indexPath) as! ReasonsTableViewCell
        reasonTableViewCell.selectionStyle = .none
        reasonTableViewCell.backgroundColor =  AppColors.APP_BACKGROUND_COLOR
        if reasonsArray.count == 0{
            reasonTableViewCell.reasonLabel.text = "No reasons found"
            reasonTableViewCell.deleteButton.isHidden = true
        }else{
            reasonTableViewCell.deleteButton.isHidden = false
            reasonTableViewCell.reasonLabel.text = reasonsArray[indexPath.row]
            reasonTableViewCell.reasonLabel.textColor = AppColors.APP_COLOR_BLUE
            reasonTableViewCell.deleteButton.tag = indexPath.row
            reasonTableViewCell.deleteButton.addTarget(self, action: #selector(deleteClicked), for: .touchUpInside)
        }
        return reasonTableViewCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if reasonsArray.count != 0{
            selectReasonView.isHidden = true
            reasonTxtFld.text = reasonsArray[indexPath.row]
        }
    }
    //MARK:- DELETE CLICKED
    @objc func deleteClicked(sender:UIButton){
        if NetworkingManager.isConnectedToNetwork(){
            let tokenString = "Bearer " + retrievedToken!
            let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
            let parms = ["reason":reasonsArray[sender.tag]]
            Alamofire.request(deleteReasonURL, method: .put, parameters: parms, encoding: JSONEncoding.default, headers: Headers).responseJSON{ response in
                self.reasonsArray.remove(at: sender.tag)
                self.reasonTableView.reloadData()
            }
        }else{
             NetworkingManager.netWorkFailed(view: self)
        }
    }
    //MARK:- ADD REASON CLICKED
    func addReason(){
        if NetworkingManager.isConnectedToNetwork(){
            if reasonTxtFld.text! == ""{
                self.reasonTxtFld.resignFirstResponder()
                Helper.alertBox(Mymsg: "please enter reason", view: self)
            }else{
                let tokenString = "Bearer " + retrievedToken!
                let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
                let parms = ["reason":reasonTxtFld.text!]
                Alamofire.request(addSpendReasonURL, method: .put, parameters: parms, encoding: JSONEncoding.default, headers: Headers).responseJSON{ response in
                    self.reasonTxtFld.resignFirstResponder()
                }
            }
        }else{
             NetworkingManager.netWorkFailed(view: self)
        }
    }
   
   //MARK:- ADD REASON CLICKED
    @IBAction func addReasonClicked(_ sender: Any) {
        reasonTxtFld.isUserInteractionEnabled = true
        reasonTxtFld.becomeFirstResponder()
        reasonTxtFld.text = ""
        selectReasonView.isHidden = true
    }
    //MARK:-HOME CLICKED
    @IBAction func homeClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- WALLET CLICKED
    @IBAction func walletClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "TransactionsViewController") as! TransactionsViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- CONTACT CLICKED
    @IBAction func contactClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "FavouriteListViewController") as! FavouriteListViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:-CHAT CLICKED
    @IBAction func chatClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:-PROFILE CLICKED
    @IBAction func profileClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:-BACK CLICKED
    @IBAction func backClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    //MARK:- PAYNOW CLICKED
    @IBAction func payNowClicked(_ sender: Any) {
        if NetworkingManager.isConnectedToNetwork(){
            if mobileNoTxtFld.text! == ""{
                Helper.alertBox(Mymsg: "please enter mobile number", view: self)
            }else if enterAmountTxtFld.text! == ""{
                Helper.alertBox(Mymsg: "Please enter amount to be payed", view: self)
            }else if reasonTxtFld.text! == ""{
                Helper.alertBox(Mymsg: "Please select the reason", view: self)
            }else {
                let amount = UserDefaults.standard.value(forKey: "amount") as! NSNumber
                let enteredString = enterAmountTxtFld.text!
                if let myInteger = Double(enteredString) {
                    let enteredNumber = NSNumber(value:myInteger)
                    if Double(truncating: enteredNumber) <= Double(truncating: amount){
                        showActivityIndicator()
                        let tokenString = "Bearer " + retrievedToken!
                        let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
                        let parms = ["paidToContactNumber":mobileNoTxtFld.text!,"amount":enterAmountTxtFld.text!,"reason":reasonTxtFld.text!,"description":descripTxtFld.text!]
                        Alamofire.request(payMoneyURL, method: .post, parameters: parms, encoding: JSONEncoding.default, headers: Headers).responseJSON{ response in
                            hideActivityIndicator()
                            switch (response.result){
                            case .success:
                                let destVc = self.storyboard?.instantiateViewController(withIdentifier: "SucessPaymentViewController") as! SucessPaymentViewController
                                destVc.paidToPerson = self.paidToNameString
                                destVc.paidToNumber = self.mobileNumberString
                                self.navigationController?.pushViewController(destVc, animated: true)
                            case .failure:
                                print("error")
                            }
                            debugPrint(response)
                        }
                    }else{
                        Helper.alertBox(Mymsg: "Insufficient balance please buy more coins", view: self)
                    }
                }
            }
        }else{
             NetworkingManager.netWorkFailed(view: self)
        }
    }
    //MARK:- NOTIFICATION CLICKED
    @IBAction func notificationClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
