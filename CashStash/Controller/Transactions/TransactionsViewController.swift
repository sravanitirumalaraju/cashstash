//
//  TransactionsViewController.swift
//  CashStash
//
//  Created by apple on 28/03/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class TransactionsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var filterImgVw: UIImageView!
    @IBOutlet var filterBtn: UIButton!
    @IBOutlet var headerLbl: UILabel!
    @IBOutlet var blurView: UIView!
    @IBOutlet var transactionTableVw: UITableView!
    @IBOutlet var filterView: UIView!
    
    var transactionArray = Array<Any>()
    var isRecentClicked = Bool()
    var spendingsArray = Array<Any>()
    var MWtoBankArray = Array<Any>()
    var bankToMWArray = Array<Any>()
    var MWtoSWArray = Array<Any>()
    var SWtoMWArray = Array<Any>()
    var commonArray = Array<Any>()
   
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        if isRecentClicked == true{
            headerLbl.text = "Recent Transactions"
            filterBtn.isHidden = true
            filterImgVw.isHidden = true
        }
        fetchTransactions()
        let viewTap = UITapGestureRecognizer(target: self, action: #selector(self.viewTap(_:)))
        blurView.addGestureRecognizer(viewTap)
        blurView.isUserInteractionEnabled = true
    }
    //MARK:- function which is triggered when viewTap is called
    @objc func viewTap(_ sender: UITapGestureRecognizer) {
        blurView.isHidden = true
    }
    //MARK:- FETCH TRANSACTIONS
    func fetchTransactions(){           
        if NetworkingManager.isConnectedToNetwork(){
        showActivityIndicator()
        let retrievedToken: String? = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "token")
        let tokenString = "Bearer " + retrievedToken!
        let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
        if isRecentClicked == false{
            Alamofire.request(transactionsURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: Headers).responseJSON{ response in
                hideActivityIndicator()
                self.commonArray.removeAll()
                switch (response.result){
                case .success(let value):
                    let json = JSON(value)
                    if response.response?.statusCode == 200{
                        if let sucessValue = json["success"].string{
                            Helper.alertBox(Mymsg: sucessValue, view: self)
                        }else{
                            self.transactionArray = json.arrayValue as Array<Any>
                            self.commonArray = self.transactionArray
                            for (i,_) in self.transactionArray.enumerated(){
                                let indDict = JSON(self.transactionArray[i])
                                if indDict["transactionType"].string! == "SPENDINGS"{
                                    self.spendingsArray.append(indDict)
                                }else if indDict["transactionType"].string! == "BANK_TO_USER_WALLET"{
                                    self.bankToMWArray.append(indDict)
                                }else if indDict["transactionType"].string! == "USER_WALLET_TO_BANK"{
                                    self.MWtoBankArray.append(indDict)
                                }else if indDict["transactionType"].string! == "MAIN_WALLET_TO_SHARED_WALLET"{
                                    self.MWtoSWArray.append(indDict)
                                }else if indDict["transactionType"].string! == "SHARED_WALLET_TO_MAIN_WALLET"{
                                    self.SWtoMWArray.append(indDict)
                                }
                            }
                            self.commonArray.reverse()
                            self.spendingsArray.reverse()
                            self.bankToMWArray.reverse()
                            self.MWtoBankArray.reverse()
                            self.MWtoSWArray.reverse()
                            self.SWtoMWArray.reverse()
                            self.transactionTableVw.delegate = self
                            self.transactionTableVw.dataSource = self
                            self.transactionTableVw.reloadData()
                        }
                    }
                case .failure(_):
                    print(response.result.error!)
                    break
                }
            }
        }
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }
    }
    
    //MARK:- TABLEVIEW DELEGATES
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if commonArray.count != 0{
            return commonArray.count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: transactionTableViewCell = tableView.dequeueReusableCell(withIdentifier:"Cell", for: indexPath as IndexPath) as! transactionTableViewCell
        if commonArray.count == 0{
            cell.receivedAmountLbl.isHidden = true
            cell.dateLbl.isHidden = true
            cell.fromDataLbl.isHidden = true
            cell.toDataLbl.isHidden = true
            tableView.isScrollEnabled = false
            let nooResults_label = UILabel.init(frame:CGRect(x: 0, y: 0, width: self.transactionTableVw.frame.size.width, height: transactionTableVw.frame.size.height))
            nooResults_label.textColor = UIColor.lightGray
            nooResults_label.text = "No Transactions"
            nooResults_label.textAlignment = .center
            nooResults_label.numberOfLines = 0
            cell.contentView.addSubview(nooResults_label)
        }else{
            cell.receivedAmountLbl.isHidden = false
            cell.dateLbl.isHidden = false
            cell.fromDataLbl.isHidden = false
            cell.toDataLbl.isHidden = false
            tableView.isScrollEnabled = true
            let dict = JSON(commonArray[indexPath.row])
            cell.receivedAmountLbl.text = String("CSH \(dict["amount"].int!)")
            let dateTimeStamp = NSDate(timeIntervalSince1970:Double(dict["transactionDate"].int64Value)/1000)
            let dateFormatter = DateFormatter.init()
            dateFormatter.locale = NSLocale.current
            dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
            let date = dateFormatter.string(from: dateTimeStamp as Date)
            cell.dateLbl.text = date
            
            let transacDict = JSON(dict["transactionBetween"].dictionary!)
            cell.fromDataLbl.text = "Paid to \(String(describing: transacDict["to"].string!))"
            cell.toDataLbl.text = "Debited from \(String(describing: transacDict["from"].string!))"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if commonArray.count != 0{
            return 100
        }else{
            return self.transactionTableVw.frame.size.height
        }
    }
    
    //MARK:- BACK CLICKED
    @IBAction func backClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    //MARK:- FILTER CLICKED
    @IBAction func filterClicked(_ sender: Any) {
        blurView.isHidden = false
        blurView.backgroundColor = AppColors.APP_BLURVIEW_COLOR
    }
    //MARK:- SPENDINGS CLICKED
    @IBAction func spendingsClicked(_ sender: Any) {
        commonArray.removeAll()
        commonArray = spendingsArray
        filters()
    }
    //MARK:- MAIN TO SHARE WALLET
    @IBAction func mainWallToShareWallClicked(_ sender: Any) {
        commonArray.removeAll()
        commonArray = MWtoSWArray
        filters()
    }
    //MARK:- SHARE WALLET TO MAIN
    @IBAction func ShareWallTomainWallClicked(_ sender: Any) {
        commonArray.removeAll()
        commonArray = SWtoMWArray
        filters()
    }
    //MARK:- MAIN TO BANK
    @IBAction func mainWallToBankClicked(_ sender: Any) {
        commonArray.removeAll()
        commonArray = MWtoBankArray
        filters()
    }
    //MARK:- BANK TO MAIN
    @IBAction func BankTomainWallClicked(_ sender: Any) {
        commonArray.removeAll()
        commonArray = bankToMWArray
        filters()
    }
    //MARK:- FILTERS
    func filters(){
        self.transactionTableVw.delegate = self
        self.transactionTableVw.dataSource = self
        self.transactionTableVw.reloadData()
        blurView.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
