//
//  SwTransacViewController.swift
//  CashStash
//
//  Created by apple on 8/6/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SwTransacViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var SWTransacTableVw: UITableView!
    @IBOutlet weak var filterBlurView: UIView!
    @IBOutlet weak var filterView: UIView!
    
    var shareWalletId = String()
    var transactionArray = Array<Any>()
    var contributionArray = Array<Any>()
    var withdrawArray = Array<Any>()
    var commonArray = Array<Any>()
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchTransactions()
        let filtertap = UITapGestureRecognizer(target: self, action: #selector(self.filterTap(_:)))
        filterBlurView.addGestureRecognizer(filtertap)
        filterBlurView.isUserInteractionEnabled = true
    }
    //MARK:- function which is triggered when filterTap is called
    @objc func filterTap(_ sender: UITapGestureRecognizer) {
        filterBlurView.isHidden = true
    }
    
    //MARK:- FETCH TRANSACTIONS
    func fetchTransactions(){       //getting share wallet recent transactions
        if NetworkingManager.isConnectedToNetwork(){
            showActivityIndicator()
            let retrievedToken: String? = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "token")
            let tokenString = "Bearer " + retrievedToken!
            let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
            Alamofire.request("\(SWRecentTransactionsURL)\(shareWalletId)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: Headers).responseJSON{ response in
                hideActivityIndicator()
                self.commonArray.removeAll()
                switch (response.result){
                case .success(let value):
                    let json = JSON(value)
                    if response.response?.statusCode == 200{
                        if let sucessValue = json["success"].string{
                            Helper.alertBox(Mymsg: sucessValue, view: self)
                        }else{
                            self.transactionArray = json.arrayValue as Array<Any>
                            self.commonArray = self.transactionArray
                            if self.transactionArray.count == 0{
                                let nooResults_label = UILabel.init(frame:CGRect(x: 0, y: (self.view.frame.size.height/2)-50, width: self.view.frame.size.width, height: 50))
                                nooResults_label.textColor = UIColor.lightGray
                                nooResults_label.text = "No Transactions Found"
                                nooResults_label.textAlignment = .center
                                nooResults_label.numberOfLines = 0
                                self.view.addSubview(nooResults_label)
                            }else{
                                for (i,_) in self.transactionArray.enumerated(){
                                    let indDict = JSON(self.transactionArray[i])
                                    if indDict["transactionType"].string! == "CONTRIBUTION"{
                                        self.contributionArray.append(indDict)
                                    }else if indDict["transactionType"].string! == "WITHDRAW"{
                                        self.withdrawArray.append(indDict)
                                    }
                                }
                                self.SWTransacTableVw.delegate = self
                                self.SWTransacTableVw.dataSource = self
                                self.SWTransacTableVw.reloadData()
                            }
                        }
                    }
                case .failure(_):
                    print(response.result.error!)
                    break
                }
            }
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }
    }
    //MARK:- TABLEVIEW DELEGATE & DATASOURCE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if commonArray.count != 0{
            return commonArray.count
        }else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SWTransacTableViewCell = tableView.dequeueReusableCell(withIdentifier:"SWTransacTableViewCell", for: indexPath as IndexPath) as! SWTransacTableViewCell
        if commonArray.count == 0{
            cell.descriptionLbl.isHidden = true
            cell.dateLbl.isHidden = true
            cell.coinsLbl.isHidden = true
            cell.statusLbl.isHidden = true
            tableView.isScrollEnabled = false
            let nooResults_label = UILabel.init(frame:CGRect(x: 0, y: 0, width: SWTransacTableVw.frame.size.width, height: SWTransacTableVw.frame.size.height))
            nooResults_label.textColor = UIColor.lightGray
            nooResults_label.text = "No Transactions"
            nooResults_label.textAlignment = .center
            nooResults_label.numberOfLines = 0
            cell.contentView.addSubview(nooResults_label)
        }else{
            cell.descriptionLbl.isHidden = false
            cell.dateLbl.isHidden = false
            cell.coinsLbl.isHidden = false
            cell.statusLbl.isHidden = false
            tableView.isScrollEnabled = true
            let dict = JSON(commonArray[indexPath.row])
            let dateTimeStamp = NSDate(timeIntervalSince1970:Double(dict["createdAt"].int64Value)/1000)
            let dateFormatter = DateFormatter.init()
            dateFormatter.locale = NSLocale.current
            dateFormatter.dateFormat = "MM/dd/yyyy"
            let date = dateFormatter.string(from: dateTimeStamp as Date)
            cell.dateLbl.text = date
            cell.dateLbl.textColor = UIColor(red: 157.0/255.0, green: 145.0/255.0, blue: 171.0/255.0, alpha: 1.0)
            
            cell.descriptionLbl.numberOfLines = 0
            cell.descriptionLbl.sizeToFit()
            cell.descriptionLbl.textColor = UIColor(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0)
            cell.descriptionLbl.text = dict["description"].string!
            
            cell.coinsLbl.text = "CSH \(String(describing: dict["coinsTransacted"].int!))"
            cell.coinsLbl.textColor = UIColor(red: 23.0/255.0, green: 32.0/255.0, blue: 63.0/255.0, alpha: 1.0)
            
            if (dict["transactionType"].string!) == "CONTRIBUTION"{
                cell.statusLbl.text = "Contribution"
                cell.statusLbl.textColor = UIColor(red: 76.0/255.0, green: 217.0/255.0, blue: 100.0/255.0, alpha: 1.0)
            }else{
                cell.statusLbl.text = "Withdrawn"
                cell.statusLbl.textColor = UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if commonArray.count != 0{
            return 120
        }else{
            return self.SWTransacTableVw.frame.size.height
        }
    }
    //MARK:- FILTER CLICKED
    @IBAction func filterClicked(_ sender: Any) {
        filterBlurView.isHidden = false
        filterBlurView.backgroundColor = AppColors.APP_BLURVIEW_COLOR
    }
    //MARK:- CONTRIBUTION CLICKED
    @IBAction func contributionClicked(_ sender: Any) {
        commonArray.removeAll()
        commonArray = contributionArray
        self.SWTransacTableVw.delegate = self
        self.SWTransacTableVw.dataSource = self
        self.SWTransacTableVw.reloadData()
        filterBlurView.isHidden = true
    }
    //MARK:- WITHDRAW CLICKED
    @IBAction func withDrawClicked(_ sender: Any) {
        commonArray.removeAll()
        commonArray = withdrawArray
        self.SWTransacTableVw.delegate = self
        self.SWTransacTableVw.dataSource = self
        self.SWTransacTableVw.reloadData()
        filterBlurView.isHidden = true
    }
    //MARK:- BACK CLICKED
    @IBAction func backClicked(_ sender: Any) {
         _ = navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
