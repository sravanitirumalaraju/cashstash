//
//  WithdrawalViewController.swift
//  CashStash
//
//  Created by apple on 7/29/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class WithdrawalViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var amountCoinsTxtFld: FloatLabelTextField!
    @IBOutlet weak var requestButton: UIButton!
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        //withdrawal content
        amountCoinsTxtFld.delegate = self
        amountCoinsTxtFld.keyboardType = .decimalPad
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneButtonAction))
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        amountCoinsTxtFld.inputAccessoryView = doneToolbar
        
        requestButton.layer.cornerRadius = requestButton.frame.size.height/2
        requestButton.clipsToBounds = true
        requestButton.layer.shadowColor = UIColor(white: 0.0, alpha: 0.25).cgColor
        requestButton.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        requestButton.layer.shadowOpacity = 1.0
        requestButton.layer.shadowRadius = 3
        requestButton.layer.masksToBounds = false
        // Do any additional setup after loading the view.
    }
    //MARK:- DONE BUTTON CLICKED
    @objc func doneButtonAction() {
        amountCoinsTxtFld.resignFirstResponder()
    }
    
    //MARK:- REQUEST BUTTON CLICKED
    @IBAction func requestBtnClicked(_ sender: Any) {
        if amountCoinsTxtFld.text == ""{
            Helper.alertBox(Mymsg: "Enter coins", view: self)
        }else{
            //checking KYC is done or not
            let keyStatus = SharedClass.sharedInstance.retrieveFromUserDefaultsBool(key: "kycStatus")
            let bankStatus = SharedClass.sharedInstance.retrieveFromUserDefaultsBool(key: "bankStatus")
            if keyStatus == false || bankStatus == false{
                let alert = UIAlertController(title: "", message: "Please update you bank details & KYC to Add money", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { UIAlertAction in
                    let destVc = self.storyboard?.instantiateViewController(withIdentifier: "KYCViewController") as! KYCViewController
                    self.navigationController?.pushViewController(destVc, animated: true)
                }))
                alert.addAction(UIAlertAction(title: "CANCEL", style: .default, handler: { UIAlertAction in
                    _ = self.navigationController?.popViewController(animated: true)
                }))
                self.present(alert, animated: true, completion: nil)
            }else{
                //KYC is done then updating withdraw details
                if NetworkingManager.isConnectedToNetwork(){
                    let retrievedToken: String? = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "token")
                    let tokenString = "Bearer " + retrievedToken!
                    let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
                    let amount = UserDefaults.standard.value(forKey: "amount") as! NSNumber
                    let enteredString = amountCoinsTxtFld.text!
                    if let myInteger = Double(enteredString) {
                        let enteredNumber = NSNumber(value:myInteger)
                        if Double(truncating: enteredNumber) <= Double(truncating: amount){
                            print("allowed")
                            showActivityIndicator()
                            let parms = ["amountRequested":amountCoinsTxtFld.text!]
                            Alamofire.request(updateWithdrawDetailsURL, method: .put, parameters: parms, encoding: JSONEncoding.default, headers: Headers).responseJSON{ response in
                                hideActivityIndicator()
                                switch (response.result){
                                case .success:
                                    let json = JSON(response.result.value!)
                                    let alert = UIAlertController(title: "", message: json["message"].string!, preferredStyle: .alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { UIAlertAction in
                                        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "WalletViewController") as! WalletViewController
                                        self.navigationController?.pushViewController(destVc, animated: true)
                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                    self.amountCoinsTxtFld.text = ""
                                case .failure:
                                    print("error")
                                }
                            }
                        }else{
                            hideActivityIndicator()
                            Helper.alertBox(Mymsg: "Insufficient balance please buy more coins", view: self)
                        }
                    }else{
                        hideActivityIndicator()
                        Helper.alertBox(Mymsg: "Insufficient balance please buy more coins", view: self)
                    }
                }else{
                    NetworkingManager.netWorkFailed(view: self)
                }
            }
        }
    }
    //MARK:- TEXT FIELD DELEGATE
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    // validating to two decimal points
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let oldText = textField.text, let r = Range(range, in: oldText) else {
            return true
        }
        
        let newText = oldText.replacingCharacters(in: r, with: string)
        let isNumeric = newText.isEmpty || (Double(newText) != nil)
        let numberOfDots = newText.components(separatedBy: ".").count - 1
        
        let numberOfDecimalDigits: Int
        if let dotIndex = newText.index(of: ".") {
            numberOfDecimalDigits = newText.distance(from: dotIndex, to: newText.endIndex) - 1
        } else {
            numberOfDecimalDigits = 0
        }
        
        return isNumeric && numberOfDots <= 1 && numberOfDecimalDigits <= 2
    }
    //MARK:- BACK CLICKED
    @IBAction func backClicked(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
