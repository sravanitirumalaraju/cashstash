//
//  GroupDetailsViewController.swift
//  CashStash
//
//  Created by apple on 05/02/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Firebase

class GroupDetailsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet var groupDetailsTblVw: UITableView!
    @IBOutlet var groupNameView: UIView!
    @IBOutlet var groupImage: UIImageView!
    @IBOutlet var groupiconBtn: UIButton!
    @IBOutlet var saveBtn: UIButton!
    @IBOutlet var groupNameTxtFld: UITextField!
    @IBOutlet var lockSwitch: UISwitch!
    @IBOutlet var lockStatusLbl: UILabel!
    @IBOutlet var mainAmountLbl: UILabel!
    @IBOutlet var addUsersBtn: UIButton!
    @IBOutlet var deleteWallBtn: UIButton!
    @IBOutlet weak var editButton: UIButton!
    
    @IBOutlet weak var withDrawBtn: UIButton!
    
    var groupNameString = String()
    var groupImageString = String()
    var groupId = String()
    var userDetailsArray = Array<Any>()
    var imagePicker = UIImagePickerController()
    var isLocked = Bool()
    var userId = String()
    var isAdminSame = Bool()
    var blurView = UIView()
    var groupUsersArray = [String]()
    var groupUsersIds = [String]()
    var isDeletedClicked = Bool()
    var userDict = [String:Any]()
    let retrievedToken: String? = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "token")
    var SWaccountType = String()
    
    let limitTextField = UITextField()
    var grantUserId = String()
    let limitView = UIView()
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        userId = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "userId")!
        imagePicker.delegate = self
        groupiconBtn.isUserInteractionEnabled = false
        groupImage.layer.cornerRadius = groupImage.frame.size.height/2
        groupImage.clipsToBounds = true
        saveBtn.isHidden = true
        saveBtn.layer.cornerRadius = saveBtn.frame.size.height/2
        saveBtn.clipsToBounds = true
        groupNameTxtFld.isUserInteractionEnabled = false
        groupNameTxtFld.text = groupNameString
        let url = URL(string: groupImageString)
        if url != nil{
            let data = try? Data(contentsOf: url!)
            groupImage.image = UIImage(data: data!)
        }else{
            groupImage.image = #imageLiteral(resourceName: "manProfile")
        }
        groupNameView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.05).cgColor
        groupNameView.layer.shadowOffset = CGSize(width: 0, height: 2)
        groupNameView.layer.shadowOpacity = 1
        groupNameView.layer.shadowRadius = 10
        groupNameView.layer.masksToBounds = false
        groupDetailsTblVw.backgroundColor = AppColors.APP_BACKGROUND_COLOR
        lockSwitch.isHidden = true
        lockStatusLbl.isHidden = true
        
        blurView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        blurView.backgroundColor = AppColors.APP_BLURVIEW_COLOR
        self.view.addSubview(blurView)
        blurView.isHidden = true
        
        fetchGroupDetails()
    }
    //MARK:-  FETCH GROUP DETAILS
    func fetchGroupDetails(){
        let dict = JSON(userDict)
        self.mainAmountLbl.text = String("CSH \((dict["mainBalance"].number)!)")
        self.userDetailsArray = (dict["usersInSharedWallet"].array)!
        self.groupUsersArray.removeAll()
        self.groupUsersIds.removeAll()
        for (index,_) in self.userDetailsArray.enumerated(){
            let dictionary = JSON(self.userDetailsArray[index])
            if dictionary["userId"].string! == self.userId{
                if dictionary["role"].string! == "ADMIN"{
                    self.isAdminSame = true
                    self.lockSwitch.isHidden = false
                    self.lockStatusLbl.isHidden = false
                    self.addUsersBtn.isHidden = false
                    self.deleteWallBtn.isHidden = false
                    self.isLocked = (dict["locked"].bool)!
                    if self.isLocked == true{
                        self.lockSwitch.isOn = true
                        self.lockStatusLbl.text = "Locked"
                    }else{
                        self.lockSwitch.isOn = false
                        self.lockStatusLbl.text = "Unlocked"
                    }
                }else if dictionary["role"].string! == "SUBADMIN"{
                    self.deleteWallBtn.isHidden = true
                    self.addUsersBtn.isHidden = false
                }
            }
        self.groupUsersArray.append(dictionary["userContactNumber"].string!)
            self.groupUsersIds.append(dictionary["userId"].string!)
        }
        if dict["isDeleted"].bool == true{
            self.isDeletedClicked = true
            self.lockSwitch.isHidden = true
            self.editButton.isHidden = true
            self.addUsersBtn.isHidden = true
            self.lockStatusLbl.isHidden = true
            self.deleteWallBtn.isHidden = false
            self.deleteWallBtn.isUserInteractionEnabled = false
            self.deleteWallBtn.setImage(UIImage(named:""), for: .normal)
            
            let dateTimeStamp = NSDate(timeIntervalSince1970:Double(dict["deletedOn"].int64Value)/1000)
            let dateFormatter = DateFormatter.init()
            dateFormatter.locale = NSLocale.current
            dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
            let date = dateFormatter.string(from: dateTimeStamp as Date)
            
            self.deleteWallBtn.setTitle("This share wallet delete by \(date)", for: .normal)
            self.deleteWallBtn.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 12.0)
            self.deleteWallBtn.setTitleColor(AppColors.APP_COLOR_BLUE, for: .normal)
        }else{
            self.isDeletedClicked = false
        }
        self.SWaccountType = dict["sharedWalletAccountType"].string!
        if dict["sharedWalletAccountType"].string! == "ORGANIZATION" {
//        if dict["sharedWalletAccountType"].string! == "BUSINESS" || dict["sharedWalletAccountType"].string! == "ORGANIZATION" {
            lockSwitch.isHidden = true
            lockStatusLbl.isHidden = true
        }
        self.groupDetailsTblVw.reloadData()
    }
    
    //MARK:- LOCK CLICKED
    @IBAction func lockClicked(_ sender: Any) {
        if NetworkingManager.isConnectedToNetwork(){
            if self.lockSwitch.isOn == false{
                self.lockStatusLbl.text = "Unlocked"
            }else{
                self.lockStatusLbl.text = "Locked"
            }
            let tokenString = "Bearer " + retrievedToken!
            let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
            let lockParams = ["locked": self.lockSwitch.isOn]
            Alamofire.request("\(makeLockURL)\(groupId)", method: .put, parameters: lockParams, encoding: JSONEncoding.default, headers: Headers).responseJSON{ response in
                switch (response.result){
                case .success(_):
                    self.refreshingDetails()
                case .failure(_):break
                }
            }
        }else{
             NetworkingManager.netWorkFailed(view: self)
        }
    }
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    //MARK:- TABLEVIEW DELEGATES
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if userDetailsArray.count == 0{
            return 0
        }else{
            return userDetailsArray.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: recentTrnsactnTableViewCell = tableView.dequeueReusableCell(withIdentifier:"Cell", for: indexPath as IndexPath) as! recentTrnsactnTableViewCell
        if userDetailsArray.count != 0{
            let Dict = JSON(userDetailsArray[indexPath.row])
            cell.userNameLbl.text = Dict["userName"].string!
            if isDeletedClicked == false{ // wallet deletion is not done
                if isAdminSame == true{ // login user is ADMIN
//                    if self.isLocked == false{ //added feature
//                        cell.withDrawBtn.isHidden = true
//                    }else{
                    cell.withDrawBtn.isHidden = false
                    cell.withDrawSwitch.isHidden = false
//                    }
                    cell.withDrawSwitch.tag = indexPath.row
                    if Dict["grantPermission"].bool! == true{ // added feature
                        cell.withDrawBtn.setTitle("Can withdraw", for: .normal)
                        cell.withDrawBtn.setTitleColor(AppColors.APP_GREEN_COLOR, for: .normal)
                        cell.withDrawSwitch.isOn = true
                    }else{
                        cell.withDrawBtn.setTitle("Cannot withdraw", for: .normal)
                        cell.withDrawBtn.setTitleColor(AppColors.APP_RED_COLOR, for: .normal)
                        cell.withDrawSwitch.isOn = false
                    }
                    cell.withDrawSwitch.addTarget(self, action: #selector(canWithdrawClicked), for: .touchUpInside)

                    if Dict["role"].string! == "ADMIN"{ // user role is ADMIN
                        cell.userStatus.setTitle("ADMIN", for: .normal)
                        cell.withDrawBtn.isHidden = true
                        cell.withDrawSwitch.isHidden = true
                    }else if Dict["reqStatus"].string! == "ACCEPTED"{ // user request status is ACCEPTED
                        if SWaccountType != "ORGANIZATION"{ // share wallet is not an organization
                            if Dict["role"].string! == "SUBADMIN"{
                                cell.userStatus.setTitle("SUBADMIN", for: .normal)
                            }else{
                                cell.userStatus.setTitle("Make subadmin", for: .normal)
                            }
                        }else{ // share wallet is an organization
                            cell.userStatus.setTitle("", for: .normal)
                        }
                    }else{ // user request status is PENDING
                        cell.userStatus.setTitle("PENDING..", for: .normal)
                    }
                }else{ // login user is not ADMIN
                    cell.withDrawBtn.isHidden = true
                    cell.withDrawSwitch.isHidden = true
                    cell.withDrawBtn.setTitle("", for: .normal)
                    if Dict["role"].string! == "ADMIN"{ // user role is ADMIN
                        cell.userStatus.setTitle("ADMIN", for: .normal)
                    }else if Dict["reqStatus"].string! == "ACCEPTED"{ // user request status is ACCEPTED
                        if SWaccountType != "ORGANIZATION"{ // share wallet is not an organization
                            if Dict["role"].string! == "SUBADMIN"{
                                cell.userStatus.setTitle("SUBADMIN", for: .normal)
                            }else{
                                cell.userStatus.setTitle("", for: .normal)
                            }
                        }else{ // share wallet is an organization
                            cell.userStatus.setTitle("", for: .normal)
                        }
                    }else{ // user request status is PENDING
                        cell.userStatus.setTitle("PENDING..", for: .normal)
                    }
                }
            }else{ // wallet deletion is done
                cell.userStatus.setTitle("", for: .normal)
                cell.withDrawBtn.isHidden = true
                cell.withDrawSwitch.isHidden = true
                cell.withDrawBtn.setTitle("", for: .normal)
            }
            cell.userAmountLbl.text = String("+CSH \(Dict["userSharedWalletAmount"].int!)")
            cell.spendAmountLbl.text = String("-CSH \(Dict["userSpentAmount"].int!)")
        }
        
        cell.userStatus.tag = indexPath.row
        cell.memberView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.05).cgColor
        cell.memberView.layer.shadowOffset = CGSize(width: 0, height: 2)
        cell.memberView.layer.shadowOpacity = 1
        cell.memberView.layer.shadowRadius = 10
        cell.memberView.layer.masksToBounds = false
        
//        cell.withDrawBtn.addTarget(self, action: #selector(canWithdrawClicked), for: .touchUpInside)
        
        return cell
    }
    @objc func canWithdrawClicked(_ sender: UISwitch){
        let Dict = JSON(userDetailsArray[sender.tag])
        print(Dict["userId"].string!)
        print(Dict["userName"].string!)
        grantUserId = (Dict["userId"].string!)
        if sender.isOn == true{
            self.blurView.isHidden = false
            self.limitView.isHidden = false
            limitView.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
            limitView.backgroundColor = UIColor.white
            limitView.center = self.view.center
            self.blurView.addSubview(limitView)

            let limitTitle = UILabel.init(frame: CGRect(x: 0, y: 20, width: limitView.frame.size.width, height: 30))
            limitTitle.text = "Set Limit"
            limitTitle.font = UIFont(name: "Nunito-Regular", size: 16.0)
            limitTitle.textAlignment = .center
            limitView.addSubview(limitTitle)

            limitTextField.frame = CGRect(x: 40, y: 80, width: 120, height: 40)
            limitTextField.placeholder = "Enter limit"
            limitTextField.textAlignment = .center
            limitTextField.autocorrectionType = .no
            limitTextField.font = UIFont(name: "Nunito-Regular", size: 16.0)
            limitTextField.delegate = self
            limitView.addSubview(limitTextField)

            let CSHLbl = UILabel.init(frame: CGRect(x: limitTextField.frame.origin.x + limitTextField.frame.size.width, y: 80, width: 25, height: 40))
            CSHLbl.text = "CSH"
            CSHLbl.textColor = AppColors.APP_COLOR_BLUE
            CSHLbl.font = UIFont(name: "Nunito-Regular", size: 12.0)
            limitView.addSubview(CSHLbl)

            let grantAccessBtn = UIButton.init(frame: CGRect(x: 25, y: limitView.frame.size.height - 60, width: 150, height: 40))
            grantAccessBtn.setTitle("Grant access", for: .normal)
            grantAccessBtn.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 16.0)
            grantAccessBtn.backgroundColor = AppColors.APP_COLOR_BLUE
            grantAccessBtn.addTarget(self, action: #selector(grantAccessClicked), for: .touchUpInside)
            limitView.addSubview(grantAccessBtn)

            let cancelBtn = UIButton.init(frame: CGRect(x: limitView.frame.size.width-35, y: 0, width: 30, height: 30))
            cancelBtn.setImage(UIImage(named:"cross_blue"), for: .normal)
            cancelBtn.addTarget(self, action: #selector(cancelClicked), for: .touchUpInside)
            limitView.addSubview(cancelBtn)
        }else{
            self.limitView.isHidden = true
            self.blurView.isHidden = true
            if NetworkingManager.isConnectedToNetwork(){
                showActivityIndicator()
                let tokenString = "Bearer " + retrievedToken!
                let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
                let params = ["sharedWalletId":self.groupId,"userId": grantUserId,"grantPermission":false,"withdrawLimt":"UNLIMITED"] as [String : Any]
                Alamofire.request("\(grantPermissionURL)\(groupId)", method: .post, parameters: params, encoding: JSONEncoding.default, headers: Headers).responseJSON{ response in
                    hideActivityIndicator()
                    print(response)
                    switch (response.result){
                    case .success(let value):
                        let json = JSON(value)
                        if response.response?.statusCode == 200{
                            if let sucessValue = json["message"].string{
                                Helper.alertBox(Mymsg: sucessValue, view: self)
                                self.refreshingDetails()
                            }
                        }
                    case .failure(_):
                        print(response.result.error!)
                        break
                    }
                }
            }else{
                NetworkingManager.netWorkFailed(view: self)
            }
        }
        
    }
    @objc func grantAccessClicked(){
        if limitTextField.text != ""{
            self.blurView.isHidden = true
            self.limitView.isHidden = true
            if NetworkingManager.isConnectedToNetwork(){
                showActivityIndicator()
                let tokenString = "Bearer " + retrievedToken!
                let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
                let params = ["sharedWalletId":self.groupId,"userId": grantUserId,"grantPermission":true,"withdrawLimt":limitTextField.text!] as [String : Any]
                Alamofire.request("\(grantPermissionURL)\(groupId)", method: .post, parameters: params, encoding: JSONEncoding.default, headers: Headers).responseJSON{ response in
                    hideActivityIndicator()
                    print(response)
                    switch (response.result){
                    case .success(let value):
                        let json = JSON(value)
                        if response.response?.statusCode == 200{
                            if let sucessValue = json["message"].string{
                                Helper.alertBox(Mymsg: sucessValue, view: self)
                                self.refreshingDetails()
                            }
                        }
                    case .failure(_):
                        print(response.result.error!)
                        break
                    }
                }
            }else{
                NetworkingManager.netWorkFailed(view: self)
            }
        }else{
            Helper.alertBox(Mymsg: "Please enter limit", view: self)
        }
    }
    @objc func cancelClicked(){
        self.blurView.isHidden = true
        self.limitView.isHidden = true
        self.view.endEditing(true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    //MARK:- MAKE ADMIN CLICKED
    @IBAction func makeAdminClicked(_ sender: UIButton) {
        if sender.title(for: .normal)! == "Make subadmin"{
            if NetworkingManager.isConnectedToNetwork(){
                showActivityIndicator()
                let tokenString = "Bearer " + retrievedToken!
                let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
                let Dict = JSON(userDetailsArray[sender.tag])
                let params = ["userId":Dict["userId"].string!,"role": "SUBADMIN"]
                Alamofire.request("\(makeAdminURL)\(groupId)", method: .post, parameters: params, encoding: JSONEncoding.default, headers: Headers).responseJSON{ response in
                    hideActivityIndicator()
                    switch (response.result){
                    case .success(let value):
                        let json = JSON(value)
                        if response.response?.statusCode == 200{
                            if let sucessValue = json["message"].string{
                                self.refreshingDetails()
                                Helper.alertBox(Mymsg: sucessValue, view: self)
                            }
                        }
                    case .failure(_):
                        print(response.result.error!)
                        break
                    }
                }
            }else{
                NetworkingManager.netWorkFailed(view: self)
            }
        }
    }
    //MARK:- REFRESHING DETAILS
    func refreshingDetails(){ //reloading all details if any update happened
        if NetworkingManager.isConnectedToNetwork(){
            showActivityIndicator()
            let tokenString = "Bearer " + retrievedToken!
            let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
            Alamofire.request("\(getShareWalletDetailsURL)\(groupId)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: Headers).responseJSON{ response in
                hideActivityIndicator()
                print(response)
                switch (response.result){
                case .success(let value):
                    let json = JSON(value)
                    if response.response?.statusCode == 200{
                        if let sucessValue = json["success"].string{
                            Helper.alertBox(Mymsg: sucessValue, view: self)
                        }else{
                            let dict = json.dictionary
                            self.userDetailsArray = (dict!["usersInSharedWallet"]?.array)!
                            self.groupUsersArray.removeAll()
                            self.groupUsersIds.removeAll()
                            for (index,_) in self.userDetailsArray.enumerated(){
                                let dictionary = JSON(self.userDetailsArray[index])
                                if dictionary["userId"].string! == self.userId{
                                    if dictionary["role"].string! == "ADMIN"{
                                        self.isAdminSame = true
                                        self.lockSwitch.isHidden = false
                                        self.lockStatusLbl.isHidden = false
                                        self.addUsersBtn.isHidden = false
                                        self.deleteWallBtn.isHidden = false
                                        self.isLocked = (dict!["locked"]?.bool)!
                                        if self.isLocked == true{
                                            self.lockSwitch.isOn = true
                                            self.lockStatusLbl.text = "Locked"
                                        }else{
                                            self.lockSwitch.isOn = false
                                            self.lockStatusLbl.text = "Unlocked"
                                        }
                                    }else if dictionary["role"].string! == "SUBADMIN"{
                                        self.deleteWallBtn.isHidden = true
                                        self.addUsersBtn.isHidden = false
                                    }
                                }
                                self.groupUsersArray.append(dictionary["userContactNumber"].string!)
                                self.groupUsersIds.append(dictionary["userId"].string!)
                            }
                            if dict!["isDeleted"]?.bool == true{ // share wallet is deleted
                                self.isDeletedClicked = true
                                self.lockSwitch.isHidden = true
                                self.editButton.isHidden = true
                                self.addUsersBtn.isHidden = true
                                self.lockStatusLbl.isHidden = true
                                self.deleteWallBtn.isHidden = false
                                self.deleteWallBtn.isUserInteractionEnabled = false
                                self.deleteWallBtn.setImage(UIImage(named:""), for: .normal)
                                
                                let dateTimeStamp = NSDate(timeIntervalSince1970:Double((dict!["deletedOn"]?.int64Value)!)/1000)
                                let dateFormatter = DateFormatter.init()
                                dateFormatter.locale = NSLocale.current
                                dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
                                let date = dateFormatter.string(from: dateTimeStamp as Date)
                                
                                self.deleteWallBtn.setTitle("This share wallet delete by \(date)", for: .normal)
                                self.deleteWallBtn.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 12.0)
                                self.deleteWallBtn.setTitleColor(AppColors.APP_COLOR_BLUE, for: .normal)
                            }else{
                                self.isDeletedClicked = false
                            }
                            //checking share wallet account type
                            self.SWaccountType = (dict!["sharedWalletAccountType"]?.string!)!
                            if dict!["sharedWalletAccountType"]?.string! == "ORGANIZATION" {
//                            if dict!["sharedWalletAccountType"]?.string! == "BUSINESS" || dict!["sharedWalletAccountType"]?.string! == "ORGANIZATION" {
                                self.lockSwitch.isHidden = true
                                self.lockStatusLbl.isHidden = true
                            }
                            self.groupDetailsTblVw.reloadData()
                        }
                    }
                case .failure(_):
                    print(response.result.error!)
                    break
                }
            }
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }
    }
    //MARK:- ADD USERS CLICKED
    @IBAction func addUsersClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "CreateGroupViewController") as! CreateGroupViewController
        destVc.isUserAdd = true
        destVc.groupName = groupNameTxtFld.text!
        destVc.existUserArray = groupUsersArray
        destVc.allUserIds = groupUsersIds
        destVc.walletId = groupId
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- EDIT CLICKED
    @IBAction func editClicked(_ sender: Any) {
        saveBtn.isHidden = false
        groupiconBtn.isUserInteractionEnabled = true
        groupNameTxtFld.isUserInteractionEnabled = true
    }
    //MARK:- SAVE CLICKED
    @IBAction func saveClicked(_ sender: Any) {
        saveBtn.isHidden = true
        if NetworkingManager.isConnectedToNetwork(){
            if groupNameTxtFld.text != groupNameString{
                showActivityIndicator()
                let tokenString = "Bearer " + retrievedToken!
                let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
                let params = ["walletName":groupNameTxtFld.text!]
                Alamofire.request("\(updateShareWalletURL)\(groupId)", method: .put, parameters: params, encoding: JSONEncoding.default, headers: Headers).responseJSON{ response in
                    hideActivityIndicator()
                    switch (response.result){
                    case .success(let value):
                        let json = JSON(value)
                        if response.response?.statusCode == 200{
                            if let sucessValue = json["message"].string{
                                Helper.alertBox(Mymsg: sucessValue, view: self)
                            }
                        }
                    case .failure(_):
                        print(response.result.error!)
                        break
                    }
                }
            }else{
                Helper.alertBox(Mymsg: "No data is updated", view: self)
            }
        }else{
             NetworkingManager.netWorkFailed(view: self)
        }
        
    }
    //MARK:-  TEXTFIELD DELEGATE
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    //MARK:- GROUP IMAGE BUTTON CLICKED
    @IBAction func groupImageBtnClicked(_ sender: Any) {
        let actionSheetController: UIAlertController = UIAlertController (title: "Choose an Option", message: nil, preferredStyle: .actionSheet)
        actionSheetController.addAction( UIAlertAction (title: "Cancel", style: .cancel, handler: nil))
        actionSheetController.addAction( UIAlertAction (title: "Photo Library", style: .default, handler:{ (alert: UIAlertAction!) in self.chooseFromPhotoLibrary()}
        ))
        actionSheetController.addAction( UIAlertAction (title: "Take Picture", style: .default, handler: {(alert: UIAlertAction!) in self.chooseFromCamera()}
        ))
        self.view.endEditing(true)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    //MARK:-  CHOOSE FROM CAMERA
    func chooseFromCamera() {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.cameraCaptureMode = .photo
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    //MARK:- CHOOSE FROM PHOTO LIBRARY
    func chooseFromPhotoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    //MARK:- IMAGE PICKER DELEGATES
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var selectedImage: UIImage?
        groupImage.contentMode = .scaleAspectFit
        selectedImage = info[UIImagePickerControllerEditedImage] as? UIImage
        if let selectedImages = selectedImage {
            showActivityIndicator()
            if NetworkingManager.isConnectedToNetwork(){
                let tokenString = "Bearer " + retrievedToken!
                let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
                let URL = try! URLRequest(url: "\(uploadSWImageURL)\(groupId)", method: .post, headers: Headers)
                Alamofire.upload(multipartFormData: { multipartFormData in
                    multipartFormData.append(UIImageJPEGRepresentation(selectedImages,0.3)!, withName: "file", fileName: "file.png", mimeType: "image/jpg/jpeg/png")
                }, with: URL, encodingCompletion: {
                    encodingResult in
                    switch encodingResult {
                        case .success(let upload, _, _):
                            upload.responseJSON { response in
                                self.groupImage.image = selectedImage
                                Helper.alertBox(Mymsg: "Shared wallet image uploaded", view: self)
                                debugPrint(response)
                            }
                        case .failure(let encodingError):
                            hideActivityIndicator()
                            print(encodingError)
                        }
                    hideActivityIndicator()
                })
            }else{
                hideActivityIndicator()
                NetworkingManager.netWorkFailed(view: self)
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:-  DELETE WALLET CLICKED
    @IBAction func deleteWalletClicked(_ sender: Any) {
        let alert = UIAlertController(title: "", message: "Are you sure to Delete", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "DELETE", style: .default, handler: { UIAlertAction in
            if NetworkingManager.isConnectedToNetwork(){
                showActivityIndicator()
                let tokenString = "Bearer " + self.retrievedToken!
                let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
                let userId = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "userId")!
                let params = ["sharedWalletId":self.groupId,"adminId":userId,"isDeleted":true] as [String : Any]
                Alamofire.request(deleteWalletURL, method: HTTPMethod.delete, parameters: params, encoding: JSONEncoding.default, headers: Headers).responseJSON{ response in
                    hideActivityIndicator()
                    switch (response.result){
                    case .success(let value):
                        let json = JSON(value)
                        if response.response?.statusCode == 200{
                            if let sucessValue = json["message"].string{
                                self.shareAlert(myMsg: sucessValue)
                            }
                            let deleteWalletRef = rootRef.child("groupUsers").child(self.groupId)
                            deleteWalletRef.removeValue()
                        }
                    case .failure(_):
                        print(response.result.error!)
                        break
                    }
                }
                
            }else{
                 NetworkingManager.netWorkFailed(view: self)
            }
        }))
        alert.addAction(UIAlertAction(title: "CANCEL", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    //MARK:- SHARE ALERT
    func shareAlert (myMsg: String) {
        let alert = UIAlertController(title: "", message: myMsg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { UIAlertAction in
            let destVc = self.storyboard?.instantiateViewController(withIdentifier: "ShareWalletViewController") as! ShareWalletViewController
            self.navigationController?.pushViewController(destVc, animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    //MARK:- RECENT TRANSAC CLICKED
    @IBAction func recentTransacClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "SwTransacViewController") as! SwTransacViewController
        destVc.shareWalletId = groupId
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- HOME CLICKED
    @IBAction func homeClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:-WALLET CLICKED
    @IBAction func walletClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "TransactionsViewController") as! TransactionsViewController
        destVc.isRecentClicked = false
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:-CONTACT CLICKED
    @IBAction func contactClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "FavouriteListViewController") as! FavouriteListViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:-CHAT CLICKED
    @IBAction func chatClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- PROFILE CLICKED
    @IBAction func profileClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- BACK CLICKED
    @IBAction func backClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    //MARK:- NOTIFICATION CLICKED
    @IBAction func notificationClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:-PREPARE FOR SEGUE
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if NetworkingManager.isConnectedToNetwork(){
            let destVC: contributionChartViewController = segue.destination as! contributionChartViewController
            destVC.shareWalletId = self.groupId
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }
    }
}
