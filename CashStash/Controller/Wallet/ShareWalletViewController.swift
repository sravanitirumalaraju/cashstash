//
//  ShareWalletViewController.swift
//  CashStash
//
//  Created by apple on 02/02/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ShareWalletViewController: UIViewController,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate{
 
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var subHeaderView: UIView!
    @IBOutlet weak var tabBar: UIView!
    @IBOutlet var walletBtn: UIButton!
    @IBOutlet var requestBtn: UIButton!
    @IBOutlet var walletView: UIView!
    @IBOutlet var requestVw: UIView!
    
    var shareWalletScrollVw = UIScrollView()
    var walletTableView = UITableView()
    var requestTableView = UITableView()
    var blurView = UIView()
    var walletDetailsArray = Array<Any>()
    var requestDetailsArray = Array<Any>()
    var isWalletCreated = Bool()
    let retrievedToken: String? = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "token")
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        //scrolview content
        blurView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        blurView.backgroundColor = AppColors.APP_BLURVIEW_COLOR
        view.addSubview(blurView)
        blurView.isHidden = true
        shareWalletScrollVw.backgroundColor = AppColors.APP_BACKGROUND_COLOR
        shareWalletScrollVw.isPagingEnabled = true
        if #available(iOS 11.0, *) {
            //other devices
            if (UIApplication.shared.keyWindow?.safeAreaInsets.top)! == 0{
                let scrollviewHeight = (subHeaderView.frame.origin.y + subHeaderView.frame.size.height + tabBar.frame.size.height)
                shareWalletScrollVw.frame = CGRect(x: 0, y: subHeaderView.frame.origin.y + subHeaderView.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height - scrollviewHeight)
            }else{//iphone x
                let scrollviewHeight = headerView.frame.size.height + subHeaderView.frame.size.height + tabBar.frame.size.height + (UIApplication.shared.keyWindow?.safeAreaInsets.bottom)! + (UIApplication.shared.keyWindow?.safeAreaInsets.top)!
               shareWalletScrollVw.frame = CGRect(x: 0, y: headerView.frame.size.height + subHeaderView.frame.size.height + (UIApplication.shared.keyWindow?.safeAreaInsets.top)!, width: self.view.frame.size.width, height: self.view.frame.size.height - scrollviewHeight)
            }
        }else{
            shareWalletScrollVw.frame = CGRect(x: 0, y: subHeaderView.frame.origin.y + subHeaderView.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height - (headerView.frame.size.height + subHeaderView.frame.size.height + tabBar.frame.size.height))
        }
        shareWalletScrollVw.delegate = self
        self.view.addSubview(shareWalletScrollVw)
        // MARK:- LOAD SCROLL VIEW
        shareWalletScrollVw.showsHorizontalScrollIndicator = false
        shareWalletScrollVw.showsVerticalScrollIndicator = false
        
        walletTableView.frame = CGRect(x: 0, y: 0, width: CGFloat(self.view.frame.size.width), height: CGFloat(self.shareWalletScrollVw.frame.size.height))
        walletTableView.separatorStyle = .none
        walletTableView.register(TableViewCell.self, forCellReuseIdentifier: "WalletTableViewCell")
        walletTableView.tag = 0
        walletTableView.bounces = false
        walletTableView.backgroundColor =  AppColors.APP_BACKGROUND_COLOR
        shareWalletScrollVw.addSubview(walletTableView)
    
        let addmemberBtn = UIButton.init(frame: CGRect(x: walletTableView.frame.size.width - 80, y: self.walletTableView.frame.size.height-70, width: 70, height: 70))
        addmemberBtn.addTarget(self, action: #selector(addGroupMember), for: .touchUpInside)
        addmemberBtn.setImage(UIImage(named:"add_member"), for: .normal)
        shareWalletScrollVw.addSubview(addmemberBtn)
    
        requestTableView.frame = CGRect(x: CGFloat(self.view.frame.size.width), y: 0, width: CGFloat(self.view.frame.size.width), height: CGFloat(self.shareWalletScrollVw.frame.size.height))
        requestTableView.separatorStyle = .none
        requestTableView.register(TableViewCell.self, forCellReuseIdentifier: "RequestTableViewCell")
        requestTableView.tag = 1
        requestTableView.bounces = false
        requestTableView.backgroundColor =  AppColors.APP_BACKGROUND_COLOR
        shareWalletScrollVw.addSubview(requestTableView)
    
        self.shareWalletScrollVw.contentSize = CGSize(width: CGFloat(self.view.frame.size.width) * 2, height: (self.shareWalletScrollVw.frame.size.height))
        
    }
    //MARK:- ADD GROUP MEMBER CLICKED
    @objc func addGroupMember(){
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "CreateGroupViewController") as! CreateGroupViewController
        destVc.isUserAdd = false
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- CANCEL CLICKED
    @objc func cancelClicked(){
        blurView.isHidden = true
    }
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        getWalletDetails()
        requestClicked()
    }
    //MARK:- GET WALLET DETAILS
    func getWalletDetails(){
        if NetworkingManager.isConnectedToNetwork(){
            showActivityIndicator()
            let tokenString = "Bearer " + retrievedToken!
            let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
            Alamofire.request(getShareWalletURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: Headers).responseJSON { response in
                hideActivityIndicator()
                switch (response.result){
                case .success(let value):
                    let json = JSON(value)
                    if response.response?.statusCode == 200 {
                        if let sucessValue = json["success"].string{
                            Helper.alertBox(Mymsg: sucessValue, view: self)
                        }else{
                            self.walletDetailsArray = json.arrayValue as Array<Any>
                            self.walletTableView.delegate = self
                            self.walletTableView.dataSource = self
                            self.walletTableView.reloadData()
                        }
                    }
                case .failure(_):
                    print(response.result.error!)
                    break
                }
            }
            
        }else{
             NetworkingManager.netWorkFailed(view: self)
        }
    }
    
    // MARK:- TABLE VIEW DELEGATE && DATA SOURCE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 0 {
            if walletDetailsArray.count == 0{
                return 1
            }else{
                return walletDetailsArray.count
            }
            
        }else{
            if requestDetailsArray.count == 0{
                return 1
            }else{
                return requestDetailsArray.count
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 0{
            let WalletTableViewCell = tableView.dequeueReusableCell(withIdentifier: "WalletTableViewCell", for: indexPath) as! TableViewCell
            WalletTableViewCell.selectionStyle = .none
            for cellView in WalletTableViewCell.contentView.subviews{
                cellView.removeFromSuperview()
            }
            WalletTableViewCell.backgroundColor =  AppColors.APP_BACKGROUND_COLOR
            
            if walletDetailsArray.count == 0{
                let nooResults_label = UILabel.init(frame:CGRect(x: 0, y: WalletTableViewCell.frame.size.height/3, width: self.view.frame.size.width, height: 50))
                nooResults_label.textColor = UIColor.lightGray
                nooResults_label.text = "No Groups Found"
                nooResults_label.textAlignment = .center
                nooResults_label.backgroundColor =  AppColors.APP_BACKGROUND_COLOR
                nooResults_label.numberOfLines = 0
                WalletTableViewCell.contentView.addSubview(nooResults_label)
            }else{
                let groupView = UIView.init(frame: CGRect(x: 20, y: 10, width: WalletTableViewCell.contentView.frame.size.width - 40, height: WalletTableViewCell.contentView.frame.size.height - 20))
                groupView.backgroundColor = UIColor.white
                
                groupView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.05).cgColor
                groupView.layer.shadowOffset = CGSize(width: 0, height: 2)
                groupView.layer.shadowOpacity = 1
                groupView.layer.shadowRadius = 10.0
                groupView.layer.masksToBounds = false
                
                WalletTableViewCell.contentView.addSubview(groupView)
                
                let indivDict = JSON(walletDetailsArray[indexPath.row])
            
                let groupNameLbl = UILabel.init(frame: CGRect(x: 20, y: 20, width: groupView.frame.size.width - 80, height: 20))
                groupNameLbl.text = indivDict["walletName"].string!
                groupNameLbl.font = UIFont(name: "Barlow-Medium", size: 16)
                groupNameLbl.textColor = UIColor(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0)
                groupView.addSubview(groupNameLbl)
                
                let groupNoLbl = UILabel.init(frame: CGRect(x: 20, y: groupNameLbl.frame.origin.y + groupNameLbl.frame.size.height + 5, width: groupView.frame.size.width - 80, height: 20))
                groupNoLbl.font = UIFont(name: "Nunito-Regular", size: 12)
                groupNoLbl.textColor = UIColor(red: 157.0/255.0, green: 145.0/255.0, blue: 171.0/255.0, alpha: 1.0)
                groupNoLbl.text = "\(String(describing: indivDict["totalUsers"].int!)) People"
                groupView.addSubview(groupNoLbl)
                
                let amountLbl = UILabel.init(frame: CGRect(x: groupView.frame.size.width - 90, y: 20, width: 80, height: 40))
                amountLbl.text = "CSH \(String(describing: indivDict["mainBalance"].number!))"
                amountLbl.textColor = UIColor(red: 23.0/255.0, green: 32.0/255.0, blue: 63.0/255.0, alpha: 1.0)
                amountLbl.font = UIFont(name: "Barlow-Medium", size: 20)
                amountLbl.textAlignment = .right
                groupView.addSubview(amountLbl)
            }
            
            return WalletTableViewCell
        }else{
            let RequestTableViewCell = tableView.dequeueReusableCell(withIdentifier: "RequestTableViewCell", for: indexPath) as! TableViewCell
            RequestTableViewCell.selectionStyle = .none
            for cellView in RequestTableViewCell.contentView.subviews{
                cellView.removeFromSuperview()
            }
            RequestTableViewCell.backgroundColor =  AppColors.APP_BACKGROUND_COLOR
            if requestDetailsArray.count == 0{
                let nooResults_label = UILabel.init(frame:CGRect(x: 0, y: RequestTableViewCell.frame.size.height/3, width: self.view.frame.size.width, height: 50))
                nooResults_label.textColor = UIColor.lightGray
                nooResults_label.text = "No Requests Found"
                nooResults_label.textAlignment = .center
                nooResults_label.backgroundColor =  AppColors.APP_BACKGROUND_COLOR
                nooResults_label.numberOfLines = 0
                RequestTableViewCell.contentView.addSubview(nooResults_label)
            }else{
                let groupView = UIView.init(frame: CGRect(x: 20, y: 10, width: RequestTableViewCell.contentView.frame.size.width - 40, height: RequestTableViewCell.contentView.frame.size.height - 20))
                groupView.backgroundColor = UIColor.white
    
                groupView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.05).cgColor
                groupView.layer.shadowOffset = CGSize(width: 0, height: 2)
                groupView.layer.shadowOpacity = 1
                groupView.layer.shadowRadius = 15.0
                groupView.layer.masksToBounds = false
    
                RequestTableViewCell.contentView.addSubview(groupView)
    
                let reqDict = JSON(requestDetailsArray[indexPath.row])
                
                let groupNameLbl = UILabel.init(frame: CGRect(x: 20, y: 20, width: groupView.frame.size.width - 120, height: 20))
                groupNameLbl.text = reqDict["sharedWalletName"].string!
                groupNameLbl.font = UIFont(name: "Barlow-Medium", size: 16)
                groupNameLbl.textColor = UIColor(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0)
                groupView.addSubview(groupNameLbl)
    
                let groupNoLbl = UILabel.init(frame: CGRect(x: 20, y: groupNameLbl.frame.origin.y + groupNameLbl.frame.size.height + 5, width: groupView.frame.size.width - 120, height: 20))
                groupNoLbl.font = UIFont(name: "Nunito-Regular", size: 12)
                groupNoLbl.textColor = UIColor(red: 157.0/255.0, green: 145.0/255.0, blue: 171.0/255.0, alpha: 1.0)
                groupNoLbl.text = "Invited by \(String(describing: reqDict["createdBy"].string!))"
                groupView.addSubview(groupNoLbl)
    
                let cancelReqBtn = UIButton.init(frame: CGRect(x: groupView.frame.size.width - 45, y: 25, width: 30, height: 30))
                cancelReqBtn.setTitle("", for: .normal)
                cancelReqBtn.setImage(#imageLiteral(resourceName: "cancel"), for: .normal)
                cancelReqBtn.tag = indexPath.row
                cancelReqBtn.addTarget(self, action: #selector(cancelReqClicked), for: .touchUpInside)
                groupView.addSubview(cancelReqBtn)
                
                let acceptReqBtn = UIButton.init(frame: CGRect(x: groupView.frame.size.width - 100, y: 25, width: 30, height: 30))
                acceptReqBtn.setTitle("", for: .normal)
                acceptReqBtn.setImage(#imageLiteral(resourceName: "favourite"), for: .normal)
                acceptReqBtn.tag = indexPath.row
                acceptReqBtn.addTarget(self, action: #selector(acceptReqClicked), for: .touchUpInside)
                groupView.addSubview(acceptReqBtn)
            }
            return RequestTableViewCell
        }
    }
    //MARK:- CANCEL REQUEST CLICKED
    @objc func cancelReqClicked(_ sender:UIButton){
        if NetworkingManager.isConnectedToNetwork(){
            showActivityIndicator()
            let tokenString = "Bearer " + retrievedToken!
            let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
            let dict = JSON(requestDetailsArray[sender.tag])
            let rejectParams = ["sharedWalletId":dict["sharedWalletId"].string!,"sharedWalletUserId":dict["sharedWalletUserId"].string!,"reqId":dict["reqId"].string!]
            Alamofire.request(rejectUserRequestURL, method: .post, parameters: rejectParams, encoding: JSONEncoding.default, headers: Headers).responseJSON{ response in
                hideActivityIndicator()
                switch (response.result){
                case .success(let value):
                    let _ = JSON(value)
                    if response.response?.statusCode == 200{
                        self.requestClicked()
                        Helper.alertBox(Mymsg: "Request is rejected", view: self)
                    }
                case .failure(_):
                    print(response.result.error!)
                    break
                }
            }
            
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }
    }
    //MARK:- ACCEPT REQUEST CLICKED
    @objc func acceptReqClicked(_ sender:UIButton){
        if NetworkingManager.isConnectedToNetwork(){
            showActivityIndicator()
            let tokenString = "Bearer " + retrievedToken!
            let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
            let dict = JSON(requestDetailsArray[sender.tag])
            let acceptParams = ["sharedWalletId":dict["sharedWalletId"].string!,"sharedWalletUserId":dict["sharedWalletUserId"].string!,"reqId":dict["reqId"].string!]
            Alamofire.request(acceptUserRequestURL, method: .post, parameters: acceptParams, encoding: JSONEncoding.default, headers: Headers).responseString{ response in
                hideActivityIndicator()
                switch (response.result){
                case .success(let value):
                    let _ = JSON(value)
                    if response.response?.statusCode == 200 {
                        self.getWalletDetails()
                        let frame :CGRect = CGRect(x: CGFloat(self.view.frame.width) * 0, y: 124, width: CGFloat(self.view.frame.width), height: self.shareWalletScrollVw.frame.size.height)
                        self.shareWalletScrollVw.scrollRectToVisible(frame, animated: true)
                        self.walletBtn.setTitleColor(UIColor.init(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0), for: .normal)
                        self.walletView.backgroundColor = UIColor.init(red: 36.0/255.0, green: 137.0/255.0, blue: 247.0/255.0, alpha: 1.0)
                        self.requestBtn.setTitleColor(UIColor.init(red: 157.0/255.0, green: 145.0/255.0, blue: 171.0/255.0, alpha: 1.0), for: .normal)
                        self.requestVw.backgroundColor = UIColor.clear
                        self.requestClicked()
                        Helper.alertBox(Mymsg: "Request accepted successfully", view: self)
                    }
                case .failure(_):
                    print(response.result.error!)
                    break
                }
            }
        }else{
             NetworkingManager.netWorkFailed(view: self)
        }
    }
    //MARK:- TABLEVIEW DELEAGTES
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 0{
            if walletDetailsArray.count != 0{
                let destVc = self.storyboard?.instantiateViewController(withIdentifier: "GroupChatViewController") as! GroupChatViewController
                let dict = JSON(walletDetailsArray[indexPath.row])
                destVc.GroupNameString = dict["walletName"].string!
                destVc.GroupId = dict["sharedWalletId"].string!
                destVc.GroupImageURL = dict["walletImage"].string!
                destVc.isDelete = dict["isDeleted"].bool!
                self.navigationController?.pushViewController(destVc, animated: true)
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 0{
            if walletDetailsArray.count == 0{
                return self.walletTableView.frame.size.height
            }else{
                return 100
            }
        }else{
            if requestDetailsArray.count == 0{
                return self.requestTableView.frame.size.height
            }else{
                return 100
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK:- HOME CLICKED
    @IBAction func homeClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- WALLET CLICKED
    @IBAction func walletClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "TransactionsViewController") as! TransactionsViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:-CONTACT CLICKED
    @IBAction func contactClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "FavouriteListViewController") as! FavouriteListViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:-CHAT CLICKED
    @IBAction func chatClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- PROFILE CLICKED
    @IBAction func profileClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    //MARK:-WALLET BUTTON CLICKED
    @IBAction func walletBtnClicked(_ sender: Any) {
        requestBtn.setTitleColor(UIColor.init(red: 157.0/255.0, green: 145.0/255.0, blue: 171.0/255.0, alpha: 1.0), for: .normal)
        requestVw.backgroundColor = UIColor.clear
        
        walletBtn.setTitleColor(UIColor.init(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0), for: .normal)
        walletView.backgroundColor = UIColor.init(red: 36.0/255.0, green: 137.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        
        let frame :CGRect = CGRect(x: CGFloat(self.view.frame.width) * 0, y: 0, width: CGFloat(self.view.frame.width), height: shareWalletScrollVw.frame.size.height)
        self.shareWalletScrollVw.scrollRectToVisible(frame, animated: true)
    }
    //MARK:- REQUEST BUTTON CLICKED
    @IBAction func requestBtnClicked(_ sender: Any) {
        requestClicked()
        walletBtn.setTitleColor(UIColor.init(red: 157.0/255.0, green: 145.0/255.0, blue: 171.0/255.0, alpha: 1.0), for: .normal)
        walletView.backgroundColor = UIColor.clear
        
        requestBtn.setTitleColor(UIColor.init(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0), for: .normal)
        requestVw.backgroundColor = UIColor.init(red: 36.0/255.0, green: 137.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        
        let frame :CGRect = CGRect(x: CGFloat(self.view.frame.width) * 1, y: 0, width: CGFloat(self.view.frame.width), height: shareWalletScrollVw.frame.size.height)
        self.shareWalletScrollVw.scrollRectToVisible(frame, animated: true)
    }
    //MARK:- SCROLLVIEW DELEGATS
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if shareWalletScrollVw.contentOffset.x == 0{
            requestBtn.setTitleColor(UIColor.init(red: 157.0/255.0, green: 145.0/255.0, blue: 171.0/255.0, alpha: 1.0), for: .normal)
            requestVw.backgroundColor = UIColor.clear
            
            walletBtn.setTitleColor(UIColor.init(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0), for: .normal)
            walletView.backgroundColor = UIColor.init(red: 36.0/255.0, green: 137.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        }else{
            walletBtn.setTitleColor(UIColor.init(red: 157.0/255.0, green: 145.0/255.0, blue: 171.0/255.0, alpha: 1.0), for: .normal)
            walletView.backgroundColor = UIColor.clear
            
            requestBtn.setTitleColor(UIColor.init(red: 66.0/255.0, green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0), for: .normal)
            requestVw.backgroundColor = UIColor.init(red: 36.0/255.0, green: 137.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        }
    }
    //MARK:- REQUEST CLICKED
    func requestClicked(){
        if NetworkingManager.isConnectedToNetwork(){
            showActivityIndicator()
            let tokenString = "Bearer " + retrievedToken!
            let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
            Alamofire.request(getUsersShareWalletsRequestsURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: Headers).responseJSON{ response in
                hideActivityIndicator()
                switch (response.result){
                case .success(let value):
                    let json = JSON(value)
                    if response.response?.statusCode == 200{
                        if let sucessValue = json["mySharedWalletRequests"].string{
                            print(sucessValue)
                            self.requestDetailsArray = []
                            self.requestTableView.delegate = self
                            self.requestTableView.dataSource = self
                            self.requestTableView.reloadData()
                        }else{
                            self.requestDetailsArray = json.arrayValue as Array<Any>
                            self.requestTableView.delegate = self
                            self.requestTableView.dataSource = self
                            self.requestTableView.reloadData()
                        }
                    }
                case .failure(_):
                    print(response.result.error!)
                    break
                }
            }
            
        }else{
             NetworkingManager.netWorkFailed(view: self)
        }
    }
    //MARK:- BACK CLICKED
    @IBAction func backClicked(_ sender: Any) {
        if isWalletCreated == false{
            _ = navigationController?.popViewController(animated: true)
        }
    }
    //MARK:- NOTIFICATION CLICKED
    @IBAction func notificationClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
}

