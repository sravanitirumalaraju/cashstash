//
//  GroupChatViewController.swift
//  CashStash
//
//  Created by apple on 16/03/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import SwiftyJSON
import Alamofire
import Firebase
import FirebaseStorage

class GroupChatViewController: JSQMessagesViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var headerView: UIView!
    var messages = [JSQMessage]()
    lazy var outgoingBubble: JSQMessagesBubbleImage = {
        return JSQMessagesBubbleImageFactory()!.outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleBlue())
    }()
    
    lazy var incomingBubble: JSQMessagesBubbleImage = {
        return JSQMessagesBubbleImageFactory()!.incomingMessagesBubbleImage(with: UIColor.white)
    }()
    
    //    var userId = String()
    var chatArray = NSMutableArray()
    var noDataLbl = UILabel()
    var GroupNameString = String()
    var GroupId = String()
    var GroupImageURL = String()
    
    var id = String()
    var name = String()
    var text = String()
    var chatViewOpened = Bool()
    var responseValueGot = false
    var items = [Message]()
    let blurView = UIView()
    var isDelete = Bool()
    var userImageArray = Array<Any>()
    var userNameArray = [String]()
    var userDetails = Array<Any>()
    var amountString = NSNumber()
    var GroupDetailsDict = [String:Any]()
    let retrievedToken: String? = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "token")
    var isContributeClicked = Bool()
    var isWithdrawClicked = Bool()
    var wallAccType = String()
    var userRole = String()
    var isLocked = Bool()
    let group = DispatchGroup()
    
    var imagePicker = UIImagePickerController()
    let userNamesView = UIView()
    var namesTableVw = UITableView()
    var enteredText = String()
    let tappedImageView = UIImageView()
    let scrollImg: UIScrollView = UIScrollView()
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.isStatusBarHidden = true
        self.collectionView.register(UINib(nibName: "DeleteMsgCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DeleteMsgCollectionViewCell")
        senderId = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "userId")!
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 10.0/255.0, green: 60.0/255.0, blue: 88.0/255.0, alpha: 1.0)
//        self.navigationController?.navigationBar.barTintColor = UIColor(red: 20.0/255.0, green: 60.0/255.0, blue: 89.0/255.0, alpha: 1.0)
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "Chat"
        self.senderDisplayName = "sravani"
        inputToolbar.contentView.leftBarButtonItem = nil
//        collectionView.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        collectionView.collectionViewLayout.incomingAvatarViewSize = CGSize(width: 35, height: 35)
        collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        let imageWidth: CGFloat = 27
        let image = UIImage(named: "send")
        inputToolbar.contentView.rightBarButtonItemWidth = imageWidth
        inputToolbar.contentView.rightBarButtonItem.setImage(image, for: .normal)
        inputToolbar.contentView.textView.font = UIFont(name: "Nunito-Regular", size: 14.0)
        addViewOnTop()
        setupBackButton()
        self.collectionView?.collectionViewLayout.sectionInset = UIEdgeInsets(top: 100, left: 0, bottom: 35, right: 0)
        collectionView.backgroundColor =  AppColors.APP_BACKGROUND_COLOR
        chatViewOpened = true
        inputToolbar.contentView.textView.keyboardType = .default
        inputToolbar.contentView.textView.autocorrectionType = .yes
        self.collectionView?.collectionViewLayout.messageBubbleFont = UIFont(name: "Nunito-Regular", size: 16.0)!
        showChat()
        imagePicker.delegate = self
        
        self.collectionView.register(UINib(nibName: "GalleryOutgoingCollectionViewCell", bundle: nil
        ), forCellWithReuseIdentifier: "GalleryOutgoingCollectionViewCell")
        self.collectionView.register(UINib(nibName: "GalleryIncomingCollectionViewCell", bundle: nil
        ), forCellWithReuseIdentifier: "GalleryIncomingCollectionViewCell")
        self.collectionView.backgroundColor = UIColor.init(red: 249.0/255.0, green: 250.0/255.0, blue:251.0/255.0, alpha: 1.0)

    }
    
    //MARK:- TEXT FIELD DELEGATES
    override func textViewDidBeginEditing(_ textView: UITextView) {
        textView.keyboardType = .default
        textView.becomeFirstResponder()
        //        inputToolbar.contentView.textView.keyboardType = .default
        //        inputToolbar.contentView.textView.becomeFirstResponder()
        if (self.automaticallyScrollsToMostRecentMessage) {
             self.collectionView?.collectionViewLayout.sectionInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
            scrollToBottom(animated: true)
        }
    }
    override func textViewDidEndEditing(_ textView: UITextView) {
        if inputToolbar.contentView.textView.keyboardType != .default{
            inputToolbar.contentView.textView.keyboardType = .default
        }
        isContributeClicked = false
        isWithdrawClicked = false
        userNamesView.isHidden = true
        self.collectionView?.collectionViewLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
//        print(self.collectionView?.collectionViewLayout.sectionInset)
    }
    override func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        enteredText = textView.text!
        enteredText = (enteredText.appending(text))
        if text == "@"{
            userNamesView.isHidden = false
            userNamesView.frame = CGRect(x: 0, y: (inputToolbar.contentView.frame.size.height + inputToolbar.frame.origin.y) - 120 , width: 250, height: 120)
            userNamesView.layer.cornerRadius = 10
            userNamesView.clipsToBounds = true
            self.view.addSubview(userNamesView)
            userNamesView.isUserInteractionEnabled = true
            
            namesTableVw.frame = CGRect(x: 0, y: 0, width: self.userNamesView.frame.size.width, height: self.userNamesView.frame.size.height)
            namesTableVw.delegate = self
            namesTableVw.dataSource = self
            userNamesView.addSubview(namesTableVw)
        }
        return true
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userNameArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        cell.textLabel?.text = userNameArray[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedTxt = userNameArray[indexPath.row]
        inputToolbar.contentView.textView.text = (enteredText.appending(selectedTxt))
        userNamesView.isHidden = true
    }
    //checking for two decimal points
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if isContributeClicked == true || isWithdrawClicked == true {
            guard let oldText = textField.text, let r = Range(range, in: oldText) else {
                return true
            }
            let newText = oldText.replacingCharacters(in: r, with: string)
            let isNumeric = newText.isEmpty || (Double(newText) != nil)
            let numberOfDots = newText.components(separatedBy: ".").count - 1
            
            let numberOfDecimalDigits: Int
            if let dotIndex = newText.index(of: ".") {
                numberOfDecimalDigits = newText.distance(from: dotIndex, to: newText.endIndex) - 1
            } else {
                numberOfDecimalDigits = 0
            }
            return isNumeric && numberOfDots <= 1 && numberOfDecimalDigits <= 2
        }
        return true
    }
    @objc func keyboardAppear(){
        if inputToolbar.contentView.textView.keyboardType == .default{
            inputToolbar.contentView.textView.resignFirstResponder()
        }
        inputToolbar.contentView.textView.keyboardType = .decimalPad
        inputToolbar.contentView.textView.becomeFirstResponder()
        if (self.automaticallyScrollsToMostRecentMessage) {
            self.collectionView?.collectionViewLayout.sectionInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
            scrollToBottom(animated: true)
        }
    }
    //MARK:- ADD VIEW ON TOP
    func addViewOnTop() {
        let groupHeaderView = UIView()
        if #available(iOS 11.0, *) {
            if (UIApplication.shared.keyWindow?.safeAreaInsets.top)! == 0{
                groupHeaderView.frame = CGRect(x: 0, y: (self.navigationController?.navigationBar.frame.size.height)! + 20, width: self.navigationController!.navigationBar.frame.size.width, height: 80)
            }else{//iphone x
                groupHeaderView.frame = CGRect(x: 0, y: (self.navigationController?.navigationBar.frame.size.height)! + (UIApplication.shared.keyWindow?.safeAreaInsets.top)!, width: self.navigationController!.navigationBar.frame.size.width, height: 80)
            }
        } else {
            // Fallback on earlier versions
            groupHeaderView.frame = CGRect(x: 0, y: (self.navigationController?.navigationBar.frame.size.height)! + 20, width: self.navigationController!.navigationBar.frame.size.width, height: 80)
        }
        groupHeaderView.backgroundColor = UIColor.white
        groupHeaderView.layer.shadowColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.1).cgColor
        groupHeaderView.layer.shadowOffset = CGSize(width: 0, height: 2)
        groupHeaderView.layer.shadowOpacity = 1
        groupHeaderView.layer.shadowRadius = 2.0
        groupHeaderView.layer.masksToBounds = false
        let gestureRecognizer = UITapGestureRecognizer(target: self, action:  #selector(self.groupHeaderTapped (_:)))
        groupHeaderView.addGestureRecognizer(gestureRecognizer)
        view.addSubview(groupHeaderView)
        
        let groupIconImg = UIImageView.init(frame: CGRect(x: 20, y: 15, width: 50, height: 50))
        let url = URL(string: GroupImageURL)
        if url != nil{
            let data = try? Data(contentsOf: url!)
            groupIconImg.image = UIImage(data: data!)
        }else{
            groupIconImg.image =  #imageLiteral(resourceName: "manProfile")
        }
        groupIconImg.layer.cornerRadius = groupIconImg.frame.size.height/2
        groupIconImg.clipsToBounds = true
        groupHeaderView.addSubview(groupIconImg)
        
        let groupNameLbl = UILabel.init(frame: CGRect(x: groupIconImg.frame.origin.x + groupIconImg.frame.size.width + 20, y: 25, width: groupHeaderView.frame.size.width - 80, height: 30))
        groupNameLbl.text = GroupNameString
        groupHeaderView.addSubview(groupNameLbl)
        
        blurView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        blurView.backgroundColor = AppColors.APP_BLURVIEW_COLOR
        view.addSubview(blurView)
        blurView.isHidden = true
    }
    //MARK:- GET WALLET USER DETAILS
//    func getWalletUsersDetails(completion: @escaping (_ finish : Bool) -> Void){
    func getWalletUsersDetails(){
        group.enter()
        userNameArray.removeAll()
        userImageArray.removeAll()
        let retrievedToken: String? = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "token")
        let tokenString = "Bearer " + retrievedToken!
        let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
        Alamofire.request("\(getShareWalletDetailsURL)\(self.GroupId)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: Headers).responseJSON{ response in
            print(response)
            switch (response.result) {
            case .success(let value):
                let json = JSON(value)
                self.wallAccType = json["sharedWalletAccountType"].string!
                let moneyView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.inputToolbar.contentView.frame.size.width - 40, height: 40))
                self.inputToolbar.contentView.addSubview(moneyView)
                moneyView.backgroundColor = UIColor.white
                
                let attachmentBtn = UIButton.init(frame: CGRect(x: self.inputToolbar.contentView.frame.size.width - 40, y: 0, width: 40, height: 40))
                attachmentBtn.setImage(UIImage(named: "attach"), for: .normal)
                attachmentBtn.addTarget(self, action: #selector(self.attachmentClicked), for: .touchUpInside)
                self.inputToolbar.contentView.addSubview(attachmentBtn)

                if json["sharedWalletAccountType"].string! == "ORGANIZATION"{
                    let contributeBtn = UIButton.init(frame: CGRect(x: 0, y: 0, width: self.inputToolbar.contentView.frame.size.width-40, height: 40))
                    contributeBtn.setTitle("Contribute Money", for: .normal)
                    contributeBtn.setTitleColor(UIColor.green, for: .normal)
                    contributeBtn.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 16.0)
                    contributeBtn.addTarget(self, action: #selector(self.contributeMoneyClicked), for: .touchUpInside)
                    moneyView.addSubview(contributeBtn)
                }
                let dict = json.dictionary
                self.GroupDetailsDict = dict!
                self.amountString = (dict!["mainBalance"]?.number)!
                self.userDetails = (dict!["usersInSharedWallet"]?.array)!
                for (index,_) in self.userDetails.enumerated(){
                    let dictionary = JSON(self.userDetails[index])
                self.userImageArray.append(["userId":dictionary["userId"].string!,"userImage":dictionary["userImage"].string!,"userName":dictionary["userName"].string!])
                    self.userNameArray.append(dictionary["userName"].string!)
                    if dictionary["userId"].string! == SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "userId")!{
                        if json["sharedWalletAccountType"].string! == "BUSINESS" {
                            self.isLocked = json["locked"].bool!
                            if dictionary["role"].string! == "ADMIN" || dictionary["role"].string! == "SUBADMIN"{
                                self.userRole = dictionary["role"].string!
                                if dictionary["grantPermission"].bool! == true{
                                    let withdrawBtn = UIButton.init(frame: CGRect(x: 0, y: 0, width: moneyView.frame.size.width/2, height: 40))
                                    withdrawBtn.setTitle("Withdraw Money", for: .normal)
                                    withdrawBtn.setTitleColor(UIColor.red, for: .normal)
                                    withdrawBtn.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 16.0)
                                    withdrawBtn.addTarget(self, action: #selector(self.withdrawMoneyClicked), for: .touchUpInside)
                                    moneyView.addSubview(withdrawBtn)
                                    
                                    let contributeBtn = UIButton.init(frame: CGRect(x: withdrawBtn.frame.size.width, y: 0, width: moneyView.frame.size.width/2, height: 40))
                                    contributeBtn.setTitle("Contribute Money", for: .normal)
                                    contributeBtn.setTitleColor(UIColor.green, for: .normal)
                                    contributeBtn.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 16.0)
                                    contributeBtn.addTarget(self, action: #selector(self.contributeMoneyClicked), for: .touchUpInside)
                                    moneyView.addSubview(contributeBtn)
                                }else{
                                    let contributeBtn = UIButton.init(frame: CGRect(x: 0, y: 0, width: self.inputToolbar.contentView.frame.size.width - 40, height: 40))
                                    contributeBtn.setTitle("Contribute Money", for: .normal)
                                    contributeBtn.setTitleColor(UIColor.green, for: .normal)
                                    contributeBtn.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 16.0)
                                    contributeBtn.addTarget(self, action: #selector(self.contributeMoneyClicked), for: .touchUpInside)
                                    moneyView.addSubview(contributeBtn)
                                }
                            }else{
                                let contributeBtn = UIButton.init(frame: CGRect(x: 0, y: 0, width: self.inputToolbar.contentView.frame.size.width - 40, height: 40))
                                contributeBtn.setTitle("Contribute Money", for: .normal)
                                contributeBtn.setTitleColor(UIColor.green, for: .normal)
                                contributeBtn.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 16.0)
                                contributeBtn.addTarget(self, action: #selector(self.contributeMoneyClicked), for: .touchUpInside)
                                moneyView.addSubview(contributeBtn)
                            }
                        }else if json["sharedWalletAccountType"].string! == "PERSONAL"{
                            self.isLocked = json["locked"].bool!
                            if dictionary["grantPermission"].bool == true{
                                let withdrawBtn = UIButton.init(frame: CGRect(x: 0, y: 0, width: moneyView.frame.size.width/2, height: 40))
                                withdrawBtn.setTitle("Withdraw Money", for: .normal)
                                withdrawBtn.setTitleColor(UIColor.red, for: .normal)
                                withdrawBtn.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 16.0)
                                withdrawBtn.addTarget(self, action: #selector(self.withdrawMoneyClicked), for: .touchUpInside)
                                moneyView.addSubview(withdrawBtn)
                                
                                let contributeBtn = UIButton.init(frame: CGRect(x: withdrawBtn.frame.size.width, y: 0, width: moneyView.frame.size.width/2, height: 40))
                                contributeBtn.setTitle("Contribute Money", for: .normal)
                                contributeBtn.setTitleColor(UIColor.green, for: .normal)
                                contributeBtn.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 16.0)
                                contributeBtn.addTarget(self, action: #selector(self.contributeMoneyClicked), for: .touchUpInside)
                                moneyView.addSubview(contributeBtn)
                            }else{
                                let contributeBtn = UIButton.init(frame: CGRect(x: 0, y: 0, width: self.inputToolbar.contentView.frame.size.width - 40, height: 40))
                                contributeBtn.setTitle("Contribute Money", for: .normal)
                                contributeBtn.setTitleColor(UIColor.green, for: .normal)
                                contributeBtn.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 16.0)
                                contributeBtn.addTarget(self, action: #selector(self.contributeMoneyClicked), for: .touchUpInside)
                                moneyView.addSubview(contributeBtn)
                            }
                            
                        }
                    }
                }
            case .failure(_):
                print(response.result.error!)
                break
            }
            self.group.leave()
            hideActivityIndicator()
        }
    }
    //MARK:- REQUEST MPONEY CLICKED
    @objc func withdrawMoneyClicked(){
        let keyStatus = SharedClass.sharedInstance.retrieveFromUserDefaultsBool(key: "kycStatus")
        let bankStatus = SharedClass.sharedInstance.retrieveFromUserDefaultsBool(key: "bankStatus")
        if keyStatus == false || bankStatus == false{
            let alert = UIAlertController(title: "", message: "Please update you bank details & KYC to Add money", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { UIAlertAction in
                let destVc = self.storyboard?.instantiateViewController(withIdentifier: "KYCViewController") as! KYCViewController
                self.navigationController?.pushViewController(destVc, animated: true)
            }))
            alert.addAction(UIAlertAction(title: "CANCEL", style: .default, handler: { UIAlertAction in
                _ = self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }else{
            if self.wallAccType != "ORGANIZATION"{
                print(self.wallAccType)
                if self.isLocked  == true{ // share wallet is locked
                    Helper.alertBox(Mymsg: "This wallet is currently locked you cannot withdraw coins", view: self)
                }else if self.wallAccType == "BUSINESS"{ //if sharewallet is unlocked and business user
                    if self.userRole == "ADMIN" || self.userRole == "SUBADMIN"{
                      keyboardAppear()
                    }else{
                        Helper.alertBox(Mymsg: "You cannot withdraw coins in this wallet", view: self)
                    }
                }
                else{
                    keyboardAppear()
                }
            }else{
                keyboardAppear()
            }
            isWithdrawClicked = true
            isContributeClicked = false
        }
    }
    
    @objc func attachmentClicked(){
        let actionSheetController: UIAlertController = UIAlertController (title: "Choose an Option", message: nil, preferredStyle: .actionSheet)
        actionSheetController.addAction( UIAlertAction (title: "Cancel", style: .cancel, handler: nil))
        
        actionSheetController.addAction( UIAlertAction (title: "Photo Library", style: .default, handler:{ (alert: UIAlertAction!) in self.chooseFromPhotoLibrary()}
        ))
        actionSheetController.addAction( UIAlertAction (title: "Take Picture", style: .default, handler: {(alert: UIAlertAction!) in self.chooseFromCamera()}
        ))
        self.view.endEditing(true)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    //MARK:- CHOOSE FROM CAMERA
    func chooseFromCamera() {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.cameraCaptureMode = .photo
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    //MARK:- CHOOSE FROM LIBRARY
    func chooseFromPhotoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    
    //MARK:- IMAGE PICKER DELEGATES
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if NetworkingManager.isConnectedToNetwork(){
            showActivityIndicator()
            let pickedImage = info[UIImagePickerControllerEditedImage]
            let imageData = UIImageJPEGRepresentation(pickedImage as! UIImage, 0.5)
            let storageRef = Storage.storage().reference().child("\(String(Date().timeIntervalSince1970)).png")
            let metaData = StorageMetadata()
            metaData.contentType = "image/png/jpeg"
            storageRef.putData(imageData!, metadata: metaData, completion: { (metadata, error) in
                if error == nil{
                    print("sucess")
                    storageRef.downloadURL(completion: { (url, error) in
                        print(url!.absoluteString)
                        let values = ["type": "photo", "content": (url!.absoluteString), "fromID": self.senderId, "toID": self.GroupId, "timestamp": Int(Date().timeIntervalSince1970), "isRead":false,"isRequestSent":false,"isCancel":false] as [String : Any]
                        Message.uploadGroupMessage(withValues: values, toID: self.GroupId, completion: { (_) in
                        })
                        let tokenString = "Bearer " + self.retrievedToken!
                        let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
                        let chatNotifyParams = ["userId":self.GroupId,"message":""] as [String : Any]
                        Alamofire.request(getSWChatNotifyURL, method: .post, parameters: chatNotifyParams, encoding: JSONEncoding.default, headers: Headers).responseJSON { response in
                            switch (response.result){
                            case .success:break
                            case .failure:
                                hideActivityIndicator()
                                break
                            }
                            debugPrint(response)
                        }
                    })
                }else{
                    print(error!)
                }
            })
            self.dismiss(animated: true, completion: nil)
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }
    }
    //MARK:- SEND MONEY CLICKED
    @objc func contributeMoneyClicked(){
        let keyStatus = SharedClass.sharedInstance.retrieveFromUserDefaultsBool(key: "kycStatus")
        let bankStatus = SharedClass.sharedInstance.retrieveFromUserDefaultsBool(key: "bankStatus")
        if keyStatus == false || bankStatus == false{
            let alert = UIAlertController(title: "", message: "Please update you bank details & KYC to Add money", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { UIAlertAction in
                let destVc = self.storyboard?.instantiateViewController(withIdentifier: "KYCViewController") as! KYCViewController
                self.navigationController?.pushViewController(destVc, animated: true)
            }))
            alert.addAction(UIAlertAction(title: "CANCEL", style: .default, handler: { UIAlertAction in
                _ = self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }else{
            keyboardAppear()
            isContributeClicked = true
            isWithdrawClicked = false
        }
    }

    //MARK:- GROUP HEADER TAPPED
    @objc func groupHeaderTapped(_ sender:UITapGestureRecognizer){
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "GroupDetailsViewController") as! GroupDetailsViewController
        destVc.groupNameString = self.GroupNameString
        destVc.groupId = GroupId
        destVc.userDict = GroupDetailsDict
        destVc.groupImageString = GroupImageURL
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- VIEW DID DISAPPEAR
    override func viewDidDisappear(_ animated: Bool) {
        self.chatViewOpened = false
        self.responseValueGot = false
        UIApplication.shared.isStatusBarHidden = false
    }
    
    //MARK:- VIEW WILL DISAPPEAR
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Message.markMessagesRead(forUserID: GroupId)
    }
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getWalletUsersDetails()
        self.navigationController?.isNavigationBarHidden = false
    }
    //MARK:- SHOW CHAT
    @objc func showChat(){
        if NetworkingManager.isConnectedToNetwork(){
            showActivityIndicator()
            print(GroupId)
            Message.downloadAllGroupMessages(forUserID: GroupId, completion: {[weak weakSelf = self] (message) in
            if message.content as! String == ""{
                hideActivityIndicator()
                self.noDataLbl.isHidden = false
                self.noDataLbl.frame = CGRect(x: 0, y: (self.view.frame.size.height/2)-30, width: self.view.frame.size.width, height: 20)
                if self.isDelete == false{
                    self.noDataLbl.text = "No chat was found"
                }else{
                    self.noDataLbl.text = "Wallet delete request processing.."
                }
                self.noDataLbl.textAlignment = .center
                self.view.addSubview(self.noDataLbl)
            }else{
                self.noDataLbl.isHidden = true
                self.responseValueGot = true
                weakSelf?.items.append(message)
                weakSelf?.items.sort{ $0.timestamp < $1.timestamp }
                DispatchQueue.main.async {
                    self.messages.removeAll()
                    if let state = weakSelf?.items.isEmpty, state == false {
                        for data in self.items{
                            if let message = JSQMessage(senderId: data.senderId, displayName: self.name, text: data.content as? String){
                                self.messages.append(message)
                            }
                        }
                        if self.isDelete == true{
                            if let message = JSQMessage(senderId: self.senderId, displayName: self.name, text: "Wallet delete request processing.."){
                                self.messages.append(message)
                            }
                        }
                        self.finishReceivingMessage(animated: false)
//                         self.collectionView?.collectionViewLayout.sectionInset = UIEdgeInsets(top: 100, left: 0, bottom: 35, right: 0)
                    }
                }
//                hideActivityIndicator()
                Message.markMessagesRead(forUserID: self.GroupId)
            }
        })
        }else{
             NetworkingManager.netWorkFailed(view: self)
        }
    }
    //MARK:- COLLECTIONVIEW DELEGATES
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData!{
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource!{
        return messages[indexPath.item].senderId == senderId ? outgoingBubble : incomingBubble
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource!{
        return JSQMessagesAvatarImageFactory.avatarImage(with: #imageLiteral(resourceName: "manProfile"), diameter: 40)
    }

//    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellBottomLabelAt indexPath: IndexPath!) -> NSAttributedString! {
//        let messageDate = Date.init(timeIntervalSince1970: TimeInterval(self.items[indexPath.row].timestamp))
//        let dataformatter = DateFormatter.init()
//        dataformatter.timeStyle = .short
//        let date = dataformatter.string(from: messageDate)
//
//        if isDelete == true{
//            if indexPath.row + 1 == messages.count{
//                return NSAttributedString(string:date)
//            }
//        }
//        if messages[indexPath.item].senderId == senderId{
//            return NSAttributedString(string:date)
//        }else{
//            if self.items.count != indexPath.row + 1{
//                let nextValue = self.items[indexPath.item + 1].senderId
//                if nextValue != self.items[indexPath.item].senderId{
//                    for value in userImageArray{
//                        let dict = value as! Dictionary<String, Any>
//                        if (dict["userId"] as! String).contains(self.items[indexPath.item].senderId) {
//                            let userName = dict["userName"] as! String
//                            return NSAttributedString(string:"\(userName)  \(date)")
//                        }
//                    }
//                }else{
//                    return NSAttributedString(string:date)
//                }
//            }else{
//                if senderId != self.items[indexPath.item].senderId{
//                    for value in userImageArray{
//                        let dict = value as! Dictionary<String, Any>
//                        if (dict["userId"] as! String).contains(self.items[indexPath.item].senderId) {
//                            let userName = dict["userName"] as! String
//                            return NSAttributedString(string:"\(userName)  \(date)")
//                        }
//                    }
//                }else{
//                    return NSAttributedString(string:date)
//                }
//            }
//            return NSAttributedString(string:date)
//        }
//    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellBottomLabelAt indexPath: IndexPath!) -> CGFloat {
        return 20
    }
//    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        if indexPath.row + 1 == messages.count{
//            if isDelete == true{
//                return CGSize(width: self.view.frame.size.width, height: 20)
//            }
//        }
//        return CGSize(width: collectionView.contentSize.width, height: collectionView.contentSize.height)
//    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let messageDate = Date.init(timeIntervalSince1970: TimeInterval(self.items[indexPath.row].timestamp))
            let dataformatter = DateFormatter.init()
            dataformatter.timeStyle = .short
            dataformatter.locale = NSLocale.current
            dataformatter.dateFormat = "MM/dd/yyyy hh:mma"
            let date = dataformatter.string(from: messageDate)
            if indexPath.row + 1 == messages.count{
                if isDelete == true{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DeleteMsgCollectionViewCell", for: indexPath) as! DeleteMsgCollectionViewCell
                    cell.backgroundColor = UIColor.init(red: 249.0/255.0, green: 250.0/255.0, blue:251.0/255.0, alpha: 1.0)
                    return cell
                }
            }
            let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
            cell.textView.font = UIFont(name: "Nunito-Regular", size: 16.0)
            cell.cellBottomLabel.font = UIFont(name: "Nunito-Regular", size: 10.0)
            cell.cellBottomLabel.textAlignment = .right
            if messages[indexPath.item].senderId == senderId{
                //if message is photo
                if self.items[indexPath.item].type == .photo{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryOutgoingCollectionViewCell", for: indexPath) as! GalleryOutgoingCollectionViewCell
                    cell.isUserInteractionEnabled = true
                    let url = URL(string: self.items[indexPath.item].content as! String)
                    cell.galleryOutgoingImgVw.kf.setImage(with: url, placeholder: UIImage(named: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
                    cell.dateAndTimeLabel.text = "\(date)"
                    return cell
                }else{
                    cell.textView!.textColor = UIColor.white
                    cell.cellBottomLabel.text = "\(date)"
                }
            }else{
                //waits for userimagearray data to receive from database
                group.notify(queue: DispatchQueue.main) {
                    cell.textView!.textColor = UIColor.init(red: 23.0/255.0, green: 32.0/255.0, blue: 63.0/255.0, alpha: 1.0)
                    cell.avatarImageView.layer.cornerRadius = cell.avatarImageView.frame.size.height / 2
                    cell.avatarImageView.clipsToBounds = true
                    if self.items.count != indexPath.row + 1{
                        let nextValue = self.items[indexPath.item + 1].senderId
                        if nextValue != self.items[indexPath.item].senderId{
                            cell.avatarImageView.isHidden = false
                            for value in self.userImageArray{
                                let dict = value as! Dictionary<String, Any>
                                if (dict["userId"] as! String).contains(self.items[indexPath.item].senderId) {
                                    let url = URL(string: dict["userImage"] as! String)
                                    cell.avatarImageView.kf.setImage(with: url, placeholder: UIImage(named: "manProfile"), options: nil, progressBlock: nil, completionHandler: nil)
                                    cell.cellBottomLabel.text = "\(dict["userName"] as! String)  \(date)"
                                }
                            }
                        }else{
                            cell.avatarImageView.isHidden = true
                            cell.cellBottomLabel.text = "\(date)"
                        }
                    }else{
                        if self.senderId != self.items[indexPath.item].senderId{
                            cell.avatarImageView.isHidden = false
                            for value in self.userImageArray{
                                let dict = value as! Dictionary<String, Any>
                                if (dict["userId"] as! String).contains(self.items[indexPath.item].senderId) {
                                    let url = URL(string: dict["userImage"] as! String)
                                    cell.avatarImageView.kf.setImage(with: url, placeholder: UIImage(named: "manProfile"), options: nil, progressBlock: nil, completionHandler: nil)
                                    cell.cellBottomLabel.text = "\(dict["userName"] as! String)  \(date)"
                                }
                            }
                        }else{
                            cell.avatarImageView.isHidden = true
                            cell.cellBottomLabel.text = "\(date)"
                        }
                    }
                }
                cell.cellBottomLabel.textAlignment = .left
                
                if self.items[indexPath.item].type == .photo{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryIncomingCollectionViewCell", for: indexPath) as! GalleryIncomingCollectionViewCell
                    let url = URL(string: self.items[indexPath.item].content as! String)
                    cell.galleryIncomingImgVw.kf.setImage(with: url, placeholder: UIImage(named: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
                    cell.userProfileImg.layer.cornerRadius = cell.userProfileImg.frame.size.height / 2
                    cell.userProfileImg.clipsToBounds = true
                    cell.isUserInteractionEnabled = true
                    if self.items.count != indexPath.row + 1 {
                        let nextValue = self.items[indexPath.item + 1].senderId
                        if nextValue != self.items[indexPath.item].senderId{
                            cell.userProfileImg.isHidden = false
                            for value in self.userImageArray{
                                let dict = value as! Dictionary<String, Any>
                                if (dict["userId"] as! String).contains(self.items[indexPath.item].senderId) {
                                    let url = URL(string: dict["userImage"] as! String)
                                    cell.userProfileImg.kf.setImage(with: url, placeholder: UIImage(named: "manProfile"), options: nil, progressBlock: nil, completionHandler: nil)
                                    cell.dateAndNameLbl.text = "\(dict["userName"] as! String)  \(date)"
                                }
                            }
                        }else{
                            cell.userProfileImg.isHidden = true
                            cell.dateAndNameLbl.text = "\(date)"
                        }
                    }else{
                        if self.senderId != self.items[indexPath.item].senderId{
                            cell.userProfileImg.isHidden = false
                            for value in self.userImageArray{
                                let dict = value as! Dictionary<String, Any>
                                if (dict["userId"] as! String).contains(self.items[indexPath.item].senderId) {
                                    let url = URL(string: dict["userImage"] as! String)
                                    cell.userProfileImg.kf.setImage(with: url, placeholder: UIImage(named: "manProfile"), options: nil, progressBlock: nil, completionHandler: nil)
                                    cell.dateAndNameLbl.text = "\(dict["userName"] as! String)  \(date)"
                                }
                            }
                        }else{
                            cell.userProfileImg.isHidden = true
                            cell.dateAndNameLbl.text = "\(date)"
                        }
                    }
                    return cell
                }
            }
            if isDelete == true{
                if indexPath.row + 1 == messages.count {
                    cell.textView.text = "Wallet delete request processing.."
                    cell.cellBottomLabel.isHidden = true
                }
            }
            return cell
    }
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        if self.items[indexPath.item].type == .photo{
            self.view.endEditing(true)
            let alert = UIAlertController(title: "", message: "Save Image?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { UIAlertAction in
                let url = URL(string: self.items[indexPath.item].content as! String)
                self.tappedImageView.kf.setImage(with: url, placeholder: UIImage(named: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
                self.save()
            }))
            alert.addAction(UIAlertAction(title: "NO", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false
        }else{
            if action == #selector(copy(_:)){
                return true
            }else{
                return false
            }
        }
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.items[indexPath.item].type == .photo{
            self.view.endEditing(true)
            scrollImg.delegate = self
            scrollImg.frame = CGRect(x: 0, y: 64, width: self.view.frame.width, height: self.view.frame.height - 64)
            scrollImg.backgroundColor = UIColor(red: 90, green: 90, blue: 90, alpha: 0.90)
            scrollImg.alwaysBounceVertical = false
            scrollImg.alwaysBounceHorizontal = false
            scrollImg.showsVerticalScrollIndicator = true
            scrollImg.flashScrollIndicators()
            scrollImg.minimumZoomScale = 1.0
            scrollImg.maximumZoomScale = 10.0
            self.view.addSubview(scrollImg)
            
            let url = URL(string: self.items[indexPath.item].content as! String)
            tappedImageView.kf.setImage(with: url, placeholder: UIImage(named: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
            tappedImageView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: scrollImg.frame.height)
            tappedImageView.backgroundColor = .black
            tappedImageView.contentMode = .scaleAspectFit
            scrollImg.addSubview(tappedImageView)
            tappedImageView.isUserInteractionEnabled = true
            
            let cancelBtn = UIButton()
            cancelBtn.frame = CGRect(x: self.view.frame.size.width - 40, y: (self.navigationController?.navigationBar.frame.origin.y)! + (self.navigationController?.navigationBar.frame.size.height)! + 10, width: 30, height: 30)
            cancelBtn.addTarget(self, action: #selector(cancelBtnClicked), for: .touchUpInside)
            cancelBtn.setImage(UIImage(named: "cross_blue"), for: .normal)
            self.tappedImageView.addSubview(cancelBtn)
        }
    }
    //MARK: - Saving Image here
    func save() {
        guard let selectedImage = tappedImageView.image else {
            print("Image not found!")
            return
        }
        UIImageWriteToSavedPhotosAlbum(selectedImage, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    //MARK: - Add image to Library
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            showAlertWith(title: "Save error", message: error.localizedDescription)
        } else {
            showAlertWith(title: "Saved!", message: "Your image has been saved to your photos.")
        }
    }
    
    func showAlertWith(title: String, message: String){
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
    }
    //MARK:- CANCEL CLICKED
    @objc func cancelBtnClicked(){
        scrollImg.removeFromSuperview()
    }
    
    override func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.tappedImageView
    }
    //MARK:- SEND MESSAGE CLICKED
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!){
        if NetworkingManager.isConnectedToNetwork(){
            let userName = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "name")!
            let tokenString = "Bearer " + retrievedToken!
            let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
            if isDelete == false{
                if isContributeClicked == true{
                    let amount = UserDefaults.standard.value(forKey: "amount") as! NSNumber
                    if let myInteger = Double(text!) {
                        let enteredNumber = NSNumber(value:myInteger)
                        if Double(truncating: enteredNumber) <= Double(truncating: amount){
                            let contributeParams = ["fromId":senderId,"toId":GroupId,"amount": text!] as [String : Any]
                            Alamofire.request(contributeURL, method: .post, parameters: contributeParams, encoding: JSONEncoding.default, headers: Headers).responseJSON { response in
                                switch (response.result){
                                case .success:break
                                case .failure:break
                                }
                                debugPrint(response)
                            }
                            
                            let values = ["type": "text", "content": String(describing:"\(userName) contributed CSH \(text!)"), "fromID": senderId , "toID": GroupId, "timestamp": Int(Date().timeIntervalSince1970), "isRead": false,"isRequestSent":false,"isCancel":false] as [String : Any]
                            let chatNotifyParams = ["sharedWalletId":self.GroupId,"message":String(describing:"\(userName) contributed CSH \(text!)")] as [String : Any]
                            Alamofire.request(getSWChatNotifyURL, method: .post, parameters: chatNotifyParams, encoding: JSONEncoding.default, headers: Headers).responseJSON { response in
                                switch (response.result){
                                case .success:break
                                case .failure:break
                                }
                                debugPrint(response)
                            }
                            Message.uploadGroupMessage(withValues: values, toID: GroupId, completion: { (_) in
                            })
                        }else{
                            hideActivityIndicator()
                            Helper.alertBox(Mymsg: "Insufficient balance please buy more coins", view: self)
                        }
                    }
                }else if isWithdrawClicked == true{
                    if let myInteger = Double(text!) {
                        let enteredNumber = NSNumber(value:myInteger)
                        if Double(truncating: enteredNumber) <= Double(truncating: amountString){
                            let withDrawParams = ["fromId":GroupId,"toId":senderId,"amount": text!] as [String : Any]
                            Alamofire.request(withdrawalURL, method: .post, parameters: withDrawParams, encoding: JSONEncoding.default, headers: Headers).responseJSON { response in
                                switch (response.result){
                                case .success:break
                                case .failure:
                                    break
                                }
                                debugPrint(response)
                            }
                            let values = ["type": "text", "content": String(describing:"\(userName) withdrawn CSH \(text!)"), "fromID": senderId , "toID": GroupId, "timestamp": Int(Date().timeIntervalSince1970), "isRead": false,"isRequestSent":false,"isCancel":false] as [String : Any]
                            
                            let chatNotifyParams = ["sharedWalletId":self.GroupId,"message":String(describing:"\(userName) withdrawn CSH \(text!)")] as [String : Any]
                            Alamofire.request(getSWChatNotifyURL, method: .post, parameters: chatNotifyParams, encoding: JSONEncoding.default, headers: Headers).responseJSON { response in
                                switch (response.result){
                                case .success:break
                                case .failure:break
                                }
                                debugPrint(response)
                            }
                            Message.uploadGroupMessage(withValues: values, toID: GroupId, completion: { (_) in
                            })
                        }else{
                            hideActivityIndicator()
                            Helper.alertBox(Mymsg: "Insufficient balance please buy more coins", view: self)
                            view.endEditing(true)
                        }
                    }
                }else{
                    let values = ["type": "text", "content": text, "fromID": senderId , "toID": GroupId, "timestamp": Int(Date().timeIntervalSince1970), "isRead": false,"isRequestSent":false,"isCancel":false] as [String : Any]
                    Message.uploadGroupMessage(withValues: values, toID: GroupId, completion: { (_) in
                    })
                    let chatNotifyParams = ["sharedWalletId":self.GroupId,"message":text] as [String : Any]
                    Alamofire.request(getSWChatNotifyURL, method: .post, parameters: chatNotifyParams, encoding: JSONEncoding.default, headers: Headers).responseJSON { response in
                        switch (response.result){
                        case .success:break
                        case .failure:break
                        }
                        debugPrint(response)
                    }
                }
            }else{
                view.endEditing(true)
                Helper.alertBox(Mymsg: "This wallet is in Deleting process..", view: self)
            }
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }

    }
//    func sendButtonAction(myMessage:String) {
//        if myMessage.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
//            let message6 = JSQMessage(senderId: senderId, displayName: senderDisplayName, text: myMessage)
//            print(message6!)
//            self.messages.append(message6!)
//            self.finishReceivingMessage(animated: false)
//        }
//    }
    //MARK:- SETUP BACK BUTTON
    func setupBackButton() {
        let backButton: UIButton = UIButton(type: UIButtonType.custom)
        backButton.setImage(UIImage(named: "back_icon"), for: UIControlState.normal)
        backButton.addTarget(self, action:  #selector(backClicked(_:)), for: UIControlEvents.touchUpInside)
        backButton.frame = CGRect(x: 5, y: 20, width: 40, height: 30)
        let barButton = UIBarButtonItem(customView: backButton)
        navigationItem.leftBarButtonItem = barButton
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK:- BACK CLICKED
    @IBAction func backClicked(_ sender: Any) {
        self.chatViewOpened = false
        self.responseValueGot = false
        _ = navigationController?.popViewController(animated: true)
    }
    
}
