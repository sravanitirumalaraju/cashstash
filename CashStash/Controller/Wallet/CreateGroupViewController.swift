//
//  CreateGroupViewController.swift
//  CashStash
//
//  Created by apple on 14/03/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit
import Firebase
import Alamofire
import SwiftyJSON
import Kingfisher

class CreateGroupViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet var createButton: UIButton!
    @IBOutlet var tilteLabel: UILabel!
    @IBOutlet var groupIconButton: UIButton!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var groupNameTxtFld: UITextField!
    @IBOutlet var groupContactsTblVw: UITableView!
    var (phoneNumbersArray,namesArray) = (Array<Any>(),NSMutableArray())
    var (tempPhoneArray,tempAllNamesArray) = (Array<Any>(),NSMutableArray())
    var filtered = [String]()
    var IDsArray = [String]()
    var groupUsersArray = [String]()
    var isRequestBool = Bool()
    var requestTag = Int()
    var imagePicker = UIImagePickerController()
    var isUserAdd = Bool()
    var groupName = String()
    var existUserArray = [String]()
    var allUserIds = [String]()
    var walletId = String()
    let blurView = UIView()
    let retrievedToken: String? = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "token")
    let userAccType = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "accountType")

    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        blurView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        blurView.backgroundColor = AppColors.APP_BLURVIEW_COLOR
        view.addSubview(blurView)
        blurView.isHidden = true
        
        if isUserAdd == true{
            tilteLabel.text = "Add Users"
            groupNameTxtFld.isUserInteractionEnabled = false
            groupIconButton.isUserInteractionEnabled = false
            groupNameTxtFld.text = groupName
            createButton.setTitle("Add", for: .normal)
        }
        
        imagePicker.delegate = self
        self.navigationController?.isNavigationBarHidden = true
        fetchContacts()
    }
    func fetchContacts(){
        var PhoneNumber = String()
        var allNumbersDict = NSMutableDictionary()
        
        showActivityIndicator()
        self.phoneNumbersArray.removeAll()
        self.namesArray.removeAllObjects()
        self.tempPhoneArray.removeAll()
        self.tempAllNamesArray.removeAllObjects()
        Contact.fetchingCoreData(completion: { (contactResults) in
            if contactResults.count == 0{
                hideActivityIndicator()
                let alert = UIAlertController(title: "", message: "No access given to contacts", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { UIAlertAction in
                    _ = self.navigationController?.popViewController(animated: true)
                }))
                self.present(alert, animated: true, completion: nil)
            }else{
                Contact.getRegisterUsers(completion: {(RegistersDict) in
                    if let _ = RegistersDict["message"] as? String{
                    }else{
                        for result in contactResults {
                            PhoneNumber = (result.value(forKey: "mobileNumber")!) as! String
                            let contactName = (result.value(forKey: "contactName")!)
                            for (_,value) in RegistersDict.enumerated(){
                                let contactDict = value.value as! NSMutableDictionary
                                let numberDict = contactDict.value(forKey: "registers") as! NSMutableDictionary
                                // accountType is only PERSONAL
                                if numberDict["accountType"] as! String == "PERSONAL" && self.userAccType == "PERSONAL"{
                                    if (PhoneNumber).contains(numberDict["Contact"] as! String){
                                        let mobileNo = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "mobile")
                                        allNumbersDict = ["name":contactName,"image":numberDict["profilePic"] as! String,"phonenumber":numberDict["Contact"] as! String]
                                        if numberDict["Contact"] as? String != mobileNo{
                                            if self.isUserAdd == true{
                                                let filtered = self.existUserArray.filter { $0.contains(PhoneNumber) }
                                                if filtered.isEmpty == true{
                                                    self.phoneNumbersArray.append(numberDict["Contact"] as! String)
                                                    self.namesArray.add(allNumbersDict)
                                                    self.tempAllNamesArray = self.namesArray
                                                    self.tempPhoneArray = self.phoneNumbersArray
                                                    self.IDsArray.append(value.key as! String)
                                                }
                                            }else{
                                                self.phoneNumbersArray.append(numberDict["Contact"] as! String)
                                                self.namesArray.add(allNumbersDict)
                                                self.tempAllNamesArray = self.namesArray
                                                self.tempPhoneArray = self.phoneNumbersArray
                                                self.IDsArray.append(value.key as! String)
                                            }
                                        }
                                    }
                                }else // accountType is PERSONAL or BUSINESS
                                    if self.userAccType == "BUSINESS" && (numberDict["accountType"] as! String == "PERSONAL" || numberDict["accountType"] as! String == "BUSINESS"){
                                        
                                        if (PhoneNumber).contains(numberDict["Contact"] as! String){
                                            let mobileNo = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "mobile")
                                            allNumbersDict = ["name":contactName,"image":numberDict["profilePic"] as! String,"phonenumber":numberDict["Contact"] as! String]
                                            if numberDict["Contact"] as? String != mobileNo{
                                                if self.isUserAdd == true{
                                                    let filtered = self.existUserArray.filter { $0.contains(PhoneNumber) }
                                                    if filtered.isEmpty == true{
                                                        self.phoneNumbersArray.append(numberDict["Contact"] as! String)
                                                        self.namesArray.add(allNumbersDict)
                                                        self.tempAllNamesArray = self.namesArray
                                                        self.tempPhoneArray = self.phoneNumbersArray
                                                        self.IDsArray.append(value.key as! String)
                                                    }
                                                }else{
                                                    self.phoneNumbersArray.append(numberDict["Contact"] as! String)
                                                    self.namesArray.add(allNumbersDict)
                                                    self.tempAllNamesArray = self.namesArray
                                                    self.tempPhoneArray = self.phoneNumbersArray
                                                    self.IDsArray.append(value.key as! String)
                                                }
                                            }
                                        }
                                    }else // accountType is PERSONAL or ORGANIZATION
                                        if self.userAccType == "ORGANIZATION" && (numberDict["accountType"] as! String == "PERSONAL" || numberDict["accountType"] as! String == "ORGANIZATION"){
                                            if (PhoneNumber).contains(numberDict["Contact"] as! String){
                                                let mobileNo = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "mobile")
                                                allNumbersDict = ["name":contactName,"image":numberDict["profilePic"] as! String,"phonenumber":numberDict["Contact"] as! String]
                                                if numberDict["Contact"] as? String != mobileNo{
                                                    if self.isUserAdd == true{
                                                        let filtered = self.existUserArray.filter { $0.contains(PhoneNumber) }
                                                        if filtered.isEmpty == true{
                                                            self.phoneNumbersArray.append(numberDict["Contact"] as! String)
                                                            self.namesArray.add(allNumbersDict)
                                                            self.tempAllNamesArray = self.namesArray
                                                            self.tempPhoneArray = self.phoneNumbersArray
                                                            self.IDsArray.append(value.key as! String)
                                                        }
                                                    }else{
                                                        self.phoneNumbersArray.append(numberDict["Contact"] as! String)
                                                        self.namesArray.add(allNumbersDict)
                                                        self.tempAllNamesArray = self.namesArray
                                                        self.tempPhoneArray = self.phoneNumbersArray
                                                        self.IDsArray.append(value.key as! String)
                                                    }
                                                }
                                            }
                                }
                                
                            }
                        }
                    }
                    DispatchQueue.main.async {
                        self.groupContactsTblVw.delegate = self
                        self.groupContactsTblVw.dataSource = self
                        self.groupContactsTblVw.reloadData()
                    }
                    hideActivityIndicator()
                })
            }
        })
    }
   //MARK:- TABLEVIEW DELEGATES
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tempAllNamesArray.count != 0{
            return tempAllNamesArray.count
        }else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: AddMembersTableViewCell = tableView.dequeueReusableCell(withIdentifier:"Cell", for: indexPath as IndexPath) as! AddMembersTableViewCell
        if tempAllNamesArray.count != 0{
            cell.contactNameLbl.isHidden = false
            cell.contactNoLbl.isHidden = false
            cell.profileImageVw.isHidden = false
            cell.requestBtn.isHidden = false
            let dict = tempAllNamesArray[indexPath.row] as! NSDictionary
            cell.contactNameLbl.text = dict["name"] as! String?
            cell.contactNoLbl.text = dict["phonenumber"] as? String
            if dict["image"] as! String != ""{
                let url = URL(string: dict["image"] as! String)
                cell.profileImageVw.kf.setImage(with: url, placeholder: UIImage(named: "manProfile"), options: nil, progressBlock: nil, completionHandler: nil)
            }else{
                 cell.profileImageVw.image = #imageLiteral(resourceName: "manProfile")
            }
            cell.requestBtn.tag = indexPath.row
            cell.requestBtn.addTarget(self, action: #selector(requestClicked), for: .touchUpInside)
            if isRequestBool == true{
                if indexPath.row == requestTag{
                    cell.requestBtn.setTitle("Requested", for: .normal)
                    groupUsersArray.append(IDsArray[indexPath.row])
                    groupUsersArray.uniqInPlace()
                }
            }
        }else{
            cell.contactNameLbl.isHidden = true
            cell.contactNoLbl.isHidden = true
            cell.profileImageVw.isHidden = true
            cell.requestBtn.isHidden = true
            let nooResults_label = UILabel.init(frame:CGRect(x: 0, y: cell.frame.size.height/3, width: self.view.frame.size.width, height: 50))
            nooResults_label.textColor = UIColor.lightGray
            if isUserAdd == true{
                nooResults_label.text = "No New Users"
            }else{
               nooResults_label.text = "No Contacts Found"
            }
            nooResults_label.textAlignment = .center
            nooResults_label.numberOfLines = 0
            cell.contentView.addSubview(nooResults_label)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tempAllNamesArray.count == 0 {
            return self.view.frame.size.height-230
        } else {
            return 80
        }
    }
    //MARK:- REQUEST CLICKED
    @objc func requestClicked(_ sender: UIButton){
        if sender.title(for: .normal)! != "Requested"{
            requestTag = sender.tag
            isRequestBool = true
            groupContactsTblVw.reloadData()
        }
    }
    //MARK:- SEARCHBAR DELEGATES
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText != ""{
            var nameArray = [String]()
            let filteredArray = NSMutableArray()
            var filteredPhoneArray = Array<Any>()
            nameArray.removeAll()
            filteredArray.removeAllObjects()
            filteredPhoneArray.removeAll()
            tempAllNamesArray = namesArray
            for index in tempAllNamesArray{
                let tempDict = index as! NSDictionary
                let name = tempDict["name"] as! String
                nameArray.append(name)
                filtered = searchText.isEmpty ? nameArray : nameArray.filter { (item: String) -> Bool in
                    return item.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
                }
                for i in filtered {
                    if i == name{
                        filteredArray.add(tempDict)
                        let number = tempDict["phonenumber"] as! String
                        filteredPhoneArray.append(number)
                    }
                }
            }
            tempAllNamesArray = filteredArray
            tempPhoneArray = filteredPhoneArray
            self.groupContactsTblVw.reloadData()
        }else{
            tempAllNamesArray = namesArray
            tempPhoneArray = phoneNumbersArray
            self.groupContactsTblVw.reloadData()
        }
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.groupContactsTblVw.reloadData()
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
        self.groupContactsTblVw.reloadData()
    }
    //MARK:- CAMERA UPLOAD CLICKED
    @IBAction func cameraUploadClicked(_ sender: Any) {
        let actionSheetController: UIAlertController = UIAlertController (title: "Choose an Option", message: nil, preferredStyle: .actionSheet)
        actionSheetController.addAction( UIAlertAction (title: "Cancel", style: .cancel, handler: nil))
        actionSheetController.addAction( UIAlertAction (title: "Photo Library", style: .default, handler:{ (alert: UIAlertAction!) in self.chooseFromPhotoLibrary()}
        ))
        actionSheetController.addAction( UIAlertAction (title: "Take Picture", style: .default, handler: {(alert: UIAlertAction!) in self.chooseFromCamera()}
        ))
        self.view.endEditing(true)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    //MARK:- CHOOSE FROM CAMERA
    func chooseFromCamera() {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.cameraCaptureMode = .photo
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    //MARK:- CHOOSE FROM LIBRARY
    func chooseFromPhotoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    //MARK:- IMAGE PICKER DELEGATES
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var selectedImage: UIImage?
        groupIconButton.contentMode = .scaleAspectFit
        selectedImage = info[UIImagePickerControllerEditedImage] as? UIImage
        self.groupIconButton.setImage(selectedImage, for: .normal)
        groupIconButton.layer.cornerRadius = groupIconButton.frame.size.height/2
        groupIconButton.clipsToBounds = true
        self.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    //MARK:- BACK CLICKED
    @IBAction func backClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    //MARK:- TEXTFIELD RETURN
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK:- CREATE GROUP CLICKED
    @IBAction func createGroupClicked(_ sender: Any) {
        if NetworkingManager.isConnectedToNetwork(){
            if groupNameTxtFld.text! == ""{
                Helper.alertBox(Mymsg: "Please enter group name", view: self)
            }else if groupUsersArray.isEmpty == true{
                if isUserAdd == true || tempAllNamesArray.count == 0{
                    Helper.alertBox(Mymsg: "No contact Found", view: self)
                }else{
                    Helper.alertBox(Mymsg: "Select atleast one contact", view: self)
                }
            }else{
                showActivityIndicator()
                var walletParams = [String:Any]()
                var walletURL = String()
                if isUserAdd == false{
                    walletParams = ["walletName":groupNameTxtFld.text!,"usersInSharedWallet":groupUsersArray]
                    walletURL = createShareWalletURL
                }else{
                    walletParams = ["sharedWalletId":walletId,"userIds":groupUsersArray]
                    walletURL = AddUsersWalletURL
                }
                let tokenString = "Bearer " + retrievedToken!
                let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
                Alamofire.request(walletURL, method: HTTPMethod.post, parameters: walletParams,encoding: JSONEncoding.default, headers: Headers).responseJSON { response in
                    hideActivityIndicator()
                    switch(response.result) {
                    case .success(let value):
                        let json = JSON(value)
                        if response.response?.statusCode == 200{
                            if let sucessValue = json["success"].string{
                                Helper.alertBox(Mymsg: sucessValue, view: self)
                            }else{
                                self.allUserIds.append(contentsOf: self.groupUsersArray)
                                let data = ["registers": self.allUserIds]
                                if self.isUserAdd == false{
                                    self.walletId = json["sharedWalletId"].string!
                                    rootRef.child("groupUsers").child(self.walletId).setValue(data)
                                }else{
                                    rootRef.child("groupUsers").child(self.walletId).updateChildValues(data)
                                }
                                let destVc = self.storyboard?.instantiateViewController(withIdentifier: "ShareWalletViewController") as! ShareWalletViewController
                                destVc.isWalletCreated = true
                                self.navigationController?.pushViewController(destVc, animated: true)
                            }
                        }
                    case .failure(_):
                        print(response.result.error!)
                        break
                    }
                }
            }
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
