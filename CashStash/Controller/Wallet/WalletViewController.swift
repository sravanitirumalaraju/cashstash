//
//  WalletViewController.swift
//  CashStash
//
//  Created by apple on 01/02/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit

class WalletViewController: UIViewController{
    
    @IBOutlet weak var amountLbl: UILabel!
   
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        let amount = UserDefaults.standard.value(forKey: "amount") as! NSNumber
        
        let amountText = NSMutableAttributedString.init(string: "CSH \(String(describing: amount))")
        amountText.setAttributes([NSAttributedStringKey.font : UIFont(name: "Barlow-Medium", size: 16.0)!,NSAttributedStringKey.foregroundColor : UIColor.white], range: NSMakeRange(0, 3))
        amountLbl.attributedText = amountText
//        amountLbl.text = "CSH 100"
    }
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    //MARK:- HOME CLICKED
    @IBAction func homeClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:-WALLET CLICKED
    @IBAction func walletClicked(_ sender: Any) {
        
    }
    //MARK:-CONTACT CLICKED
    @IBAction func contactClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "FavouriteListViewController") as! FavouriteListViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:-CHAT CLICKED
    @IBAction func chatClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:-PROFILE CLICKED
    @IBAction func profileClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:-BACK CLICKED
    @IBAction func backClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK:- NOTIFICATION CLICKED
    @IBAction func notificationClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
}
