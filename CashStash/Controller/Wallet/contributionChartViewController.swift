//
//  contributionChartViewController.swift
//  CashStash
//
//  Created by apple on 3/27/19.
//  Copyright © 2019 havells. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class contributionChartViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    

    @IBOutlet weak var chartTableView: UITableView!
    var shareWalletId = String()
    var userDetailsArray = Array<Any>()
    var totalContribution = Float()
    var totalWithdrawn = Float()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getTransactionDetails()
    }
    func getTransactionDetails(){
        if NetworkingManager.isConnectedToNetwork(){
            showActivityIndicator()
            let retrievedToken: String? = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "token")
            let tokenString = "Bearer " + retrievedToken!
            let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
            Alamofire.request("\(SWContributionURL)\(shareWalletId)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: Headers).responseJSON{ response in
                hideActivityIndicator()
                print(response)
                switch (response.result){
                case .success(let value):
                    let json = JSON(value)
                    let receiveDict = json.dictionary
                    self.userDetailsArray = (receiveDict!["users"]?.array)!
                    let allContribute = (receiveDict!["allContributions"]?.string)!
                    self.totalContribution = (allContribute as NSString).floatValue
                    let allWithdraw = (receiveDict!["allWithdraws"]?.string)!
                    self.totalWithdrawn = (allWithdraw as NSString).floatValue
                    self.chartTableView.reloadData()
                case .failure(_):
                    print(response.result.error!)
                    break
                }
            }
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.userDetailsArray.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ChartTableViewCell = tableView.dequeueReusableCell(withIdentifier:"ChartTableViewCell", for: indexPath as IndexPath) as! ChartTableViewCell
        cell.selectionStyle = .none
        
        if userDetailsArray.count == 0{
            let nooResults_label = UILabel.init(frame:CGRect(x: 0, y: cell.frame.size.height/3, width: self.view.frame.size.width, height: 50))
            nooResults_label.textColor = UIColor.lightGray
            nooResults_label.text = "No Results Found"
            nooResults_label.textAlignment = .center
            nooResults_label.backgroundColor =  AppColors.APP_BACKGROUND_COLOR
            nooResults_label.numberOfLines = 0
            cell.contentView.addSubview(nooResults_label)
        }else{
            let indivialDict = JSON(self.userDetailsArray[indexPath.row])
            cell.userNameLbl.text = indivialDict["userName"].string!
            let contriFloat = ((indivialDict["totalContribution"].string!) as NSString).floatValue
            let withdrawFloat = ((indivialDict["totalWithdraw"].string!) as NSString).floatValue
            cell.contributeprogress.setProgress(contriFloat/self.totalContribution, animated: false)
            cell.contributeStatusLbl.text = "\(indivialDict["totalContribution"].string!) CSH Contributed"
            cell.withDrawProgress.setProgress(withdrawFloat/self.totalWithdrawn, animated: false)
            cell.withDrawStatusLbl.text = "\(indivialDict["totalWithdraw"].string!) CSH Withdrawn"
            let url = URL(string: (indivialDict["UserImage"].string!))
            cell.userImageVw.kf.setImage(with: url, placeholder: UIImage(named: "manProfile"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        return cell

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 165
    }
    @IBAction func backClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
}
