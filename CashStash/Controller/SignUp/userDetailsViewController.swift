//
//  userDetailsViewController.swift
//  CashStash
//
//  Created by apple on 3/22/19.
//  Copyright © 2019 havells. All rights reserved.
//

import UIKit
import Firebase
import Alamofire
import SwiftyJSON

class userDetailsViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var addressTxtfld: FloatLabelTextField!
    @IBOutlet weak var cityTxtFLd: FloatLabelTextField!
    @IBOutlet weak var zipTxtfld: FloatLabelTextField!
    @IBOutlet weak var stateTxtfld: FloatLabelTextField!
    @IBOutlet weak var dobTxtfld: FloatLabelTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addressTxtfld.delegate = self
        cityTxtFLd.delegate = self
        zipTxtfld.delegate = self
        stateTxtfld.delegate = self
        dobTxtfld.delegate = self
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == addressTxtfld {
            self.cityTxtFLd.becomeFirstResponder()
        }else if textField == cityTxtFLd{
            self.stateTxtfld.becomeFirstResponder()
        }else if textField == stateTxtfld{
            self.zipTxtfld.becomeFirstResponder()
        }else if textField == zipTxtfld{
            self.dobTxtfld.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
        return true
    }

    @IBAction func saveClicked(_ sender: Any) {
        if NetworkingManager.isConnectedToNetwork(){
            showActivityIndicator()
            if addressTxtfld.text! == "" && cityTxtFLd.text! == "" && stateTxtfld.text! == "" && zipTxtfld.text! == "" && dobTxtfld.text! == ""{
                Helper.alertBox(Mymsg: "Please enter all fields", view: self)
            }else if addressTxtfld.text! == "" {
                Helper.alertBox(Mymsg: "Please enter address", view: self)
            }else if cityTxtFLd.text! == ""{
                Helper.alertBox(Mymsg: "Please enter city", view: self)
            }else if stateTxtfld.text! == ""{
                Helper.alertBox(Mymsg: "Please enter state", view: self)
            }else if zipTxtfld.text! == "" {
                Helper.alertBox(Mymsg: "Please enter zip", view: self)
            }else if dobTxtfld.text! == ""{
                Helper.alertBox(Mymsg: "Please enter date of birth", view: self)
            }else{
                let userDetailsParams = ["address":"\(self.addressTxtfld.text!)","city":"\(self.cityTxtFLd.text!)","state":"\(self.stateTxtfld.text!)","zip":"\(self.zipTxtfld.text!)","dob":self.dobTxtfld.text!]
                let retrievedToken: String? = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "token")
                let tokenString = "Bearer " + retrievedToken!
                let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
                Alamofire.request(updateOtherDetailsURL, method: HTTPMethod.put, parameters: userDetailsParams,encoding: JSONEncoding.default, headers: Headers).responseJSON { response in
//                    print(response)
                    hideActivityIndicator()
                    switch(response.result) {
                    case .success(let value):
                        let json = JSON(value)
                        if response.response?.statusCode == 200{
                            //                        if let sucessValue = json["success"].string{
                            //                            Helper.alertBox(Mymsg: sucessValue, view: self)
                            //                        }else{
                            let destVc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                            self.navigationController?.pushViewController(destVc, animated: true)
                            //                        }
                        }else{
                            if let message = json["message"].string{
                                Helper.alertBox(Mymsg: message, view: self)
                            }
                        }
                    case .failure(_):
                        print(response.result.error!)
                        break
                    }
                    debugPrint(response)
                    hideActivityIndicator()
                }
            }
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }
    }
}
