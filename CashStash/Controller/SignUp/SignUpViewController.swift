//
//  SignUpViewController.swift
//  CashStash
//
//  Created by apple on 30/01/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import FirebaseCore
import Firebase
import UserNotifications

class SignUpViewController: UIViewController,UITextFieldDelegate {
    struct Platform {
        static let isSimulator: Bool = {
            var isSim = false
            #if arch(i386) || arch(x86_64)
            isSim = true
            #endif
            return isSim
        }()
    }
    @IBOutlet var countryCodeBtn: UIButton!
    @IBOutlet var countryCodeLbl: UILabel!
    @IBOutlet var nameTxtFld: FloatLabelTextField!
    @IBOutlet var mobileNoTxtFld: FloatLabelTextField!
    @IBOutlet var passwordTxtFld: FloatLabelTextField!
    @IBOutlet var emailTxtFld: FloatLabelTextField!
    @IBOutlet var sendOtpBtn: UIButton!
    @IBOutlet var segmentControl: UISegmentedControl!
    @IBOutlet var selectUserBtn: UIButton!
    @IBOutlet weak var userTypeVw: UIView!
    @IBOutlet weak var referalTxtFld: FloatLabelTextField!
    
    let blurView = UIView()
    let popUpView = UIView()
    var accountType = String()
    var enterOTPTxtFld = FloatLabelTextField()
    var locationManager: CLLocationManager!
    var countryCodeVw = UIView()
    var isOtpClicked = Bool()
    
    //MARK:-VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        userTypeVw.isHidden = true
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = UIColor.white
        
        isOtpClicked = false
        countryCodeVw.isHidden = true
        sendOtpBtn.layer.cornerRadius = sendOtpBtn.frame.size.height/2
        sendOtpBtn.clipsToBounds = true
        selectUserBtn.layer.cornerRadius = 10
        selectUserBtn.clipsToBounds = true
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneButtonAction))
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        mobileNoTxtFld.inputAccessoryView = doneToolbar
        enterOTPTxtFld.inputAccessoryView = doneToolbar
        
        blurView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        blurView.backgroundColor = AppColors.APP_BLURVIEW_COLOR
        self.view.addSubview(blurView)
        blurView.isHidden = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        view.addGestureRecognizer(tap)
        view.isUserInteractionEnabled = true
    }
    //MARK:- HANDLE TAP GESTURE
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
        userTypeVw.isHidden = true
    }
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    //MARK:- SELECT USER CLICKED
    @IBAction func selectUserClicked(_ sender: Any) {
        userTypeVw.isHidden = false
        popUpView.isHidden = true
        userTypeVw.layer.shadowColor = UIColor(white: 0.0, alpha: 0.25).cgColor
        userTypeVw.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        userTypeVw.layer.shadowOpacity = 1.0
        userTypeVw.layer.shadowRadius = 10
        userTypeVw.layer.masksToBounds = false
        userTypeVw.layer.cornerRadius = 10
        userTypeVw.backgroundColor = UIColor.white
    }
    //MARK:- DONE BUTTO CLICKED
    @objc func doneButtonAction() {
        if isOtpClicked == false{
            passwordTxtFld.becomeFirstResponder()
        }else{
            enterOTPTxtFld.resignFirstResponder()
        }
    }
    //MARK:-TETXFIELD DELEGATES
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == passwordTxtFld{
            self.view.frame.origin.y -= 50
        }else if textField == emailTxtFld{
            self.view.frame.origin.y -= 50
        }else if textField == referalTxtFld{
            self.view.frame.origin.y -= 50
        }
        else if textField == mobileNoTxtFld{
            isOtpClicked = false
        }else if textField == enterOTPTxtFld{
            isOtpClicked = true
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == passwordTxtFld{
            self.view.frame.origin.y += 50
        }else if textField == emailTxtFld{
            self.view.frame.origin.y += 50
        }else if textField == referalTxtFld{
            self.view.frame.origin.y += 50
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == nameTxtFld {
            self.mobileNoTxtFld.becomeFirstResponder()
        }else if textField == mobileNoTxtFld{
            self.passwordTxtFld.becomeFirstResponder()
        }else if textField == passwordTxtFld{
            self.emailTxtFld.becomeFirstResponder()
        }else if textField == emailTxtFld{
            self.emailTxtFld.resignFirstResponder()
            sendOTPClicked(UIButton.self)
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
    //MARK:-SEND OTP CLICKED
    @IBAction func sendOTPClicked(_ sender: Any) {
        if nameTxtFld.text! == "" && mobileNoTxtFld.text! == "" && passwordTxtFld.text! == "" && emailTxtFld.text! == "" && selectUserBtn.titleLabel?.text == "Select user Type"{
            Helper.alertBox(Mymsg: "please enter all fields", view: self)
        }
        else if selectUserBtn.titleLabel?.text == "Select user Type" {
            Helper.alertBox(Mymsg: "please select User Type", view: self)
        }else if nameTxtFld.text! == "" {
            Helper.alertBox(Mymsg: "please enter User name", view: self)
        }else if mobileNoTxtFld.text! == ""{
            Helper.alertBox(Mymsg: "please enter Mobile number", view: self)
        }else if passwordTxtFld.text! == ""{
            Helper.alertBox(Mymsg: "please enter Password", view: self)
        }else if emailTxtFld.text! == ""{
            Helper.alertBox(Mymsg: "please enter email", view: self)
        }
        else{
            let isEmailAddressValid = isValidEmailAddress(emailAddressString: emailTxtFld.text!)
            if isEmailAddressValid{
                blurView.isHidden = false
                popUpView.isHidden = false
                isOtpClicked = true
                
                popUpView.frame = CGRect(x: 0, y: 0, width: 300, height: 200)
                popUpView.center = self.blurView.center
                popUpView.backgroundColor = AppColors.APP_COLOR_WHITE
                popUpView.layer.cornerRadius = 10
                popUpView.clipsToBounds = true
                self.view.addSubview(popUpView)
                
                let cancelBtn = UIButton.init(frame: CGRect(x: popUpView.frame.size.width-40, y: 10, width: 30, height: 30))
                cancelBtn.setImage(UIImage(named:"cross_blue"), for: .normal)
                cancelBtn.addTarget(self, action: #selector(cancelClicked), for: .touchUpInside)
                popUpView.addSubview(cancelBtn)
                
                enterOTPTxtFld.frame = CGRect(x: 50, y: 40, width: 200, height: 40)
                enterOTPTxtFld.placeholder = "Enter OTP"
                enterOTPTxtFld.font = UIFont(name: "Nunito-Regular", size: 16)
                enterOTPTxtFld.autocorrectionType = .no
                enterOTPTxtFld.keyboardType = .numberPad
                enterOTPTxtFld.delegate = self
                popUpView.addSubview(enterOTPTxtFld)
                
                verifyWithFirebase(phonenumber: mobileNoTxtFld.text!)
                
                let verifyOtpBtn = UIButton.init(frame: CGRect(x: 50, y: popUpView.frame.size.height - 100, width: 200, height: 40))
                verifyOtpBtn.setTitle("Verify OTP to SignUp", for: .normal)
                verifyOtpBtn.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 14)
                verifyOtpBtn.layer.cornerRadius = verifyOtpBtn.frame.size.height/2
                verifyOtpBtn.backgroundColor = UIColor(red: 36.0/255.0, green: 137.0/255.0, blue: 247.0/255.0, alpha: 1.0)
                verifyOtpBtn.addTarget(self, action: #selector(verifyClicked), for: .touchUpInside)
                popUpView.addSubview(verifyOtpBtn)
                
                let resendOTP = UIButton.init(frame: CGRect(x: 0, y: popUpView.frame.size.height - 30, width: popUpView.frame.size.width, height: 20))
                resendOTP.setTitle("Resend OTP", for: .normal)
                resendOTP.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 14)
                resendOTP.setTitleColor(UIColor(red: 36.0/255.0, green: 137.0/255.0, blue: 247.0/255.0, alpha: 1.0), for: .normal)
                resendOTP.addTarget(self, action: #selector(resendClicked), for: .touchUpInside)
                popUpView.addSubview(resendOTP)
            }
        }
    }
    //MARK:- VERIFY EMAIL
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            if results.count == 0{
                returnValue = false
            }
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        return  returnValue
    }
    //MARK:- RESEND OTP CLICKED
    @objc func resendClicked(){
       verifyWithFirebase(phonenumber: mobileNoTxtFld.text!)
    }
    //MARK:- VERIFY FIREBASE
    func verifyWithFirebase(phonenumber : String){
        let mobileNumber = ("+1\(mobileNoTxtFld.text!)")
        print(mobileNumber)
        PhoneAuthProvider.provider().verifyPhoneNumber(mobileNumber, uiDelegate: nil, completion: { (verificationID, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }else{
                Helper.alertBox(Mymsg: "OTP sent successfully", view: self)
            }
            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
            UserDefaults.standard.synchronize()
        })
//        PhoneAuthProvider.provider().verifyPhoneNumber("+1\(mobileNoTxtFld.text ?? "")", uiDelegate: nil) { (verificationID, error) in
//            if let error = error {
//                print(error.localizedDescription)
//                return
//            }
//            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
//            UserDefaults.standard.synchronize()
//        }
    }
    //MARK:- CANCEL CLICKED
    @objc func cancelClicked(){
        blurView.isHidden = true
        popUpView.isHidden = true
    }
    //MARK:- VERIFY CLICKED
    @objc func verifyClicked(){
        if NetworkingManager.isConnectedToNetwork(){
            if enterOTPTxtFld.text! == ""{
                Helper.alertBox(Mymsg: "Enter OTP", view: self)
            }else{
                let credentials : PhoneAuthCredential = PhoneAuthProvider.provider().credential(withVerificationID: UserDefaults.standard.value(forKey: "authVerificationID") as! String, verificationCode: enterOTPTxtFld.text!)
                Auth.auth().signInAndRetrieveData(with: credentials, completion: { (user, error) in
//                Auth.auth().signIn(with: credentials) { (user, error) in
                    if (error != nil){
                        print(error.debugDescription)
                    }else{
//                        print("Phone Number :-", user?.phoneNumber! as Any)
//                        let userinfo = user?.provideImageData[0]
//                        print(userinfo?.providerID as Any)
                        showActivityIndicator()
                        let signUpParams = ["name":"\(self.nameTxtFld.text!)","contactNumber":"\(self.mobileNoTxtFld.text!)","password":"\(self.passwordTxtFld.text!)","accountType":self.accountType,"emailId":self.emailTxtFld.text!,"referralCode":self.referalTxtFld.text!]
                        let userHeaders: HTTPHeaders = ["Content-Type": "application/json"]
                        Alamofire.request(signUpURL, method: HTTPMethod.post, parameters: signUpParams,encoding: JSONEncoding.default, headers: userHeaders).responseJSON { response in
                            switch(response.result) {
                            case .success(let value):
                                let json = JSON(value)
                                if response.response?.statusCode == 200{
                                    if let sucessValue = json["success"].string{
                                        Helper.alertBox(Mymsg: sucessValue, view: self)
                                    }else{
                                        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "userDetailsViewController") as! userDetailsViewController
                                        self.navigationController?.pushViewController(destVc, animated: true)
                                        let stringValue = json["token"].string!
                                        SharedClass.sharedInstance.saveToUserDefaultsWithString(string: stringValue, key: "token")
                                        SharedClass.sharedInstance.saveToUserDefaultsWithString(string: json["userId"].string!, key: "userId")
                                        SharedClass.sharedInstance.saveToUserDefaultsWithString(string: json["userName"].string!, key: "name")
                                        SharedClass.sharedInstance.saveToUserDefaultsWithString(string: json["userContactNumber"].string!, key: "mobile")
                                        SharedClass.sharedInstance.saveToUserDefaultsWithString(string: json["userImageUrl"].string!, key: "profile")
                                        SharedClass.sharedInstance.saveToUserDefaultsWithString(string: json["accountType"].string!, key: "accountType")
                                        UserDefaults.standard.setValue(json["walletAmount"].number!, forKey: "amount")
                                        UserDefaults.standard.synchronize()
                                        let Dict = ["name":json["userName"].string!,"Contact":json["userContactNumber"].string!,"email":self.emailTxtFld.text!,"profilePic":json["userImageUrl"].string!,"accountType":json["accountType"].string!]
                                        let regRef = rootRef.child("users").child(json["userId"].string!).child("registers")
                                        regRef.setValue(Dict)
                                        self.appRegisteration()
                                    }
                                }else{
                                    if let message = json["message"].string{
//                                        Helper.alertBox(Mymsg: "Already registered with this number", view: self)
                                        Helper.alertBox(Mymsg: message, view: self)
                                        self.popUpView.isHidden = true
                                        self.blurView.isHidden = true
                                    }
                                }
                            case .failure(_):
                                print(response.result.error!)
                                break
                            }
                            debugPrint(response)
                            hideActivityIndicator()
                        }
                    }
                })
            }
        }else{
             NetworkingManager.netWorkFailed(view: self)
        }
    }
    
    //MARK:- APP REGISTERATION
    @objc func appRegisteration(){
        if NetworkingManager.isConnectedToNetwork(){
        if Platform.isSimulator == false {
//            print(UserDefaults.standard.value(forKey: "FCM_Token")!)
            //            if UserDefaults.standard.value(forKey: "FCM_Token") == nil{
//            if UserDefaults.standard.value(forKey: "FCM_Token") != nil{
            if let FCMToken: String = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "FCM_Token"){
                if FCMToken != "" {
                    let registerationId = UserDefaults.standard.value(forKey: "FCM_Token") as! String
                    let retrievedToken: String? = SharedClass.sharedInstance.retrieveFromUserDefaultString(key: "token")
                    let tokenString = "Bearer " + retrievedToken!
                    let Headers : HTTPHeaders = ["Authorization":tokenString,"Content-Type":"application/json"]
                    let registerParams = ["registrationId":registerationId]
                    Alamofire.request(appRegisterURL, method: .post, parameters: registerParams, encoding: JSONEncoding.default, headers: Headers).responseJSON { response in
                        switch (response.result){
                        case .success:
                            print(response.result.value!)
                        case .failure:
                            hideActivityIndicator()
                            print(response.result.error!)
                            break
                        }
                        debugPrint(response)
                    }
                }
            }
        }
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }
    }
    //MARK:- COUNTRY CODE CLICKED
    @IBAction func countryCodeBtnClicked(_ sender: Any) {
        countryCodeVw.isHidden = false
        countryCodeVw.frame = CGRect(x: countryCodeBtn.frame.origin.x, y: countryCodeBtn.frame.origin.y + countryCodeBtn.frame.size.height, width: countryCodeBtn.frame.size.width, height: (countryCodeBtn.frame.size.height * 2)+1)
        countryCodeVw.backgroundColor = AppColors.APP_BLURVIEW_COLOR
        self.view.addSubview(countryCodeVw)
        
        let indianCodeBtn = UIButton.init(frame: CGRect(x: 0, y: 0, width: countryCodeVw.frame.size.width, height: countryCodeBtn.frame.size.height))
        indianCodeBtn.setTitle("+91", for: .normal)
        indianCodeBtn.setTitleColor(AppColors.APP_COLOR_WHITE, for: .normal)
        indianCodeBtn.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 14)
        indianCodeBtn.backgroundColor = UIColor.clear
        indianCodeBtn.addTarget(self, action: #selector(indianCodeClicked), for: .touchUpInside)
        countryCodeVw.addSubview(indianCodeBtn)
        
        let lineLbl = UILabel.init(frame: CGRect(x: 0, y: indianCodeBtn.frame.origin.y + indianCodeBtn.frame.size.height, width: countryCodeBtn.frame.size.width, height: 1))
        lineLbl.backgroundColor = UIColor.white
        countryCodeVw.addSubview(lineLbl)
        
        let USCodeBtn = UIButton.init(frame: CGRect(x: 0, y: indianCodeBtn.frame.origin.y + indianCodeBtn.frame.size.height + 1, width: countryCodeBtn.frame.size.width, height: countryCodeBtn.frame.size.height))
        USCodeBtn.setTitle("+1", for: .normal)
        USCodeBtn.backgroundColor = UIColor.clear
        USCodeBtn.setTitleColor(AppColors.APP_COLOR_WHITE, for: .normal)
        USCodeBtn.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 14)
        USCodeBtn.addTarget(self, action: #selector(USCodeClicked), for: .touchUpInside)
        countryCodeVw.addSubview(USCodeBtn)
    }
    //MARK:- INDIAN CLICKED
    @objc func indianCodeClicked(){
        countryCodeLbl.text = "+91"
        countryCodeVw.isHidden = true
    }
    //MARK:- US CODE CLICKED
    @objc func USCodeClicked(){
        countryCodeLbl.text = "+1"
        countryCodeVw.isHidden = true
    }
    @IBAction func personalClicked(_ sender: Any) {
        nameTxtFld.placeholder = "Name"
        accountType = "PERSONAL"
        userTypeVw.isHidden = true
        self.selectUserBtn.setTitle("Personal", for: .normal)
    }
    @IBAction func businessClicked(_ sender: Any) {
        nameTxtFld.placeholder = "Business Name"
        accountType = "BUSINESS"
        userTypeVw.isHidden = true
        self.selectUserBtn.setTitle("Business", for: .normal)
    }
    @IBAction func orgClicked(_ sender: Any) {
        nameTxtFld.placeholder = "Organization Name"
        accountType = "ORGANIZATION"
        userTypeVw.isHidden = true
        self.selectUserBtn.setTitle("Organization", for: .normal)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
