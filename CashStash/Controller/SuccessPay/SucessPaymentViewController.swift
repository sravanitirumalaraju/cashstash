//
//  SucessPaymentViewController.swift
//  CashStash
//
//  Created by apple on 06/02/18.
//  Copyright © 2018 havells. All rights reserved.
//

import UIKit

class SucessPaymentViewController: UIViewController,UITabBarDelegate {

    @IBOutlet weak var paidMobileNoLbl: UILabel!
    @IBOutlet weak var paidNameLbl: UILabel!
    @IBOutlet var sucessTabBar: UITabBar!
    
    var paidToNumber = String()
    var paidToPerson = String()
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        paidMobileNoLbl.text = paidToNumber
        paidNameLbl.text = paidToPerson
    }
    //MARK:- VIEW WILL DISAPPEAR
    override func viewWillDisappear(_ animated: Bool) {
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = UIColor(red: 5.0/255.0, green: 78.0/255.0, blue: 106.0/255.0, alpha: 1.0)
    }
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = UIColor.clear
        self.navigationController?.isNavigationBarHidden = true
    }
    //MARK:- HOME CLICKED
    @IBAction func homeClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- WALLET CLICKED
    @IBAction func walletClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "TransactionsViewController") as! TransactionsViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- CONTACT CLICKED
    @IBAction func contactClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "FavouriteListViewController") as! FavouriteListViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- CHAT CLICKED
    @IBAction func chatClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- PROFILE CLICKED
    @IBAction func profileClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    //MARK:- MAKE PAY CLICKED
    @IBAction func makePayClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "FavouriteListViewController") as! FavouriteListViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

}
