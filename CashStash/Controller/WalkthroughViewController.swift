//
//  WalkthroughViewController.swift
//  CashStash
//
//  Created by apple on 3/26/19.
//  Copyright © 2019 havells. All rights reserved.
//

import UIKit

class WalkthroughViewController: UIViewController,UIScrollViewDelegate {

    @IBOutlet weak var walkthroughScrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var skipButton: UIButton!
    
    var (walkImages,walkContent,walkString) = ([String](),[String](),[String]())

    
    override func viewDidLoad() {
        super.viewDidLoad()
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = UIColor.white

        skipButton.isHidden = false
        walkContent = ["Create Account", "Easy Transactions", "Share Wallet"]
        walkString  = ["Sign Up as Personal, Business or Organization", "Send and Receive USD in the form of CSH Coin", "Share wallet with friends, family and acquaintance"]
        walkImages = ["createAccount","easyTransactions","shareWalletWalk"]
        walkthroughScrollView.delegate = self
        self.walkthroughScrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        walkthroughScrollView.isPagingEnabled = true

        for index in 0...2{
            let walkthroughImage = UIImageView()
            walkthroughImage.frame = CGRect(x: (walkthroughScrollView.frame.size.width) * CGFloat(index) + 20, y: 70, width: (walkthroughScrollView.frame.size.width) - 50 , height: (walkthroughScrollView.frame.size.width) - 54 )
            walkthroughImage.image = UIImage(named:walkImages[index])
            self.walkthroughScrollView.addSubview(walkthroughImage)
            
            let titleLabel = UILabel.init(frame: CGRect(x: (walkthroughScrollView.frame.size.width) * CGFloat(index) + 20, y: (walkthroughImage.frame.origin.y) + (walkthroughImage.frame.size.height) + 30, width: (walkthroughScrollView.frame.size.width) - 40, height: 25))
            titleLabel.textAlignment = .center
            titleLabel.text = walkContent[index]
            titleLabel.font = UIFont(name: "Barlow-Medium", size: 20.0)
            titleLabel.textColor = UIColor(red: 66.0/255.0 , green: 78.0/255.0, blue: 117.0/255.0, alpha: 1.0)
            self.walkthroughScrollView.addSubview(titleLabel)
            
            let contentLabel = UILabel.init(frame: CGRect(x: (walkthroughScrollView.frame.size.width) * CGFloat(index), y: (titleLabel.frame.origin.y) + (titleLabel.frame.size.height) + 20, width: (walkthroughScrollView.frame.size.width), height: 40))
            contentLabel.textAlignment = .center
            contentLabel.numberOfLines = 0
            contentLabel.textColor = UIColor(red: 157.0/255.0 , green: 145.0/255.0, blue: 171.0/255.0, alpha: 1.0)
            contentLabel.font = UIFont(name: "Nunito-Light", size: 14.0)
            contentLabel.text = walkString[index]
            self.walkthroughScrollView.addSubview(contentLabel)
        }
        self.walkthroughScrollView.contentSize = CGSize(width: self.walkthroughScrollView.frame.width * 3, height: self.walkthroughScrollView.frame.height - 20 )
    }
    

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
        if pageControl.currentPage == 2 {
            skipButton.setTitle("Next", for: .normal)
        }else{
            skipButton.setTitle("Skip", for: .normal)
        }
    }

    @IBAction func skipClicked(_ sender: Any) {
        let destVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    
    @IBAction func pageControlClicked(_ sender: Any) {
        let pageNumber = pageControl.currentPage
        var frame:CGRect = walkthroughScrollView.frame
        frame.origin.x = walkthroughScrollView.frame.size.width * CGFloat(pageNumber)
        walkthroughScrollView.scrollRectToVisible(frame, animated: true)
    }
    
}
